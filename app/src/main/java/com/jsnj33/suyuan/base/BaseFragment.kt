package com.lxm.module_library.base

import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.util.ClassUtil
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment<VM : ViewModel> : Fragment() {

    // ViewModel
    protected lateinit var viewModel: VM

    private var mCompositeDisposable: CompositeDisposable? = null


    private var isViewCreated: Boolean = false // 界面是否已创建完成


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val ll = inflater.inflate(getLayoutID(), null)
        return ll
    }

    /**
     * 视图
     */
    protected abstract fun getLayoutID(): Int;

    /**
     * 初始化控件
     */
    protected abstract fun initLoadView();

    /**
     *加载数据
     */
    protected abstract fun loadData();

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        isViewCreated = true
        initViewModel()
        initLoadView()
        loadData()
    }


    /**
     * 初始化ViewModel
     */
    private fun initViewModel() {
        this.viewModel = ViewModelProviders.of(this).get(viewModel::class.java)
    }
}
