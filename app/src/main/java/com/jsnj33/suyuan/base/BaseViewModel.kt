package com.jsnj33.suyuan.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsnj33.suyuan.repository.model.Resource
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel() {
    var mPage = 0;
    var mPageSize = 15
    private var mCompositeDisposable: CompositeDisposable? = null
    val loadStatus by lazy {
        MutableLiveData<Resource<String>>()
    }

    /**
     * 加载中
     */
    protected fun onLoading(msg: String? = ""): Unit {
        loadStatus.postValue(Resource.loading(msg))
    }

    /**
     * 加载成功
     */
    protected fun onSuccess(msg: String? = ""): Unit {
        loadStatus.postValue(Resource.success(msg))
    }

    /**
     * 加载中
     */
    protected fun onFailure(msg: String? = ""): Unit {
        loadStatus.postValue(Resource.error(msg))
    }

    protected fun addDisposable(disposable: Disposable) {
        if (this.mCompositeDisposable == null) {
            this.mCompositeDisposable = CompositeDisposable()
        }
        this.mCompositeDisposable?.add(disposable)
    }

    public override fun onCleared() {
        super.onCleared()
        if (this.mCompositeDisposable != null && !mCompositeDisposable?.isDisposed!!) {
            this.mCompositeDisposable?.clear()
        }
    }
}
