package com.jsnj33.suyuan.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.jsnj33.suyuan.util.ClassUtil
import me.jessyan.autosize.internal.CustomAdapt


abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity(), CustomAdapt {

    // ViewModel
    protected lateinit var viewModel: VM


    /**
     * 视图
     */
    protected abstract fun getLayoutID(): Int;

    /**
     * 初始化控件
     */
    protected abstract fun initLoadView();

    /**
     *加载数据
     */
    protected abstract fun loadData();

    /**
     * 监听数据变化
     */
    protected abstract fun watchListner();

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(getLayoutID())
        initViewModel()
        initLoadView()
        loadData()
        watchListner()
        super.onCreate(savedInstanceState)

    }


    /**
     * 初始化ViewModel
     */
    private fun initViewModel() {
        val viewModelClass = ClassUtil.getViewModel<VM>(this)
        if (viewModelClass != null) {
            viewModel = ViewModelProviders.of(this).get(viewModelClass)
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        this.viewModel.onCleared();
    }

    override fun isBaseOnWidth(): Boolean = true;

    override fun getSizeInDp(): Float = 375F
}
