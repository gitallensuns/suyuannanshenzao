package com.jsnj33.suyuan.view.record

import android.content.Context
import android.content.Intent
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.viewmodel.PatrolViewModel
import kotlinx.android.synthetic.main.activity_patrol_detail.*
import kotlinx.android.synthetic.main.toolbar_header_right.*

/**
 * 巡田详情
 */
class PatrolDetailActivity : BaseActivity<PatrolViewModel>() {
    companion object {
        @JvmStatic
        fun start(context: Context, patrolId: String) {
            val starter = Intent(context, PatrolDetailActivity::class.java)
            starter.putExtra("patrolId", patrolId)
            context.startActivity(starter)
        }
    }

    override fun getLayoutID(): Int = R.layout.activity_patrol_detail

    override fun initLoadView() {
        tvTitle.text = "巡田详情"
        var patrolId = intent.getStringExtra("patrolId")

        viewModel.getPatrolDetail(patrolId)
    }

    override fun loadData() {
    }

    override fun watchListner() {
        viewModel.patrolDetail.observe(this, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                with(it.data) {
                    Glide.with(this@PatrolDetailActivity).load(this?.fieldPic)
                        .into(ivImage)
                    tvFertilizerTime.text="${this?.fieldTime}"
                    tvFertilizerCreater.text="${this?.fieldPerson}"
                    tvInfoUpload.text="${this?.messageReportName}"
                    tvFarmerName.text="${this?.fieldFarmName}"
                    tvAreaName.text="${this?.fieldLocation}"
                    tvAreaName.text="${this?.fieldLocation}"
                    tvRemark.text="${this?.remarks?:""}"
                }
            } else {
                ToastUtil.show("${it.message}")
            }
        })
        ivBack.setOnClickListener {
            finish()
        }

        ivAdd.setOnClickListener {
            AddPatrolActivity.start(this)
        }
    }
}