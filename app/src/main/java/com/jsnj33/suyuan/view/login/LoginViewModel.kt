package com.jsnj33.suyuan.view.login

import androidx.lifecycle.MutableLiveData
import com.jsnj33.suyuan.http.RxHelper
import com.jsnj33.suyuan.repository.model.HttpResponse
import com.jsnj33.suyuan.repository.model.LoginBean
import com.jsnj33.suyuan.repository.model.RequestLoginParams
import com.jsnj33.suyuan.repository.model.ResultListing
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.base.BaseViewModel
import com.jsnj33.suyuan.repository.client.RetrofitClient
import io.reactivex.functions.Consumer

class LoginViewModel : BaseViewModel() {
    var loginBean = MutableLiveData<HttpResponse<LoginBean>>()
    /**
     * 登录接口
     */
    fun login(
        phone: String,
        password: String,
        mobileToken: String
    ): ResultListing<HttpResponse<LoginBean>> {
        RetrofitClient.getInstance().login(RequestLoginParams(phone, password, mobileToken))
            .compose(RxHelper.rxSchedulerHelper()).subscribe(Consumer {
                loginBean.postValue(it)
            }, Consumer {
                loginBean.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
            })
        return ResultListing(loginBean)
    }
}