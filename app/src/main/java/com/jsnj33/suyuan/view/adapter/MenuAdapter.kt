package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.MenuBean

class MenuAdapter : BaseMultiItemQuickAdapter<MenuBean, BaseViewHolder> {
    companion object {
        const val ONE = 0;
        const val TOW = 1;
    }

    init {
        addItemType(ONE, R.layout.adapter_menu_one)
        addItemType(TOW, R.layout.adapter_menu_two)
    }

    constructor(data: MutableList<MenuBean>?) : super(data)

    override fun convert(helper: BaseViewHolder, item: MenuBean) {
        helper.addOnClickListener(R.id.llOnclick)
        when (helper.itemViewType) {
            ONE -> {
                helper.setText(R.id.tvName, item.name)
                    .setImageResource(R.id.ivIcon, item.url)
                    .setBackgroundRes(R.id.llOnclick, item.bg)
            }
            TOW -> {
                helper.setText(R.id.tvName, item.name)
                    .setImageResource(R.id.ivIcon, item.url)
            }
        }
    }

}