package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.TraceProductionBean
import com.jsnj33.suyuan.util.StringUtils

class SuccessAdapter(data: List<TraceProductionBean>?) :
    BaseQuickAdapter<TraceProductionBean, BaseViewHolder>(R.layout.adapter_successitem, data) {
    override fun convert(helper: BaseViewHolder, item: TraceProductionBean?) {
        helper.setText(R.id.tvInputName, item?.inputName)
            .setText(
                R.id.tvNum,
                "${item?.inputConsum} ${StringUtils.getInputUnitValue(item?.inputUnit!!)} "
            )
    }


}