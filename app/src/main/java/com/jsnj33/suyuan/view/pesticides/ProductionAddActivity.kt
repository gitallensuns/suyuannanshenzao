package com.jsnj33.suyuan.view.pesticides

import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.widget.addTextChangedListener
import com.bigkoo.pickerview.builder.TimePickerBuilder
import com.bigkoo.pickerview.view.TimePickerView
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.model.RequestProductionParams
import com.jsnj33.suyuan.repository.model.UploadIMGBean
import com.jsnj33.suyuan.util.TimeUtils
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.framing.UploadIMGActivity2
import com.jsnj33.suyuan.viewmodel.ProductionViewModel
import kotlinx.android.synthetic.main.activity_production_add.*
import kotlinx.android.synthetic.main.toolbar_header.*
import java.util.*

/**
 *@Author 萤火虫
 *@Date  2021/6/3
 *@Desc 生产加工
 */
class ProductionAddActivity : BaseActivity<ProductionViewModel>() {

    companion object {
        private const val TAG = "ProductionAddActivity"

        @JvmStatic
        fun start(context: Context, archiId: String?, enterNumber: String, dangan: String, productionName: String, persion: String) {
            val starter = Intent(context, ProductionAddActivity::class.java)
            starter.putExtra("archiId", archiId)
            starter.putExtra("enterNumber", enterNumber)
            starter.putExtra("dangan", dangan)
            starter.putExtra("productionName", productionName)
            starter.putExtra("persion", persion)
            context.startActivity(starter)
        }
    }

    override fun getLayoutID(): Int = R.layout.activity_production_add
    var dangan: String? = null
    var archiId: String? = null
    var enterNumber: String? = null
    var productionName: String? = null
    var persion: String? = null
    private var timePicker: TimePickerView? = null
    val requestProductionParams by lazy {
        RequestProductionParams()
    }
    var uploadList: ArrayList<UploadIMGBean>? = null
    override fun initLoadView() {
        tvTitle.text = "生产加工"
        archiId = intent.getStringExtra("archiId")
        dangan = intent.getStringExtra("dangan")
        enterNumber = intent.getStringExtra("enterNumber")
        productionName = intent.getStringExtra("productionName")
        persion = intent.getStringExtra("persion")
        requestProductionParams.archiId = archiId
        requestProductionParams.archiNumber = dangan
        requestProductionParams.enterNumber = enterNumber
        requestProductionParams.principal = persion
        requestProductionParams.productName = productionName
        tvDA.text = "$dangan"
        tvProductName.text = "$productionName"
        tvRZR.text = "$persion"
        initChooseTime()
    }

    override fun loadData() {
    }

    override fun watchListner() {
        viewModel.uploadIMGResult.observe(this@ProductionAddActivity, androidx.lifecycle.Observer {
            if (it.code == ResultStatus.SUCCESS) {
                ToastUtil.show("上传成功")
                var stringBuffer = StringBuffer("")
                with(it.data) {
                    this?.forEachIndexed { index, uploadIMGBean2 ->
                        if (index == (this.size - 1)) {
                            stringBuffer.append("${uploadIMGBean2.filePath + uploadIMGBean2.fileName}")
                        } else {
                            stringBuffer.append("${uploadIMGBean2.filePath + uploadIMGBean2.fileName},")
                        }
                    }
                    requestProductionParams.pic = stringBuffer.toString()
                }
                viewModel.addProduction(requestProductionParams)
            } else {
                ToastUtil.show("上传失败")
//                mStatusView.handleSuccess()
            }
        })

        viewModel.addResult.observe(this, androidx.lifecycle.Observer {
            if (it.code == ResultStatus.SUCCESS) {
                ToastUtil.show("添加成功")
                finish()
            } else {
                ToastUtil.show("添加失败")
//                mStatusView.handleSuccess()
            }
        })
        tvPic.setOnClickListener {
            UploadIMGActivity2.start(this, type = 0, uploadatas = uploadList)
        }
        btnChoose.setOnClickListener {
//            viewModel.addProduction(requestProductionParams)
            if (tvTime.text.isNullOrEmpty()) {
                ToastUtil.show("请选择生产日期")
                return@setOnClickListener
            }

            if (uploadList.isNullOrEmpty()) {
                ToastUtil.show("请上传生产图片")
                return@setOnClickListener
            } else {
                if (etRemark.text.isNullOrEmpty()) {
                    ToastUtil.show("请输入备注信息")
                    return@setOnClickListener
                }
                viewModel.uploadIMG(uploadList!!)
            }
        }
        tvTime.setOnClickListener {
            timePicker?.show()
        }
        etRemark.addTextChangedListener {
            requestProductionParams.processRemark = "${it.toString()}"
        }
        ivBack.setOnClickListener { finish() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == RESULT_OK) {
            try {
                uploadList = data?.getSerializableExtra("uploadGson") as ArrayList<UploadIMGBean>
                if (uploadList.isNullOrEmpty() || (uploadList?.size!! - 1 == 0)) {
                    tvPic.text = ""
                } else {
                    tvPic.text = "您选择了${(uploadList?.size!! - 1)}张图片"
                }

            } catch (e: Exception) {
                Log.d(TAG, "onActivityResult: ${e.message}")
            }
        }
    }

    private fun initChooseTime() {
        val selectedDate = Calendar.getInstance()
        selectedDate.time = Date()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker = TimePickerBuilder(this) { date, v -> //选中事件回调
                tvTime.text = TimeUtils.date2Str(date, TimeUtils.FORMAT_YMDHMS)
                requestProductionParams.processDate = "${TimeUtils.date2Str(date, TimeUtils.FORMAT_YMDHMS)}"
            }
                .setType(booleanArrayOf(true, true, true, true, true, false)) // 默认全部显示
                .setCancelText("取消") //取消按钮文字
                .setSubmitText("确定") //确认按钮文字
                .setSubCalSize(15)
                .setTitleSize(18) //标题文字大小
                .setTitleText("时间选择") //标题文字
                .setOutSideCancelable(false) //点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(false) //是否循环滚动
                .setTitleColor(getColor(R.color.colorPrimary)) //标题文字颜色
                .setSubmitColor(getColor(R.color.colorPrimary)) //确定按钮文字颜色
                .setCancelColor(getColor(R.color.colorPrimary)) //取消按钮文字颜色
                .setTitleBgColor(getColor(R.color.colorWhite)) //标题背景颜色 Night mode
                .setBgColor(getColor(R.color.colorWhite)) //滚轮背景颜色 Night mode
                .setDate(selectedDate) // 如果不设置的话，默认是系统时间*/
//                .setRangDate(startDate, selectedDate) //起始终止年月日设定
                .setLabel("年", "月", "日", "时", "分", "") //默认设置为年月日时分秒
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .isDialog(false) //是否显示为对话框样式
                .build()
        }
    }

}
