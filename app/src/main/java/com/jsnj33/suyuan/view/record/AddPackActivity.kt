package com.jsnj33.suyuan.view.record

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bigkoo.pickerview.builder.TimePickerBuilder
import com.bigkoo.pickerview.view.TimePickerView
import com.example.zhouwei.library.CustomPopWindow
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.model.PackLebelBean
import com.jsnj33.suyuan.repository.model.PesticidesBean
import com.jsnj33.suyuan.repository.model.RequstAddPackParams
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.util.SoftKeyboardUtil
import com.jsnj33.suyuan.util.TimeUtils
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.PackLebel2Adapter
import com.jsnj33.suyuan.view.adapter.PackLebelAdapter
import com.jsnj33.suyuan.viewmodel.PackViewModel
import kotlinx.android.synthetic.main.activity_add_pack.*
import kotlinx.android.synthetic.main.toolbar_header.*
import me.jessyan.autosize.utils.AutoSizeUtils
import java.util.*

//添加包装管理
class AddPackActivity : BaseActivity<PackViewModel>() {
    companion object {
        @JvmStatic
        fun start(context: Context, processStatus: String) {
            val starter = Intent(context, AddPackActivity::class.java)
            starter.putExtra("processStatus", processStatus)
            context.startActivity(starter)
        }
    }

    private var timePicker: TimePickerView? = null
    lateinit var processStatus: String
    var personName: String? by PreferencesUtil("personName", "")
    lateinit var popwindow: CustomPopWindow
    lateinit var mRecyclerView: RecyclerView
    lateinit var adapter: PackLebelAdapter

    lateinit var popwindow2: CustomPopWindow
    lateinit var mRecyclerView2: RecyclerView
    lateinit var adapter2: PackLebel2Adapter

    val requstAddPackParams by lazy {
        RequstAddPackParams()
    }

    override fun getLayoutID(): Int = R.layout.activity_add_pack

    override fun initLoadView() {
        processStatus = intent.getStringExtra("processStatus")
        tvTitle.text = "新增包装管理"
        initChooseTime()
        popwindow()
        popwindow2()
    }

    override fun loadData() {
        mStatusView.handleSuccess()
        viewModel.getPestList(processStatus, "$personName")
    }

    override fun watchListner() {
        btnChoose.setOnClickListener {
            if (verEmpty()) {
                mStatusView.handleLoading()
                viewModel.addPackManage(requstAddPackParams)
            }
        }
        ivBack.setOnClickListener {
            finish()
        }
        viewModel.addStatus.observe(this, androidx.lifecycle.Observer {
            mStatusView.handleSuccess()
            if (it.code == ResultStatus.SUCCESS) {
                ToastUtil.show("添加成功")
                finish()
            } else {
                ToastUtil.show("${it.message}")
            }
        })
        viewModel.productionList.observe(this, androidx.lifecycle.Observer {
            if (it.code == ResultStatus.SUCCESS) {
                adapter.setNewData(it.data)
            }
        })

        tvDAH.setOnClickListener {
            SoftKeyboardUtil.hideInput(this)

            popwindow.showAsDropDown(tvDAH, 0, 10, Gravity.RIGHT)
        }
        tvDate.setOnClickListener {
            SoftKeyboardUtil.hideInput(this)
            timePicker?.show()
        }
        tvPackMethod.setOnClickListener {
            SoftKeyboardUtil.hideInput(this)
            popwindow2.showAsDropDown(tvPackMethod, 0, 10, Gravity.RIGHT)
        }
    }

    private fun verEmpty(): Boolean {
        if (tvDAH.text.isNullOrEmpty()) {
            ToastUtil.show("请选择档案号")
            return false
        }

        if (tvSpeac.text.isNullOrEmpty()) {
            ToastUtil.show("请输入规格")
            return false
        }
        requstAddPackParams.specs = "${tvSpeac.text}"
        if (tvCL.text.isNullOrEmpty()) {
            ToastUtil.show("请输入材料")
            return false
        }
        requstAddPackParams.material = "${tvCL.text}"
        if (tvPackMethod.text.isNullOrEmpty()) {
            ToastUtil.show("请选择包装方式")
            return false
        }
        requstAddPackParams.material = "${tvCL.text}"
        if (tvPackNum.text.isNullOrEmpty()) {
            ToastUtil.show("请输入包装数量")
            return false
        }
        requstAddPackParams.num = "${tvPackNum.text}"

        if (tvLevel.text.isNullOrEmpty()) {
            ToastUtil.show("请输入等级")
            return false
        }
        requstAddPackParams.level = "${tvLevel.text}"

        if (tvDate.text.isNullOrEmpty()) {
            ToastUtil.show("请选择包装日期")
            return false
        }
        return true
    }

    private fun initChooseTime() {
        val selectedDate = Calendar.getInstance()
        selectedDate.time = Date()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker = TimePickerBuilder(this) { date, v -> //选中事件回调
                tvDate.text = TimeUtils.date2Str(date, TimeUtils.FORMAT_YMDHMS)
                requstAddPackParams.packDate = TimeUtils.date2Str(date, TimeUtils.FORMAT_YMDHMS)
            }
                .setType(booleanArrayOf(true, true, true, true, true, false)) // 默认全部显示
                .setCancelText("取消") //取消按钮文字
                .setSubmitText("确定") //确认按钮文字
                .setSubCalSize(15)
                .setTitleSize(18) //标题文字大小
                .setTitleText("时间选择") //标题文字
                .setOutSideCancelable(false) //点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(false) //是否循环滚动
                .setTitleColor(getColor(R.color.colorPrimary)) //标题文字颜色
                .setSubmitColor(getColor(R.color.colorPrimary)) //确定按钮文字颜色
                .setCancelColor(getColor(R.color.colorPrimary)) //取消按钮文字颜色
                .setTitleBgColor(getColor(R.color.colorWhite)) //标题背景颜色 Night mode
                .setBgColor(getColor(R.color.colorWhite)) //滚轮背景颜色 Night mode
                .setDate(selectedDate) // 如果不设置的话，默认是系统时间*/
//                .setRangDate(startDate, selectedDate) //起始终止年月日设定
                .setLabel("年", "月", "日", "时", "分", "") //默认设置为年月日时分秒
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .isDialog(false) //是否显示为对话框样式
                .build()
        }
    }

    private fun popwindow() {
        var view = LayoutInflater.from(this).inflate(R.layout.layout_popwindow, null)
        mRecyclerView = view.findViewById(R.id.mRecyclerView)
        adapter = PackLebelAdapter(null, processStatus)
        mRecyclerView.adapter = adapter
        adapter.setOnItemChildClickListener { adapter, view, position ->
            var data = adapter.data[position] as PesticidesBean
            tvProduction.text = "${data.productName}"
            if (processStatus == "10000") {
                requstAddPackParams.archiNumber = "${data.archiNumber}"
                tvDAH.text = "${data.archiNumber}"
            } else {
                requstAddPackParams.slaughterArchiNumber = "${data.slaughterArchiNumber}"
                tvDAH.text = "${data.slaughterArchiNumber}"
            }
            requstAddPackParams.productName = "${data.productName}"
            requstAddPackParams.archiId = "${data.id}"
            popwindow.dissmiss()
        }
        popwindow = CustomPopWindow.PopupWindowBuilder(this).setView(view)
            .size(AutoSizeUtils.dp2px(this, 200f), ViewGroup.LayoutParams.WRAP_CONTENT)
            .create()
    }

    private fun popwindow2() {
        var view = LayoutInflater.from(this).inflate(R.layout.layout_popwindow, null)
        mRecyclerView2 = view.findViewById(R.id.mRecyclerView)
        adapter2 = PackLebel2Adapter(arrayListOf(PackLebelBean("箱装", "100901"), PackLebelBean("盒装", "100902"), PackLebelBean("袋装", "100903"), PackLebelBean("其他", "100904")))
        mRecyclerView2.adapter = adapter2
        adapter2.setOnItemChildClickListener { adapter, view, position ->
            var data = adapter.data[position] as PackLebelBean
            requstAddPackParams.mode = data.value
            tvPackMethod.text = data.name
            popwindow2.dissmiss()
        }
        popwindow2 = CustomPopWindow.PopupWindowBuilder(this).setView(view)
            .size(AutoSizeUtils.dp2px(this, 200f), ViewGroup.LayoutParams.WRAP_CONTENT)
            .create()
    }
}