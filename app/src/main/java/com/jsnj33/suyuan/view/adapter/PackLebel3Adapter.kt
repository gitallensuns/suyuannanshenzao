package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.PackLebelBean
import com.jsnj33.suyuan.repository.model.PesticidesBean
import com.jsnj33.suyuan.repository.model.ReportBean

/**
 * @Author 萤火虫
 * @Date 2021/10/12
 * @Desc
 */
class PackLebel3Adapter(data: MutableList<ReportBean>?) : BaseQuickAdapter<ReportBean, BaseViewHolder>(R.layout.adapter_pack_lebel,data) {
    override fun convert(helper: BaseViewHolder, item: ReportBean?) {
        helper.setText(R.id.tvInfo,item?.dictionaryKey)
            .addOnClickListener(R.id.tvInfo)
    }
}