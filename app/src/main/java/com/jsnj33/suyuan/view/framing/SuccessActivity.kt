package com.jsnj33.suyuan.view.framing

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import cn.bingoogolapple.bgabanner.BGABanner
import com.amap.api.location.AMapLocation
import com.amap.api.maps2d.AMap
import com.amap.api.maps2d.LocationSource
import com.amap.api.maps2d.MapView
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.util.LogUtil
import kotlinx.android.synthetic.main.toolbar_header.*
import com.amap.api.location.AMapLocationClientOption.AMapLocationMode
import com.amap.api.location.AMapLocationClientOption
import com.amap.api.location.AMapLocationClient
import com.amap.api.location.AMapLocationListener
import com.amap.api.maps2d.model.MyLocationStyle
import com.amap.api.maps2d.CameraUpdateFactory
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.jsnj33.suyuan.repository.model.FramingDetailBean
import com.jsnj33.suyuan.view.adapter.SuccessAdapter
import com.jsnj33.suyuan.view.main.MainActivity
import com.jsnj33.suyuan.viewmodel.FramingViewModel
import com.leaf.library.StatusBarUtil
import com.jsnj33.suyuan.repository.client.RetrofitClient
import kotlinx.android.synthetic.main.activity_success.*
import java.util.*
import kotlin.collections.ArrayList


//提交成功页面
class SuccessActivity : AppCompatActivity(), LocationSource, AMapLocationListener {

    companion object {
        const val TAG = "SuccessActivity"
        fun start(activity: Activity, framingDetail: String, type: Int): Unit {
            var intent = Intent(activity, SuccessActivity::class.java)
            intent.putExtra("framingDetail", framingDetail)
            intent.putExtra("type", type)
            activity.startActivity(intent)
        }
    }

    lateinit var viewModel: FramingViewModel;
    var mMapView: MapView? = null
    var aMap: AMap? = null
    var type: Int = 0
    var framingDetail: FramingDetailBean? = null
    var mListener: LocationSource.OnLocationChangedListener? = null
    var mlocationClient: AMapLocationClient? = null
    var mLocationOption: AMapLocationClientOption? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)
        StatusBarUtil.setGradientColor(this, mMaterialCardView)
        viewModel = ViewModelProviders.of(this).get(FramingViewModel::class.java)
        mMapView = findViewById<MapView>(R.id.mMapView);
        mMapView?.onCreate(savedInstanceState)
        aMap = mMapView?.map
        aMap?.moveCamera(CameraUpdateFactory.zoomTo(17f))
        initData();
        watchListner()
    }

    fun initData(): Unit {
        viewModel.getDataList();
        tvTitle.text = "提交成功"
        type = intent.getIntExtra("type", 0)
        framingDetail = Gson().fromJson(
            intent.getStringExtra("framingDetail"),
            FramingDetailBean::class.java
        ) as FramingDetailBean
        if (type == 3) {
            llContent.visibility = View.VISIBLE
            mRecyclerView.visibility = View.GONE
        } else {
            llContent.visibility = View.GONE
            mRecyclerView.visibility = View.VISIBLE
            when (viewModel.traceIndustry) {
                10008 -> mRecyclerView.adapter =
                    SuccessAdapter(framingDetail?.traceProductionLivestockInputList)
                10009 -> mRecyclerView.adapter =
                    SuccessAdapter(framingDetail?.traceProductionFishInputList)
                else -> mRecyclerView.adapter =
                    SuccessAdapter(framingDetail?.traceProductionInputList)
            }
        }
        tvTime.text = "时间：" + framingDetail?.createDate
        when (type) {
            0 -> {
                tvFinishInfo.text = when (viewModel.traceIndustry) {
                    10008 -> {
                        "您今天完成投喂记录"
                    }
                    10009 -> {
                        "您今天完成投喂记录"
                    }
                    else -> {
                        "您今天完成农药记录"
                    }
                }
            }
            1 -> {
                tvFinishInfo.text = when (viewModel.traceIndustry) {
                    10008 -> {
                        "您今天完成投放药品记录"
                    }
                    10009 -> {
                        "您今天完成添加鱼苗记录"
                    }
                    else -> {
                        "您今天完成施肥记录"
                    }
                }

            }
            2 -> {
                tvFinishInfo.text = when (viewModel.traceIndustry) {
                    10008 -> {
                        "您今天完成投入幼崽记录"
                    }
                    10009 -> {
                        "您今天完成添加鱼药记录"
                    }
                    else -> {
                        "您今天完成种子记录"
                    }
                }

            }
            3 -> {
                tvFinishInfo.text = when (viewModel.traceIndustry) {
                    10008 -> {
                        "您今天完成养殖管理记录"
                    }
                    10009 -> {
                        "您今天完成渔业管理记录"
                    }
                    else -> {
                        "您今天完成农事生产记录"
                    }
                }
            }
        }
    }

    fun watchListner() {
        ivBack.setOnClickListener {
            MainActivity.start(this)
        }
        // 设置定位监听
        aMap?.setLocationSource(this)
        // 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        aMap?.isMyLocationEnabled = true;
        // 设置定位的类型为定位模式，有定位、跟随或地图根据面向方向旋转几种
        mBGABanner.setAdapter(BGABanner.Adapter<ImageView, String> { _, itemView, model, _ ->
            Glide.with(this)
                .load(model)
                .error(R.mipmap.ic_chayang)
                .centerCrop()
                .dontAnimate()
                .into(itemView)
        })
        var picList = ArrayList<String>()
        framingDetail?.filePath?.forEach {
            picList.add("${RetrofitClient.BASE_URL}${it}")
        }
        if (picList.isNotEmpty()) {
            mBGABanner.setData(picList, null)
        } else {
            mBGABanner.setData(Arrays.asList(R.mipmap.ic_headerbg), null)
        }
        viewModel.dataList.observe(this, androidx.lifecycle.Observer {
            it.forEach {
                when (viewModel.traceIndustry) {
                    10008 -> {
                        if (it.farmingCode == framingDetail!!.breedCode) {
                            tvType.text = it.name
                        }
                    }
                    10009 -> {
                        if (it.farmingCode == framingDetail!!.fishbreedCode) {
                            tvType.text = it.name
                        }
                    }
                    else -> {
                        if (it.farmingCode == framingDetail!!.farmingCode) {
                            tvType.text = it.name
                        }
                    }
                }

            }
        })
    }


    override fun deactivate() {
        mListener = null;
        if (mlocationClient != null) {
            mlocationClient?.stopLocation();
            mlocationClient?.onDestroy();
        }
        mlocationClient = null;
    }

    override fun activate(p0: LocationSource.OnLocationChangedListener?) {
        mListener = p0!!
        if (mlocationClient == null) {
            //初始化定位
            mlocationClient = AMapLocationClient(this)
            //初始化定位参数
            mLocationOption = AMapLocationClientOption()
            //设置定位回调监听
            mlocationClient?.setLocationListener(this)
            //设置为高精度定位模式
            mLocationOption?.setLocationMode(AMapLocationMode.Hight_Accuracy)
            //设置定位参数
            mlocationClient?.setLocationOption(mLocationOption)
            val myLocationStyle: MyLocationStyle
            myLocationStyle =
                MyLocationStyle()//初始化定位蓝点样式类myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE);//连续定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动。（1秒1次定位）如果不设置myLocationType，默认也会执行此种模式。
            myLocationStyle.interval(5000) //设置连续定位模式下的定位间隔，只在连续定位模式下生效，单次定位模式下不会生效。单位为毫秒。
            aMap?.setMyLocationStyle(myLocationStyle)//设置定位蓝点的Style
//aMap.getUiSettings().setMyLocationButtonEnabled(true);设置默认定位按钮是否显示，非必需设置。
            aMap?.setMyLocationEnabled(true)

            // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
            // 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
            // 在定位结束后，在合适的生命周期调用onDestroy()方法
            // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
            mlocationClient?.startLocation()//启动定位
        }
    }

    override fun onLocationChanged(mAMapLocation: AMapLocation?) {
        if (mListener != null && mlocationClient != null) {
            if (mlocationClient != null && mAMapLocation?.getErrorCode() == 0) {
                mListener!!.onLocationChanged(mAMapLocation)// 显示系统小蓝点
                LogUtil.e(TAG, mAMapLocation.address)
            } else {
                val errText =
                    "定位失败," + mAMapLocation?.getErrorCode() + ": " + mAMapLocation?.getErrorInfo()
                LogUtil.d(TAG, errText)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView?.onDestroy()
        mlocationClient?.onDestroy();
        viewModel.onCleared();
    }

    override fun onResume() {
        super.onResume()
        mMapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView?.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mMapView?.onSaveInstanceState(outState)
    }

}

