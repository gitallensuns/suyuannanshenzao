package com.jsnj33.suyuan.view.framing

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import com.bigkoo.pickerview.builder.OptionsPickerBuilder
import com.bigkoo.pickerview.listener.OnOptionsSelectListener
import com.bigkoo.pickerview.view.OptionsPickerView
import com.google.gson.Gson
import com.huantansheng.easyphotos.EasyPhotos
import com.huantansheng.easyphotos.models.album.entity.Photo
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.model.ListBean
import com.jsnj33.suyuan.repository.model.ProductList
import com.jsnj33.suyuan.repository.model.UploadIMGBean
import com.jsnj33.suyuan.repository.model.WHBean
import com.jsnj33.suyuan.util.GlideEngine
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.UploadIMGAdapter
import com.jsnj33.suyuan.viewmodel.FramingViewModel
import com.leaf.library.StatusBarUtil
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.mRecyclerView
import kotlinx.android.synthetic.main.activity_uploadimg.*
import kotlinx.android.synthetic.main.toolbar_header.*
import top.zibin.luban.Luban
import java.io.File
import java.lang.Exception


class UploadIMGActivity : BaseActivity<FramingViewModel>() {

    companion object {
        const val TAG = "TakePhotoActivity"
        fun start(
            activity: Activity,
            farmingCode: Int = 0,
            archId: String,
            productList: ArrayList<ProductList>? = null,
            type: Int,
            plantSeasonId: String,
        ): Unit {
            var intent = Intent(activity, UploadIMGActivity::class.java)
            intent.putExtra("farmingCode", farmingCode)
            intent.putExtra("archId", archId)
            intent.putExtra("type", type)
            intent.putExtra("plantSeasonId", plantSeasonId)
            intent.putParcelableArrayListExtra("productList", productList)
            activity.startActivity(intent)
        }
    }

    val REQUEST_TAKE_PHOTO = 101

    //适配器
    private lateinit var adapterUpload: UploadIMGAdapter;

    var farmingCode = 0
    var archId: String? = null
    var productList: ArrayList<ProductList>? = null
    var type = 0
    var addressDatas: List<ListBean>? = null
    var addressSelectPosition: Int = -1
    var phenologicalPhaseId: String = ""
    var phenologicalPhaseName: String = ""
    var inputId: String = ""
    var traceIndustry: Int by PreferencesUtil("traceIndustry", 10007)
    override fun getLayoutID(): Int = R.layout.activity_uploadimg

    override fun initLoadView() {
        StatusBarUtil.setGradientColor(this, mMaterialCardView)
        tvTitle.text = "请拍照上传"
        var plantSeasonId = intent.getStringExtra("plantSeasonId")
        viewModel.tracePlantSeason(plantSeasonId)
        adapterUpload = UploadIMGAdapter(null)
        mRecyclerView.also {
            it.setHasFixedSize(true)
            it.adapter = adapterUpload;
        }
        mStatusView.rootView = mRecyclerView

    }

    @Suppress("UNCHECKED_CAST")
    override fun loadData() {
        farmingCode = intent.getIntExtra("farmingCode", 0)
        archId = intent.getStringExtra("archId")
        type = intent.getIntExtra("type", 0)
        productList = intent.getSerializableExtra("productList") as ArrayList<ProductList>?
        productList?.removeAt((productList?.size!! - 1))
        viewModel.initPicControl()
        if (traceIndustry == 10007) {
            llChooseAddress.visibility = View.VISIBLE
            viewModel.getList(archId!!)
        }
    }

    override fun watchListner() {
        viewModel.tracePlantSeason.observe(this, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                viewModel.getTracePhenologicalPhase("${it.data?.plantCrop}")
            }
        })
        var list: ArrayList<WHBean>? = null
        viewModel.tracePhenologicalPhase.observe(this, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                list = it.data.records ?: arrayListOf()
                var nothList = ArrayList<String>()
                list!!.forEach { wh ->
                    nothList.add(wh.phenologicalPhaseName)
                }
                pvOptions2 = OptionsPickerBuilder(this@UploadIMGActivity,
                    OnOptionsSelectListener { options1, _, _, _ -> //返回的分别是三个级别的选中位置
                        try {
                            tvWhq.text = list?.get(options1)?.phenologicalPhaseName
                            phenologicalPhaseName = "${list?.get(options1)?.phenologicalPhaseName}"
                            phenologicalPhaseId = "${list?.get(options1)?.id}"
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }).build<String>()
                @Suppress("UNCHECKED_CAST")
                pvOptions2.setPicker(nothList as List<Nothing>?)
            }
        })

        viewModel.dataUploadList.observe(this, Observer {
            adapterUpload.setNewData(it)
        })

        llChooseAddress.setOnClickListener {
            pvOptions.show()
        }

        llWHQ.setOnClickListener {
            pvOptions2?.show()
        }

        adapterUpload.setOnItemChildClickListener { _, view, position ->
            when (view.id) {
                R.id.llAddIMG -> {
                    EasyPhotos.createAlbum(this, true, GlideEngine.getInstance())
                        .setFileProviderAuthority("$packageName.fileprovider")
                        .setCleanMenu(false)
                        .setPuzzleMenu(false)
                        .start(REQUEST_TAKE_PHOTO);
                }
                R.id.ivDeleteIMG ->
                    viewModel.deletePicture(position)
            }
        }
        ivBack.setOnClickListener {
            finish()
        }
        mBMaterialButton.setOnClickListener {
            if (phenologicalPhaseId.isNullOrEmpty()) {
                ToastUtil.show("请先选择物候期")
                return@setOnClickListener
            }

            if (viewModel.dataIMG?.size == 1) {
                ToastUtil.show("上传图片")
                return@setOnClickListener
            }
            mStatusView.handleLoading(true, "上传中...")
            viewModel.uploadIMG()
        }

        viewModel.productionMassif.observe(this@UploadIMGActivity, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                addressDatas = it.data.records
                initPicker()
            } else {
                ToastUtil.show("获取失败")
                mStatusView.handleSuccess()
            }
        })

        viewModel.livestockField.observe(this@UploadIMGActivity, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                addressDatas = it.data.records
                initPicker()
            } else {
                ToastUtil.show("获取失败")
                mStatusView.handleSuccess()
            }
        })
        viewModel.uploadIMGResult.observe(this@UploadIMGActivity, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                ToastUtil.show("上传成功")
                var massifName = try {
                    "${addressDatas?.get(addressSelectPosition)?.massifName}"
                } catch (e: Exception) {
                    null
                }
                var massifId = try {
                    "${addressDatas?.get(addressSelectPosition)?.massifId}"
                } catch (e: Exception) {
                    null
                }
                viewModel.addAppProductFarming(
                    it.message!!,
                    "$farmingCode",
                    archId!!,
                    massifName,
                    massifId,
                    productList,
                    phenologicalPhaseId,
                    phenologicalPhaseName
                )
            } else {
                ToastUtil.show("上传失败")
                mStatusView.handleSuccess()
            }
        })
        /**
         * 上传数据的监听
         */
        viewModel.framingDetail.observe(this@UploadIMGActivity, Observer {
            mStatusView.handleSuccess()
            if (it.code == ResultStatus.SUCCESS) {
                var data = Gson().toJson(it.data);
                SuccessActivity.start(this@UploadIMGActivity, data, type)
            } else {
                ToastUtil.show(it.message!!)
            }
        })
    }


    private lateinit var pvOptions: OptionsPickerView<*>
    private lateinit var pvOptions2: OptionsPickerView<*>

    private fun initPicker() {
        //条件选择器
        pvOptions =
            OptionsPickerBuilder(this@UploadIMGActivity,
                OnOptionsSelectListener { options1, _, _, _ -> //返回的分别是三个级别的选中位置
                    try {
                        tvAddress.text = addressDatas?.get(options1)?.showName
                        addressSelectPosition = options1
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }).build<String>()
        @Suppress("UNCHECKED_CAST")
        pvOptions.setPicker(getAddressItems() as List<Nothing>?)
    }

    private fun getAddressItems(): List<String> {
        var addressItems = ArrayList<String>()
        addressDatas!!.forEach {
            addressItems.add(it.showName)
        }
        return addressItems
    }

    @Suppress("UNCHECKED_CAST")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            //拍照
            REQUEST_TAKE_PHOTO -> {
                val resultPhotos =
                    data!!.getParcelableArrayListExtra<Parcelable>(EasyPhotos.RESULT_PHOTOS) as ArrayList<Photo>
                var resultPath = arrayListOf<String>()
                resultPath.clear()
                resultPhotos.map {
                    resultPath.add(it.path)
                }
                //鲁班压缩图片
                Flowable.just(resultPath)
                    .observeOn(Schedulers.io())
                    .map(object : Function<ArrayList<String>>,
                        io.reactivex.functions.Function<ArrayList<String>, ArrayList<File>> {
                        override fun apply(t: ArrayList<String>?): ArrayList<File> {
                            return Luban.with(this@UploadIMGActivity).load(t)
                                .get() as ArrayList<File>
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(Consumer {
                        it.forEach {
                            viewModel.addPicture(
                                UploadIMGBean(
                                    (Uri.fromFile(it)),
                                    UploadIMGAdapter.PICTAG
                                )
                            )
                        }
                    })
            }
        }
    }
}