package com.jsnj33.suyuan.view.record

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.TransportAdapter
import com.jsnj33.suyuan.viewmodel.PackViewModel
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import kotlinx.android.synthetic.main.activity_transport.*
import kotlinx.android.synthetic.main.layout_refresh.*

/**
 * 运输管理
 */
class TransportActivity : BaseActivity<PackViewModel>() {
    companion object {
        @JvmStatic
        fun start(context: Context, processStatus: Int) {
            val starter = Intent(context, TransportActivity::class.java)
                .putExtra("processStatus", processStatus)
            context.startActivity(starter)
        }
    }
    var processStatus: Int = 10000
    var page = 1
    lateinit var mTransportAdapter: TransportAdapter
    override fun getLayoutID(): Int = R.layout.activity_transport

    override fun initLoadView() {
        var status = intent.getIntExtra("processStatus", 10000)
        mTransportAdapter = TransportAdapter(R.layout.adapter_transport, null)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.adapter = mTransportAdapter
        if (status == 10007) {
            tvZS.visibility = View.GONE
        } else {
            tvZS.visibility = View.VISIBLE
        }
        mStatusView.handleLoading()
    }

    override fun onResume() {
        super.onResume()
        mRecyclerView.smoothScrollToPosition(0)
        viewModel.getDataTansPortList(page, processStatus)
    }
    override fun loadData() {
        viewModel.transportList.observe(this, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                if (page == 1) {
                    mSmartRefreshLayout.finishRefresh()
                    mTransportAdapter.setNewData(it.data.records)
                    if (it.data.total == 0) {
                        mStatusView.handleEmpty()
                    } else {
                        mStatusView.handleSuccess()
                    }
                    if (it.data.total < 15) {
                        mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                    }
                    return@Observer
                }
                if (it.data.isNextPage) {
                    mSmartRefreshLayout.finishLoadMore()
                } else {
                    mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                }
                mTransportAdapter.addData(it.data.records)
            } else {
                ToastUtil.show("${it.message}")
                mStatusView.handleEmpty()
                mSmartRefreshLayout.finishRefresh()
                mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
            }
        })
        ivBack.setOnClickListener {
            finish()
        }
    }

    override fun watchListner() {
        rgContent.check(R.id.tvZC)
        rgContent.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.tvZC -> {
                    processStatus = 10000
                    mTransportAdapter.processStatus = "$processStatus"
                    page = 1
                    mSmartRefreshLayout.setNoMoreData(false)
                    viewModel.getDataTansPortList(page, processStatus)
                }
                R.id.tvZS -> {
                    processStatus = 10001
                    mTransportAdapter.processStatus = "$processStatus"
                    page = 1
                    mSmartRefreshLayout.setNoMoreData(false)
                    viewModel.getDataTansPortList(page, processStatus)
                }
            }
        }
        mSmartRefreshLayout.setOnRefreshLoadMoreListener(object : OnRefreshLoadMoreListener {
            override fun onRefresh(refreshLayout: RefreshLayout) {
                page = 1
                viewModel.getDataTansPortList(page, processStatus)
            }

            override fun onLoadMore(refreshLayout: RefreshLayout) {
                page += 1
                viewModel.getDataTansPortList(page, processStatus)
            }
        })
        mTransportAdapter.setOnItemChildClickListener { adapter, view, position ->
            var data = adapter.data[position]
            TransportDetailActivity.start(this, Gson().toJson(data), "$processStatus")
        }
        ivAdd.setOnClickListener {
            AddTransportActivity.start(this, "$processStatus")
        }
    }
}