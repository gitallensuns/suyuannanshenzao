package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.TraceProductionBean

class RecordAdapter(data: List<TraceProductionBean>, val type: Int) :
    BaseQuickAdapter<TraceProductionBean, BaseViewHolder>(

        R.layout.adapter_record, data
    ) {
    override fun convert(helper: BaseViewHolder, item: TraceProductionBean?) {
        when (type) {
            1 -> {
                helper.setText(R.id.tvAdaName, "施肥方式")
                    .setText(R.id.tvAdaNameValue, item!!.inputName)
                    .setText(R.id.tvAdaName1, "使用量")
                    .setText(R.id.tvAdaNameValue1, "${item.inputConsum}${item.inputUnit}")
            }
            2 -> {
                helper.setText(R.id.tvAdaName, "化肥名称")
                    .setText(R.id.tvAdaNameValue, item!!.inputName)
                    .setText(R.id.tvAdaName1, "使用量")
                    .setText(R.id.tvAdaNameValue1, "${item.inputConsum}${item.inputUnit}")
            }
            3 -> {
                helper.setText(R.id.tvAdaName, "种子品种")
                    .setText(R.id.tvAdaNameValue, item!!.inputName)
                    .setText(R.id.tvAdaName1, "投入量")
                    .setText(R.id.tvAdaNameValue1, "${item.inputConsum}${item.inputUnit}")
            }
        }
    }

}