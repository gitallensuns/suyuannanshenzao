package com.jsnj33.suyuan.view.record

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.PackAdapter
import com.jsnj33.suyuan.view.pesticides.AddpesticidesActivity
import com.jsnj33.suyuan.viewmodel.PackViewModel
import com.jsnj33.suyuan.viewmodel.RecordViewModel
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import kotlinx.android.synthetic.main.activity_pack.*
import kotlinx.android.synthetic.main.layout_refresh.*

/**
 *@Author 萤火虫
 *@Date  2021/10/11
 *@Desc 包装加工
 */
class PackActivity : BaseActivity<PackViewModel>() {
    companion object {
        @JvmStatic
        fun start(context: Context, processStatus: Int) {
            val starter = Intent(context, PackActivity::class.java)
                .putExtra("processStatus", processStatus)
            context.startActivity(starter)
        }
    }

    override fun getLayoutID(): Int = R.layout.activity_pack
    var processStatus: Int = 10000
    var page = 1
    lateinit var packAdapter: PackAdapter
    override fun initLoadView() {
        var status = intent.getIntExtra("processStatus", 10000)
        packAdapter = PackAdapter(R.layout.adapter_pack, null)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.adapter = packAdapter
        if (status == 10007) {
            tvZS.visibility = View.GONE
        } else {
            tvZS.visibility = View.VISIBLE
        }
        mStatusView.handleLoading()

    }

    override fun onResume() {
        super.onResume()
        mRecyclerView.smoothScrollToPosition(0)
        viewModel.getDataList(page, processStatus)
    }

    override fun loadData() {

        viewModel.packList.observe(this, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                /**
                 * true还有下一页
                 * false 没有下一页了
                 */
                if (page == 1) {
                    mSmartRefreshLayout.finishRefresh()
                    packAdapter.setNewData(it.data.records)
                    if (it.data.total == 0) {
                        mStatusView.handleEmpty()
                    } else {
                        mStatusView.handleSuccess()
                    }
                    if (it.data.total < 15) {
                        mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                    }
                    return@Observer
                }
                if (it.data.isNextPage) {
                    mSmartRefreshLayout.finishLoadMore()
                } else {
                    mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                }
                packAdapter.addData(it.data.records)
            } else {
                ToastUtil.show("${it.message}")
                mStatusView.handleEmpty()
                mSmartRefreshLayout.finishRefresh()
                mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
            }
        })
        ivBack.setOnClickListener {
            finish()
        }
    }

    override fun watchListner() {
        rgContent.check(R.id.tvZC)
        rgContent.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.tvZC -> {
                    processStatus = 10000
                    packAdapter.processStatus = "$processStatus"
                    page = 1
                    mSmartRefreshLayout.setNoMoreData(false)
                    viewModel.getDataList(page, processStatus)

                }
                R.id.tvZS -> {
                    processStatus = 10001
                    packAdapter.processStatus = "$processStatus"
                    page = 1
                    mSmartRefreshLayout.setNoMoreData(false)
                    viewModel.getDataList(page, processStatus)
                }
            }
        }
        mSmartRefreshLayout.setOnRefreshLoadMoreListener(object : OnRefreshLoadMoreListener {
            override fun onRefresh(refreshLayout: RefreshLayout) {
                page = 1
                viewModel.getDataList(page, processStatus)
            }

            override fun onLoadMore(refreshLayout: RefreshLayout) {
                page += 1
                viewModel.getDataList(page, processStatus)
            }
        })
        packAdapter.setOnItemChildClickListener { adapter, view, position ->
            var data = adapter.data[position]
            PackDetailActivity.start(this, Gson().toJson(data), "$processStatus")
        }
        ivAdd.setOnClickListener {
            AddPackActivity.start(this, "$processStatus")
        }
    }
}