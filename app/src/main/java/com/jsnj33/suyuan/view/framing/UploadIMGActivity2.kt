package com.jsnj33.suyuan.view.framing

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.bigkoo.pickerview.builder.OptionsPickerBuilder
import com.bigkoo.pickerview.listener.OnOptionsSelectListener
import com.bigkoo.pickerview.view.OptionsPickerView
import com.google.gson.Gson
import com.huantansheng.easyphotos.EasyPhotos
import com.huantansheng.easyphotos.models.album.entity.Photo
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.model.ListBean
import com.jsnj33.suyuan.repository.model.ProductList
import com.jsnj33.suyuan.repository.model.UploadIMGBean
import com.jsnj33.suyuan.util.GlideEngine
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.UploadIMGAdapter
import com.jsnj33.suyuan.viewmodel.FramingViewModel
import com.leaf.library.StatusBarUtil
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.mRecyclerView
import kotlinx.android.synthetic.main.activity_uploadimg.*
import kotlinx.android.synthetic.main.toolbar_header.*
import top.zibin.luban.Luban
import java.io.File
import java.lang.Exception
import kotlin.math.max


class UploadIMGActivity2 : BaseActivity<FramingViewModel>() {

    companion object {
        const val TAG = "TakePhotoActivity"
        fun start(
            activity: Activity,
            type: Int, uploadatas: ArrayList<UploadIMGBean>? = null, maxSize: Int? = 4
        ) {
            var intent = Intent(activity, UploadIMGActivity2::class.java)
            intent.putExtra("type", type)
            intent.putExtra("maxSize", maxSize)
            if (uploadatas.isNullOrEmpty().not()) {
                var bundle = Bundle()
                bundle.putSerializable("uploadatas", uploadatas)
                intent.putExtras(bundle)
            }
            activity.startActivityForResult(intent, 1)
        }
    }

    val REQUEST_TAKE_PHOTO = 101

    //适配器
    private lateinit var adapterUpload: UploadIMGAdapter;

    var type = 0
    var traceIndustry: Int by PreferencesUtil("traceIndustry", 10007)
    override fun getLayoutID(): Int = R.layout.activity_uploadimg2

    var uploadList: ArrayList<UploadIMGBean>? = null
    var maxSize = 0
    override fun initLoadView() {
        StatusBarUtil.setGradientColor(this, mMaterialCardView)
        tvTitle.text = "请拍照上传"
        maxSize = intent.getIntExtra("maxSize", 9)
        uploadList = try {
            intent?.getSerializableExtra("uploadatas") as ArrayList<UploadIMGBean>
        } catch (e: Exception) {
            null
        }
        if (uploadList.isNullOrEmpty()) {
            adapterUpload = UploadIMGAdapter(null)
            viewModel.initPicControl()
        } else {
            adapterUpload = UploadIMGAdapter(uploadList)
            viewModel.dataIMG = uploadList
        }
        mRecyclerView.also {
            it.setHasFixedSize(true)
            it.adapter = adapterUpload;
        }
        mStatusView.rootView = mRecyclerView
        adapterUpload.maxSize = maxSize

    }

    @Suppress("UNCHECKED_CAST")
    override fun loadData() {
        type = intent.getIntExtra("type", 0)
    }

    override fun watchListner() {
        viewModel.dataUploadList.observe(this, Observer {
            adapterUpload.setNewData(it)
        })

        adapterUpload.setOnItemChildClickListener { _, view, position ->
            when (view.id) {
                R.id.llAddIMG -> {
                    EasyPhotos.createAlbum(this, true, GlideEngine.getInstance())
                        .setFileProviderAuthority("$packageName.fileprovider")
                        .setCount(maxSize)
                        .setCleanMenu(false)
                        .setPuzzleMenu(false)
                        .start(REQUEST_TAKE_PHOTO);
                }
                R.id.ivDeleteIMG ->
                    viewModel.deletePicture(position)
            }
        }
        ivBack.setOnClickListener {
            finish()
        }
        /**
         * 上传数据的监听
         */
        viewModel.framingDetail.observe(this@UploadIMGActivity2, Observer {
            mStatusView.handleSuccess()
            if (it.code == ResultStatus.SUCCESS) {
                var data = Gson().toJson(it.data);
                SuccessActivity.start(this@UploadIMGActivity2, data, type)
            } else {
                ToastUtil.show(it.message!!)
            }
        })
        mBMaterialButton.setOnClickListener {
            var data = adapterUpload.data as ArrayList<UploadIMGBean>
            var bundle = Bundle()
            bundle.putSerializable("uploadGson", data)
            var intent = Intent()
            intent.putExtras(bundle)
            setResult(RESULT_OK, intent)
            finish()
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            //拍照
            REQUEST_TAKE_PHOTO -> {
                val resultPhotos =
                    data!!.getParcelableArrayListExtra<Parcelable>(EasyPhotos.RESULT_PHOTOS) as ArrayList<Photo>
                var resultPath = arrayListOf<String>()
                resultPath.clear()
                resultPhotos.map {
                    resultPath.add(it.path)
                }
                //鲁班压缩图片
                Flowable.just(resultPath)
                    .observeOn(Schedulers.io())
                    .map(object : Function<ArrayList<String>>,
                        io.reactivex.functions.Function<ArrayList<String>, ArrayList<File>> {
                        override fun apply(t: ArrayList<String>?): ArrayList<File> {
                            return Luban.with(this@UploadIMGActivity2).load(t)
                                .get() as ArrayList<File>
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(Consumer { it ->
                        it.forEach {
                            viewModel.addPicture(
                                UploadIMGBean(
                                    (Uri.fromFile(it)),
                                    UploadIMGAdapter.PICTAG
                                )
                            )

                        }
                    })
            }
        }
    }
}