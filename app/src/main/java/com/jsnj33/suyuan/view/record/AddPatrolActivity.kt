package com.jsnj33.suyuan.view.record

import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.bigkoo.pickerview.builder.TimePickerBuilder
import com.bigkoo.pickerview.view.TimePickerView
import com.example.zhouwei.library.CustomPopWindow
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.model.AddPatrolParams
import com.jsnj33.suyuan.repository.model.MassifBean
import com.jsnj33.suyuan.repository.model.ReportBean
import com.jsnj33.suyuan.repository.model.UploadIMGBean
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.util.TimeUtils
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.PackLebel3Adapter
import com.jsnj33.suyuan.view.adapter.PackLebel4Adapter
import com.jsnj33.suyuan.view.adapter.PackLebel5Adapter
import com.jsnj33.suyuan.view.framing.UploadIMGActivity2
import com.jsnj33.suyuan.viewmodel.PatrolViewModel
import kotlinx.android.synthetic.main.activity_add_patrol.*
import kotlinx.android.synthetic.main.activity_add_patrol.tvPic
import kotlinx.android.synthetic.main.activity_add_patrol.tvTime
import kotlinx.android.synthetic.main.toolbar_header_right.*
import me.jessyan.autosize.utils.AutoSizeUtils
import java.util.*
import kotlin.collections.ArrayList

/**
 *
 */
class AddPatrolActivity : BaseActivity<PatrolViewModel>() {
    override fun getLayoutID(): Int = R.layout.activity_add_patrol

    var personName: String? by PreferencesUtil("personName", "")
    var id: String? by PreferencesUtil("id", "")

    companion object {
        private const val TAG = "AddPatrolActivity"

        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, AddPatrolActivity::class.java)
            context.startActivity(starter)
        }
    }

    private val patrolParams: AddPatrolParams by lazy {
        AddPatrolParams()
    }

    private var timePicker: TimePickerView? = null

    lateinit var dialogMassif: MaterialDialog

    override fun initLoadView() {
        tvTitle.text = "添加巡田"
        viewModel.getPatrolUpload()
        viewModel.getPatrolMassif()
        patrolParams.id = id
        patrolParams.updateBy = id
        patrolParams.createBy = id
        patrolParams.fieldPerson = personName
        tvFeidPerson.text = "$personName"
        initChooseTime()
        popwindow()
        popwindowFarmer()
        initMassifDialog()
        mStatusView.handleSuccess()
    }

    override fun loadData() {

    }

    var massifList: ArrayList<MassifBean>? = null
    override fun watchListner() {
        viewModel.apply {
            this.reportList.observe(this@AddPatrolActivity, androidx.lifecycle.Observer {
                if (it.code == ResultStatus.SUCCESS) {
                    adapter?.setNewData(it.data)
                }
            })
            this.massifList.observe(this@AddPatrolActivity, androidx.lifecycle.Observer {
                if (it.code == ResultStatus.SUCCESS) {
                    this@AddPatrolActivity.massifList = it.data
                    adapterMassif?.setNewData(it.data)
                }
            })
            addPatrol.observe(this@AddPatrolActivity, androidx.lifecycle.Observer {
                mStatusView.handleSuccess()
                if (it.code == ResultStatus.SUCCESS) {
                    ToastUtil.show("添加成功")
                    finish()
                } else {
                    ToastUtil.show("${it.message}")
                }
            })
            uploadIMGResult.observe(this@AddPatrolActivity, androidx.lifecycle.Observer {
                if (it.code == ResultStatus.SUCCESS) {
                    ToastUtil.show("上传成功")
                    var stringBuffer = StringBuffer("")
                    with(it.data) {
                        this?.forEachIndexed { index, uploadIMGBean2 ->
                            if (index == (this.size - 1)) {
                                stringBuffer.append("${uploadIMGBean2.filePath + uploadIMGBean2.fileName}")
                            } else {
                                stringBuffer.append("${uploadIMGBean2.filePath + uploadIMGBean2.fileName},")
                            }
                        }
                        patrolParams.fieldPic = stringBuffer.toString()
                    }
                    viewModel.addPatrol(this@AddPatrolActivity.patrolParams)
                } else {
                    ToastUtil.show("上传失败")
                }
            })
        }
        btnCommit.setOnClickListener {
            if (ver()) {
                mStatusView.handleLoading(true)
                viewModel.uploadIMG(uploadList!!)
            }
        }

        tvTime.setOnClickListener {
            timePicker?.show()
        }
        tvUploadInfo.setOnClickListener {
            popwindow?.showAsDropDown(tvUploadInfo, 0, 10, Gravity.RIGHT)
        }
        tvFarmer.setOnClickListener {
            popwindowFarmer?.showAsDropDown(tvFarmer, 0, 10, Gravity.RIGHT)
        }
        tvPic.setOnClickListener {
            UploadIMGActivity2.start(this, 0, uploadList, 1)
        }
        tvPatrolArea.setOnClickListener {
            dialogMassif.show()
        }
        ivBack.setOnClickListener {
            finish()
        }
    }

    private fun ver(): Boolean {
        if (tvUploadInfo.text.isNullOrEmpty()) {
            ToastUtil.show("请选择上报信息")
            return false
        }
        if (tvTime.text.isNullOrEmpty()) {
            ToastUtil.show("请选择巡田日期")
            return false
        }
        if (tvPatrolArea.text.isNullOrEmpty()) {
            ToastUtil.show("请选择巡田地块")
            return false
        }

        if (uploadList.isNullOrEmpty()) {
            ToastUtil.show("请上传巡田图片")
            return false
        }

        patrolParams.remarks = etRemark.text.toString()
        return true
    }

    private var uploadList: ArrayList<UploadIMGBean>? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == RESULT_OK) {
            try {
                uploadList = data?.getSerializableExtra("uploadGson") as java.util.ArrayList<UploadIMGBean>
                if (uploadList.isNullOrEmpty() || (uploadList?.size!! - 1 == 0)) {
                    tvPic.text = ""
                } else {
                    tvPic.text = "已选择"
                }

            } catch (e: Exception) {
                Log.d(TAG, "onActivityResult: ${e.message}")
            }
        }
    }


    private fun initChooseTime() {
        val selectedDate = Calendar.getInstance()
        selectedDate.time = Date()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker = TimePickerBuilder(this) { date, v -> //选中事件回调
                tvTime.text = TimeUtils.date2Str(date, TimeUtils.FORMAT_YMDHMS)
                patrolParams.fieldTime = TimeUtils.date2Str(date, TimeUtils.FORMAT_YMDHMS)
            }
                .setType(booleanArrayOf(true, true, true, true, true, false)) // 默认全部显示
                .setCancelText("取消") //取消按钮文字
                .setSubmitText("确定") //确认按钮文字
                .setSubCalSize(15)
                .setTitleSize(18) //标题文字大小
                .setTitleText("时间选择") //标题文字
                .setOutSideCancelable(false) //点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(false) //是否循环滚动
                .setTitleColor(getColor(R.color.colorPrimary)) //标题文字颜色
                .setSubmitColor(getColor(R.color.colorPrimary)) //确定按钮文字颜色
                .setCancelColor(getColor(R.color.colorPrimary)) //取消按钮文字颜色
                .setTitleBgColor(getColor(R.color.colorWhite)) //标题背景颜色 Night mode
                .setBgColor(getColor(R.color.colorWhite)) //滚轮背景颜色 Night mode
                .setDate(selectedDate) // 如果不设置的话，默认是系统时间*/
//                .setRangDate(startDate, selectedDate) //起始终止年月日设定
                .setLabel("年", "月", "日", "时", "分", "") //默认设置为年月日时分秒
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .isDialog(false) //是否显示为对话框样式
                .build()
        }
    }


    var adapter: PackLebel3Adapter? = null
    private var popwindow: CustomPopWindow? = null
    private fun popwindow() {
        var view = LayoutInflater.from(this).inflate(R.layout.layout_popwindow, null)
        var mRecyclerView = view.findViewById<RecyclerView>(R.id.mRecyclerView)
        adapter = PackLebel3Adapter(null)
        mRecyclerView.adapter = adapter
        adapter?.setOnItemChildClickListener { adapter, view, position ->
            var data = adapter.data[position] as ReportBean
            tvUploadInfo.text = "${data.dictionaryKey}"
            patrolParams.messageReportId = data.id
            popwindow?.dissmiss()
        }
        popwindow = CustomPopWindow.PopupWindowBuilder(this).setView(view)
            .size(AutoSizeUtils.dp2px(this, 200f), ViewGroup.LayoutParams.WRAP_CONTENT)
            .create()
    }

    var adapterFarmer: PackLebel4Adapter? = null
    private var popwindowFarmer: CustomPopWindow? = null
    private fun popwindowFarmer() {
        var view = LayoutInflater.from(this).inflate(R.layout.layout_popwindow, null)
        var mRecyclerView = view.findViewById<RecyclerView>(R.id.mRecyclerView)
        adapterFarmer = PackLebel4Adapter(arrayListOf("南沈灶农场", "头灶农场"))
        mRecyclerView.adapter = adapterFarmer
        adapterFarmer?.setOnItemChildClickListener { adapter, view, position ->
            tvFarmer.text = "${adapter.data.get(position)}"
            patrolParams.fieldFarm = (position + 1).toString()
            popwindowFarmer?.dissmiss()
        }
        popwindowFarmer = CustomPopWindow.PopupWindowBuilder(this).setView(view)
            .size(AutoSizeUtils.dp2px(this, 200f), ViewGroup.LayoutParams.WRAP_CONTENT)
            .create()

    }

    var adapterMassif: PackLebel5Adapter? = null

    private fun initMassifDialog() {
        dialogMassif = MaterialDialog(this)
        dialogMassif.customView(viewRes = R.layout.layout_popwindow, noVerticalPadding = true)
        var mRecyclerView = dialogMassif.getCustomView().findViewById<RecyclerView>(R.id.mRecyclerView)
        adapterMassif = PackLebel5Adapter(null)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.adapter = adapterMassif
        dialogMassif.positiveButton(text = "确定") {
            var stringBuffer = StringBuffer("")
            var stringBuffer2 = StringBuffer("")
            adapterMassif?.data?.forEach {
                if (it.isCheck == true) {
                    stringBuffer.append(",${it.id}")
                    stringBuffer2.append(",${it.massifName}")
                }
            }
            try {
                tvPatrolArea.text = "${stringBuffer2.substring(1, stringBuffer2.length)}"
                patrolParams.fieldPlotId = stringBuffer.substring(1, stringBuffer.length)
            } catch (e: Exception) {
                tvPatrolArea.text = ""
                patrolParams.fieldPlotId = ""
            }

        }
    }

}