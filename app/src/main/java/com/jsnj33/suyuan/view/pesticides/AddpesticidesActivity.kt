package com.jsnj33.suyuan.view.pesticides

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.Observer
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.model.PesticidesBean
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.util.StringUtils
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.PesticidesAdapter
import com.jsnj33.suyuan.view.framing.AddFramingActivity
import com.jsnj33.suyuan.viewmodel.PesticidesViewModel
import com.leaf.library.StatusBarUtil
import kotlinx.android.synthetic.main.activity_addpesticides.*
import kotlinx.android.synthetic.main.toolbar_header.*

/**
 * 添加农药
 */
class AddpesticidesActivity : BaseActivity<PesticidesViewModel>() {
    companion object {
        const val TAG = "AddpesticidesActivity"
        fun start(activity: Activity, type: Int, farmingCode: Int): Unit {
            val intent = Intent(activity, AddpesticidesActivity::class.java)
            intent.putExtra("type", type)
            intent.putExtra("farmingCode", farmingCode)
            activity.startActivity(intent)
        }
    }

    lateinit var pesticidesAdapter: PesticidesAdapter;
    var personName: String? by PreferencesUtil("personName", "")

    override fun getLayoutID(): Int = R.layout.activity_addpesticides
    var type: Int = 0//0添加农药、1添加肥料、2添加种子
    var farmingCode: Int = 0
    override fun initLoadView() {
        StatusBarUtil.setGradientColor(this, mMaterialCardView)
        type = intent.getIntExtra("type", 0);
        farmingCode = intent.getIntExtra("farmingCode", 0)
        pesticidesAdapter = PesticidesAdapter(R.layout.adapter_pesticides, null)
        mRecyclerView.adapter = pesticidesAdapter
    }

    override fun loadData() {
        mStatusView.rootView = mRecyclerView
        mStatusView.handleLoading()
        tvTitle.text = StringUtils.getAddTitle(farmingCode)
        viewModel.getDataList(personName + "");

    }

    override fun watchListner() {
        viewModel.dataList.observe(this@AddpesticidesActivity, Observer {
            mStatusView.handleSuccess()
            if (it.code == ResultStatus.SUCCESS) pesticidesAdapter.setNewData(it?.data)
            else ToastUtil.show(it.message ?: it.msg ?: "网络异常")
        })
        pesticidesAdapter.setOnItemChildClickListener { adapter, _, position ->
            var mPesticidesBean = adapter.data.get(position) as PesticidesBean;
            when (type) {
                4 -> ProductionAddActivity.start(
                    this@AddpesticidesActivity, mPesticidesBean.id, mPesticidesBean.enterNumber, "${mPesticidesBean.archiBatch}",
                    mPesticidesBean.productName, mPesticidesBean.personCharge
                )
                3 -> AddFramingActivity.start(this@AddpesticidesActivity, mPesticidesBean.id!!, type,mPesticidesBean.plantSeasonId)
                else -> ChooseActivity.start(this@AddpesticidesActivity, farmingCode, mPesticidesBean.id!!, type,mPesticidesBean.plantSeasonId)
            }
        }
        ivBack.setOnClickListener {
            finish()
        }
    }

}
