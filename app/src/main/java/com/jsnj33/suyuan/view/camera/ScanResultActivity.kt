package com.jsnj33.suyuan.view.camera

import android.app.Activity
import android.content.Intent
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.model.ScanEntity
import com.jsnj33.suyuan.viewmodel.ScanModel
import com.jsnj33.suyuan.repository.client.RetrofitClient
import kotlinx.android.synthetic.main.activity_scan_result.*
import kotlinx.android.synthetic.main.toolbar_header.*

class ScanResultActivity : BaseActivity<ScanModel>() {
    companion object {
        fun start(activity: Activity, scanInfo: String): Unit {
            var intent = Intent(activity, ScanResultActivity::class.java)
            intent.putExtra("scanInfo", scanInfo)
            activity.startActivity(intent)
        }
    }

    lateinit var scanEntity: ScanEntity


    override fun loadData() {
        tvName.text = scanEntity.name!!
        tvInfo.text = scanEntity.quality!!
    }

    override fun watchListner() {
        btnConfirm.setOnClickListener {
            finish()
        }
        ivBack.setOnClickListener {
            finish()
        }
    }

    override fun getLayoutID(): Int = R.layout.activity_scan_result

    override fun initLoadView() {
        tvTitle.text = "OCR识别结果"
        var infoStr = intent.getStringExtra("scanInfo")
        scanEntity = Gson().fromJson(infoStr, ScanEntity::class.java)
        Glide.with(this).load("${RetrofitClient.BASE_URL + scanEntity.filePath}").into(ivIMG)
    }

}
