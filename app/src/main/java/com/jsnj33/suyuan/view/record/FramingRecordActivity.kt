package com.jsnj33.suyuan.view.record

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.model.FramingDetailBean
import com.jsnj33.suyuan.repository.model.TraceProductionBean
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.model.AddProductBean
import com.jsnj33.suyuan.util.StringUtils
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.FramingRecordAdapter
import com.jsnj33.suyuan.view.pesticides.AddpesticidesActivity
import com.jsnj33.suyuan.viewmodel.RecordViewModel
import com.leaf.library.StatusBarUtil
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import kotlinx.android.synthetic.main.activity_choose.mRecyclerView
import kotlinx.android.synthetic.main.activity_framing_record.*
import kotlinx.android.synthetic.main.layout_refresh.*
import kotlinx.android.synthetic.main.toolbar_header.tvTitle
import kotlinx.android.synthetic.main.toolbar_header_right.*
import kotlinx.android.synthetic.main.toolbar_header_right.ivBack
import kotlinx.android.synthetic.main.toolbar_header_right.mMaterialCardView

/**
 * 农事记录
 */
class FramingRecordActivity : BaseActivity<RecordViewModel>() {

    companion object {
        fun start(activity: Activity, type: Int, farmingCode: Int): Unit {
            var intent = Intent(activity, FramingRecordActivity::class.java)
            intent.putExtra("type", type)
            intent.putExtra("farmingCode", farmingCode)
            activity.startActivity(intent)
        }
    }

    lateinit var framingRecordAdapter: FramingRecordAdapter
    var type: Int = 0;//0：农事记录、1：肥料记录、2：农药记录；3：种子记录,9:生产记录
    var page = 1
    var sort: Int? = 0//传1 代表农事生产(其余则不传)
    var farmingCode = 0//100102 肥料，100103农药 100100种子
    override fun getLayoutID(): Int = R.layout.activity_framing_record

    override fun initLoadView() {
        StatusBarUtil.setGradientColor(this, mMaterialCardView)
        type = intent.getIntExtra("type", 0)
        farmingCode = intent.getIntExtra("farmingCode", 0)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        framingRecordAdapter = FramingRecordAdapter(R.layout.adapter_record, null, type)
        mRecyclerView.adapter = framingRecordAdapter
        mStatusView.rootView = mSmartRefreshLayout
        mStatusView.handleLoading()
    }

    override fun loadData() {
        when (type) {
            0 -> {
                sort = 0
                tvTitle.text = ""
            }
            9 -> {
                sort = 9
                tvTitle.text = "生产加工记录"
            }
            else -> {
                sort = 0
                tvTitle.text = StringUtils.getRecordTitle(viewModel.traceIndustry, farmingCode)
            }
        }
        viewModel.getData(sort, farmingCode, page, type)
    }

    override fun watchListner() {
        mSmartRefreshLayout.setOnRefreshLoadMoreListener(object : OnRefreshLoadMoreListener {
            override fun onLoadMore(refreshLayout: RefreshLayout) {
                page++
                viewModel.getData(sort, farmingCode, page, type)
            }

            override fun onRefresh(refreshLayout: RefreshLayout) {
                page = 1
                viewModel.getData(sort, farmingCode, page, type)
            }
        })
        viewModel.framingList.observe(this, Observer { it ->
            with(it)
            {
                mStatusView.handleSuccess()
                if (it.code == ResultStatus.SUCCESS) {
                    /**
                     * true还有下一页
                     * false 没有下一页了
                     */
                    if (page == 1) {
                        mSmartRefreshLayout.finishRefresh()
                        framingRecordAdapter.setNewData(it.data.records as List<Any>?)
                        if (it.data.total == 0) {
                            mStatusView.handleEmpty()
                        }
                        if (data.total < 15) {
                            mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                        }
                        return@Observer
                    }
                    if (it.data.isNextPage) {
                        mSmartRefreshLayout.finishLoadMore()
                    } else {
                        mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                    }
                    framingRecordAdapter.addData(it.data.records as List<Any>)
                } else {
                    mStatusView.handleEmpty()
                    ToastUtil.show("" + it.message)
                    mSmartRefreshLayout.finishRefresh()
                    mSmartRefreshLayout.finishLoadMore()
                }

            }
        })
        viewModel.otherList.observe(this, Observer { it ->
            with(it)
            {
                mStatusView.handleSuccess()
                if (it.code == ResultStatus.SUCCESS) {
                    /**
                     * true还有下一页
                     * false 没有下一页了
                     */
                    if (page == 1) {
                        mSmartRefreshLayout.finishRefresh()
                        framingRecordAdapter.setNewData(it.data.records as List<Any>?)
                        if (it.data.total == 0) {
                            mStatusView.handleEmpty()
                        }
                        if (data.total < 15) {
                            mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                        }
                        return@Observer
                    }
                    if (it.data.isNextPage) {
                        mSmartRefreshLayout.finishLoadMore()
                    } else {
                        mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                    }
                    framingRecordAdapter.addData(it.data.records as List<Any>)
                } else {
                    mStatusView.handleEmpty()
                    ToastUtil.show("" + it.message)
                    mSmartRefreshLayout.finishRefresh()
                    mSmartRefreshLayout.finishLoadMore()
                }

            }
        })
        viewModel.otherList2.observe(this, Observer { it ->
            with(it)
            {
                mStatusView.handleSuccess()
                if (it.code == ResultStatus.SUCCESS) {
                    /**
                     * true还有下一页
                     * false 没有下一页了
                     */
                    if (page == 1) {
                        mSmartRefreshLayout.finishRefresh()
                        framingRecordAdapter.setNewData(it.data.records as List<Any>?)
                        if (it.data.total == 0) {
                            mStatusView.handleEmpty()
                        }
                        if (data.total < 15) {
                            mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                        }
                        return@Observer
                    }
                    if (it.data.isNextPage) {
                        mSmartRefreshLayout.finishLoadMore()
                    } else {
                        mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                    }
                    framingRecordAdapter.addData(it.data.records as List<Any>)
                } else {
                    mStatusView.handleEmpty()
                    ToastUtil.show("" + it.message)
                    mSmartRefreshLayout.finishRefresh()
                    mSmartRefreshLayout.finishLoadMore()
                }

            }
        })
        ivAdd.setOnClickListener {
            when (type) {
                0 ->
                    AddpesticidesActivity.start(this, 3, farmingCode)
                1 -> {
                    var type = when (viewModel.traceIndustry) {
                        10007 -> 1
                        else -> 0
                    }
                    AddpesticidesActivity.start(this, type, farmingCode)
                }

                2 -> {
                    var type = when (viewModel.traceIndustry) {
                        10007 -> 0
                        else -> 1
                    }
                    AddpesticidesActivity.start(this, type, farmingCode)
                }
                9 -> AddpesticidesActivity.start(this, 4, farmingCode)
                3 ->
                    AddpesticidesActivity.start(this, 2, farmingCode)
            }
        }
        framingRecordAdapter.setOnItemChildClickListener { adapter, _, position ->
            when (type) {
                0 -> {
                    var framingDetailBean = adapter.data.get(position) as FramingDetailBean
                    RecordDetailActivity.start(this, type, framingDetailBean.id, farmingCode)
                }
                9 -> {
                    var maddProduction = adapter.data.get(position) as AddProductBean
                    RecordDetailActivity.start(this, type, maddProduction)
                }
                else -> {
                    var framingDetailBean = adapter.data.get(position) as TraceProductionBean
                    RecordDetailActivity.start(this, type, framingDetailBean.id, farmingCode)
                }
            }

        }
        ivBack.setOnClickListener {
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mStatusView!!.onDestory()
    }
}
