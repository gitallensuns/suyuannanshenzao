package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.PesticidesBean

class PesticidesAdapter : BaseQuickAdapter<PesticidesBean, BaseViewHolder> {

    constructor(layoutResId: Int, data: List<PesticidesBean>?) : super(layoutResId, data) {}

    constructor(data: List<PesticidesBean>?) : super(data) {

    }

    override fun convert(helper: BaseViewHolder, item: PesticidesBean) {
        helper.addOnClickListener(R.id.llPesticidesOnclick)
            .setText(R.id.tvArchivesNum, "${item.archiNumber}")
            .setText(R.id.tvProductName, "${item.productName}")
            .setText(R.id.tvArchBatch, "${item.archiBatch}")
            .setText(R.id.tvProductDate, "${item.productDate}")
            .setText(R.id.tvPersonCharge, "${item.personCharge}")
    }
}
