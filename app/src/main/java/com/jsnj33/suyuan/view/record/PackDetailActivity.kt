package com.jsnj33.suyuan.view.record

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.model.PackBean
import com.jsnj33.suyuan.viewmodel.PackViewModel
import kotlinx.android.synthetic.main.activity_pack_detail.*
import kotlinx.android.synthetic.main.toolbar_header.*

class PackDetailActivity : BaseActivity<PackViewModel>() {
    companion object {
        @JvmStatic
        fun start(context: Context, gson: String, processStatus: String) {
            val starter = Intent(context, PackDetailActivity::class.java)
            starter.putExtra("gson", gson)
            starter.putExtra("processStatus", processStatus)
            context.startActivity(starter)
        }
    }

    override fun getLayoutID(): Int = R.layout.activity_pack_detail
    lateinit var processStatus: String
    lateinit var data: PackBean
    override fun initLoadView() {
        tvTitle.text = "包装详情"
        var gson = intent.getStringExtra("gson")
          processStatus = intent.getStringExtra("processStatus")
        data = Gson().fromJson(gson, PackBean::class.java)
        with(data) {
            tvName.text = "${data.productName}"
            tvPC.text = if (processStatus == "10000") "${this.archiNumber}" else "${this.slaughterArchiNumber}"
            tvGG.text = "${data.specs}"
            tvmaterial.text = "${data.material}"
            tvmode.text = "${setValue(data.mode)}"
            tvnum.text = "${data.num}"
            tvlevel.text = "${data.level}"
            tvpackDate.text = "${data.packDate}"
        }

    }

    fun setValue(value: Int): String {
//        箱装100901；盒装100902；袋装100903；其他100904
        var str = when (value) {
            100901 -> "箱装"
            100902 -> "盒装"
            100903 -> "袋装"
            100904 -> "其他"
            else -> "其他"
        }
        return str
    }

    override fun loadData() {
    }

    override fun watchListner() {
        ivBack.setOnClickListener {
            finish()
        }
    }
}