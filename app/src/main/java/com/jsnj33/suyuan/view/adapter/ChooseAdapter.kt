package com.jsnj33.suyuan.view.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.widget.addTextChangedListener
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.ChooseBean
import com.jsnj33.suyuan.repository.model.DataListBean
import com.jsnj33.suyuan.util.StringUtils
import com.jsnj33.suyuan.util.ToastUtil
import com.loc.v
import java.util.*

/**
 * 添加选择的投入产品
 */
class ChooseAdapter(data: List<ChooseBean>? = null) :
    BaseMultiItemQuickAdapter<ChooseBean, BaseViewHolder>(data) {

    init {
        addItemType(TAGITEM, R.layout.adapter_choose_item)
        addItemType(TAGADD, R.layout.adapter_choose_add)
    }

    override fun convert(helper: BaseViewHolder, item: ChooseBean) {
        when (helper.itemViewType) {
            TAGITEM -> {
                if (helper.adapterPosition == 0) {
                    helper.setGone(R.id.tvInfo, true)
                } else {
                    helper.setGone(R.id.tvInfo, false)
                }
                var etNum = helper.getView<EditText>(R.id.etNum)
                etNum.tag = helper.adapterPosition
                etNum.addTextChangedListener {
                    if (item.id == helper.adapterPosition) {
                        item.inputConsum = it.toString()

                    }
                }
                helper.setText(R.id.etNum, item.inputConsum)
                val mCompany = helper.getView<Spinner>(R.id.mCompany)
                mCompany.adapter = ArrayAdapter(
                    helper.itemView.context,
                    android.R.layout.simple_spinner_dropdown_item,
                    StringUtils.listCompany()
                )
                mCompany.setSelection(-1, true)

                val mSpinner = helper.getView<Spinner>(R.id.mSpinner)
                mSpinner.adapter =
                    ArrayAdapter(
                        helper.itemView.context,
                        android.R.layout.simple_spinner_dropdown_item,
                        item.dataList!!
                    )
                mSpinner.setSelection(item.position)
                mSpinner.setOnItemSelectedListener(object : OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        item.position = position
                        item.productId = item.dataList!!.get(position).id
                        item.inputSubtype = "${item.dataList!!.get(position).inputSubtype}"
                        //                        item.inputUnit = item.dataList!!.get(position).inputUnit
                        var tvNum = helper.getView<EditText>(R.id.etNum)
                        tvNum.hint = "请输入" + item.dataList!!.get(position).inputName + "使用量"
                        var pos = StringUtils.quearPosition(item.dataList!!.get(position).inputUnit)
                        mCompany.setSelection(pos)
                    }
                })
                mCompany.setOnItemSelectedListener(object : OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        item.inputUnit = StringUtils.listCompany().get(position).inputUnit
                    }
                })
                helper.addOnClickListener(R.id.ivDelete)
            }
            TAGADD ->
                helper.addOnClickListener(R.id.mMCOnclick)
        }
    }

    companion object {
        const val TAGITEM = 0x001
        const val TAGADD = 0x002
    }
}
