package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.MassifBean
import com.jsnj33.suyuan.repository.model.PackLebelBean
import com.jsnj33.suyuan.repository.model.PesticidesBean
import com.jsnj33.suyuan.repository.model.ReportBean

/**
 * @Author 萤火虫
 * @Date 2021/10/12
 * @Desc
 */
class PackLebel4Adapter(data: MutableList<String>?) : BaseQuickAdapter<String, BaseViewHolder>(R.layout.adapter_pack_lebel,data) {
    override fun convert(helper: BaseViewHolder, item: String?) {
        helper.setText(R.id.tvInfo,item)
            .addOnClickListener(R.id.tvInfo)
    }
}