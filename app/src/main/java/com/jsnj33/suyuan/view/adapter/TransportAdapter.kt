package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.PackBean
import com.jsnj33.suyuan.repository.model.PesticidesBean
import com.jsnj33.suyuan.repository.model.TransportBean

class TransportAdapter : BaseQuickAdapter<TransportBean, BaseViewHolder> {

    constructor(layoutResId: Int, data: List<TransportBean>?) : super(layoutResId, data) {}


    var processStatus: String = "10000"

    override fun convert(helper: BaseViewHolder, item: TransportBean) {
        helper.addOnClickListener(R.id.llTransportOnclik)
            .setText(R.id.tvArchBatch, if (processStatus == "10000") "${item.archiNumber}" else "${item.slaughterArchiNumber}")
            .setText(R.id.tvProductName, "${item.productName}")
            .setText(R.id.tvFZR, "${item.charge}")
            .setText(R.id.tvChe, "${item.vehicle}")
            .setText(R.id.tvTime, "${item.arriveDate}")
    }
}
