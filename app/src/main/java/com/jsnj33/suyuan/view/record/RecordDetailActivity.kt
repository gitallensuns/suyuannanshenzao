package com.jsnj33.suyuan.view.record

import android.app.Activity
import android.content.Intent
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.Observer
import cn.bingoogolapple.bgabanner.BGABanner
import com.bumptech.glide.Glide
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.model.FramingDetailBean
import com.jsnj33.suyuan.repository.model.TraceProductionBean
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.LogUtil
import com.jsnj33.suyuan.util.StringUtils
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.pesticides.AddpesticidesActivity
import com.jsnj33.suyuan.viewmodel.RecordViewModel
import com.leaf.library.StatusBarUtil
import com.jsnj33.suyuan.repository.client.RetrofitClient
import com.jsnj33.suyuan.repository.model.AddProductBean
import kotlinx.android.synthetic.main.activity_record_detail.*
import kotlinx.android.synthetic.main.activity_record_detail2.*
import kotlinx.android.synthetic.main.activity_record_detail3.*
import kotlinx.android.synthetic.main.activity_record_detail4.*
import kotlinx.android.synthetic.main.activity_record_detail4.mBGABanner4
import kotlinx.android.synthetic.main.activity_record_detail4.tvArchild4
import kotlinx.android.synthetic.main.activity_record_detail4.tvName4
import kotlinx.android.synthetic.main.activity_record_detail4.tvSeedCreater
import kotlinx.android.synthetic.main.activity_record_detail4.tvSeedName
import kotlinx.android.synthetic.main.activity_record_detail4.tvSeedNum
import kotlinx.android.synthetic.main.activity_record_detail4.tvSeedTime
import kotlinx.android.synthetic.main.activity_record_detail4.tvTouruSub
import kotlinx.android.synthetic.main.activity_record_detail4.tvTypeSub4
import kotlinx.android.synthetic.main.activity_record_detail5.*
import kotlinx.android.synthetic.main.toolbar_header.ivBack
import kotlinx.android.synthetic.main.toolbar_header_right.tvTitle
import java.util.*


class RecordDetailActivity : BaseActivity<RecordViewModel>() {
    companion object {
        fun start(activity: Activity, type: Int, framingId: Long, farmingCode: Int): Unit {
            val intent = Intent(activity, RecordDetailActivity::class.java)
            intent.putExtra("type", type)
            intent.putExtra("framingId", framingId)
            intent.putExtra("farmingCode", farmingCode)
            activity.startActivity(intent)
        }

        fun start(activity: Activity, type: Int, mddProductBean: AddProductBean): Unit {
            val intent = Intent(activity, RecordDetailActivity::class.java)
            intent.putExtra("type", type)
            intent.putExtra("mddProductBean", mddProductBean)
            activity.startActivity(intent)
        }
    }

    var type = 0;//0：农事生产详情、1：肥料生产详情、2：农药生产详情、3：种子生产详情,9:生产加工记录
    var framingId: Long = 0
    var farmingCode: Int = 0
    var picList = ArrayList<String>()
    override fun getLayoutID(): Int {
        /**
         * 如果加模块需要配置对应的布局M
         */
        type = intent.getIntExtra("type", 0)
        framingId = intent.getLongExtra("framingId", 0)
        farmingCode = intent.getIntExtra("farmingCode", 0)
        when (type) {
            0 -> {
                LogUtil.d("TAG", "activity_record_detail")

                return R.layout.activity_record_detail
            }
            1 -> {
                LogUtil.d("TAG", "activity_record_detail2")
                return R.layout.activity_record_detail2
            }
            2 -> {
                LogUtil.d("TAG", "activity_record_detail3")

                return R.layout.activity_record_detail3
            }
            3 -> {
                LogUtil.d("TAG", "activity_record_detail4")
                return R.layout.activity_record_detail4
            }
            9 -> {
                return R.layout.activity_record_detail5
            }
            else -> {
                LogUtil.d("TAG", "else activity_record_detail4")

                return R.layout.activity_record_detail4
            }
        }
    }

    override fun initLoadView() {
        when (type) {
            0 -> {
                StatusBarUtil.setGradientColor(this, m_tool_bar1)
            }
            1 -> {
                StatusBarUtil.setGradientColor(this, m_tool_bar2)
            }
            2 -> {
                StatusBarUtil.setGradientColor(this, m_tool_bar3)
            }
            3 -> {
                StatusBarUtil.setGradientColor(this, m_tool_bar4)
            }
            9 -> {
                StatusBarUtil.setGradientColor(this, m_tool_bar5)
            }
            else -> {
                StatusBarUtil.setGradientColor(this, m_tool_bar4)
            }
        }
    }

    override fun loadData() {
        tvTitle.text = StringUtils.getRecordDetailTitle(viewModel.traceIndustry, farmingCode)
        when (type) {
            0 -> {
                viewModel.getFramingRecordDetailData(framingId)
            }
            9 -> {
                tvTitle.text = "生产加工详情"
                with(intent.getSerializableExtra("mddProductBean") as AddProductBean) {
                    var listpic = this.pic?.split(",")
                    mBGABanner5.setAdapter(BGABanner.Adapter<ImageView?, String?> { _, itemView, model, position ->
                        Glide.with(this@RecordDetailActivity).load((RetrofitClient.BASE_URL + listpic?.get(position)))
                            .into(itemView!!)
                    })
                    if (listpic != null)
                        mBGABanner5.setData(listpic, null)
                    tvSeedTime.text = "${this.processDate}"
                    tvSeedCreater.text = "${this.principal}"
                    tvName4.text = "${this.productName}"
                    tvArchild4.text = "${this.archiNumber}"
                    tvRemark.text = "${this.processRemark}"
                }
            }
            else -> {
                viewModel.getOtherRecordDetailData(framingId)
            }
        }

    }

    override fun watchListner() {
        /**
         * 农事生产详情
         */
        viewModel.framingRecordDetail.observe(this@RecordDetailActivity, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                var framingDetailBean = it?.data as FramingDetailBean

                when (type) {
                    0 -> {
                        tvTypeSub.text = StringUtils.getRecordName(viewModel.traceIndustry, type)
                        tvFramingTime.text = framingDetailBean.createDate!!
                        tvFramingCreater.text = framingDetailBean.personName!!
                        when (viewModel.traceIndustry) {
                            10008 -> {
                                tvFramingType.text =
                                    StringUtils.farmingTypeValue(
                                        viewModel.traceIndustry,
                                        framingDetailBean.breedCode
                                    )
                            }
                            10009 -> {
                                tvFramingType.text =
                                    StringUtils.farmingTypeValue(
                                        viewModel.traceIndustry,
                                        framingDetailBean.fishbreedCode
                                    )
                            }
                            else -> {
                                tvFramingType.text =
                                    StringUtils.farmingTypeValue(
                                        viewModel.traceIndustry,
                                        framingDetailBean.farmingCode
                                    )
                            }
                        }
//                        Glide.with(this)
//                            .load((RetrofitClient.BASE_URL + framingDetailBean.filePath?.get(0)))
//                            .into(ivHeader)
                        mBGABanner.setAdapter(BGABanner.Adapter<ImageView?, String?> { _, itemView, _, position ->
                            Glide.with(this)
                                .load(
                                    (RetrofitClient.BASE_URL + framingDetailBean.filePath?.get(
                                        position
                                    ))
                                )
                                .into(itemView!!)
                        })
                        var list = framingDetailBean.filePath
                        if (list != null)
                            mBGABanner.setData(list, null)
                        tvName.text = framingDetailBean.productName!!
                        tvArchild.text = framingDetailBean.archiId!!

                    }
                }
            } else {
                ToastUtil.show(it.message!!)
            }
        })
        /**
         * 农事生产详情
         */
        viewModel.oterRecordDetail.observe(this@RecordDetailActivity, Observer {
            if (it.code == ResultStatus.SUCCESS) {
                var framingDetailBean = it?.data as TraceProductionBean
                when (type) {
                    1 -> {
                        tvFertilizerTime.text = framingDetailBean.createDate
                        tvFertilizerCreater.text = framingDetailBean.personName
                        tvFertilizerName.text = framingDetailBean.inputName
                        tvTypeSub1.text =
                            StringUtils.getRecordName(viewModel.traceIndustry, type)
                        tvFertilizerNum.text =
                            "${framingDetailBean.inputConsum}${
                                StringUtils.getInputUnitValue(
                                    framingDetailBean.inputUnit
                                )
                            }"
//                        tvWTName.text = framingDetailBean.productName!!
                        tvWTName.text = "${framingDetailBean.productName}"
                        tvArchild2?.text = "${framingDetailBean.archiId}"
                        tvFertilizerType.text = framingDetailBean.enterName
                        StringUtils.farmingTypeValue(
                            viewModel.traceIndustry,
                            framingDetailBean.farmingCode
                        )
//                        Glide.with(this)
//                            .load((RetrofitClient.BASE_URL + framingDetailBean.filePath?.get(0)))
//                            .into(ivHeader2)
                        mBGABanner2.setAdapter(BGABanner.Adapter<ImageView?, String?> { _, itemView, _, position ->
                            Glide.with(this)
                                .load(
                                    (RetrofitClient.BASE_URL + framingDetailBean.filePath?.get(
                                        position
                                    ))
                                )
                                .into(itemView!!)
                        })
                        var list = framingDetailBean.filePath
                        if (list != null)
                            mBGABanner2.setData(list, null)
                    }
                    2 -> {
                        tvTypeSub3.text = StringUtils.getRecordName(viewModel.traceIndustry, type)
                        tvPesticidesTime.text = framingDetailBean.createDate
                        tvPesticidesCreater.text = framingDetailBean.personName!!
                        tvPesticideName.text = framingDetailBean.inputName
                        tvName3.text = "${framingDetailBean.productName!!}"
                        tvArchild3.text = "${framingDetailBean.archiId!!}"

                        tvNum.text =
                            "${framingDetailBean.inputConsum}${
                                StringUtils.getInputUnitValue(
                                    framingDetailBean.inputUnit
                                )
                            }"
                        tvFangzhiOBJ.text =
                            StringUtils.pestControlTargets(framingDetailBean.pestControlTargets)
                        mBGABanner3.setAdapter(BGABanner.Adapter<ImageView?, String?> { _, itemView, _, position ->
                            Glide.with(this)
                                .load(
                                    (RetrofitClient.BASE_URL + framingDetailBean.filePath?.get(
                                        position
                                    ))
                                )
                                .into(itemView!!)
                        })
                        var list = framingDetailBean.filePath
                        if (list != null)
                            mBGABanner3.setData(list, null)
                    }
                    3 -> {
                        tvTypeSub4.text = StringUtils.getRecordName(viewModel.traceIndustry, type)
                        tvSeedTime.text = framingDetailBean.createDate
                        tvSeedCreater.text = framingDetailBean.personName
                        tvSeedName.text = framingDetailBean.inputName
                        tvName4.text = framingDetailBean.productName!!
                        tvArchild4.text = framingDetailBean.archiId!!

                        tvTouruSub.text =
                            StringUtils.getRecordDoValue(viewModel.traceIndustry, type)
                        tvSeedNum.text =
                            "${framingDetailBean.inputConsum}${
                                StringUtils.getInputUnitValue(
                                    framingDetailBean.inputUnit
                                )
                            }"
                        mBGABanner4.setAdapter(BGABanner.Adapter<ImageView?, String?> { _, itemView, _, position ->
                            Glide.with(this)
                                .load(
                                    (RetrofitClient.BASE_URL + framingDetailBean.filePath?.get(
                                        position
                                    ))
                                )
                                .into(itemView!!)
                        })
                        var list = framingDetailBean.filePath
                        if (list != null)
                            mBGABanner4.setData(list, null)
                    }
                }
            } else {
                ToastUtil.show(it.message!!)
            }
        })

        ivBack.setOnClickListener {
            finish()
        }
    }


    fun OnClick(view: View) {
        view.id
        when (type) {
            0 ->
                AddpesticidesActivity.start(this, 3, farmingCode)
            1 -> {
                var type = when (viewModel.traceIndustry) {
                    10007 -> 1
                    else -> 0
                }
                AddpesticidesActivity.start(this, type, farmingCode)
            }
            2 -> {
                var type = when (viewModel.traceIndustry) {
                    10007 -> 0
                    else -> 1
                }
                AddpesticidesActivity.start(this, type, farmingCode)
            }
            3 -> AddpesticidesActivity.start(this, 2, farmingCode)
            9 -> AddpesticidesActivity.start(this, 4, farmingCode)
        }
    }

    fun back(view: View) {
        view.id
        finish()
    }
}
