package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.PackBean
import com.jsnj33.suyuan.repository.model.PesticidesBean

class PackAdapter : BaseQuickAdapter<PackBean, BaseViewHolder> {

    constructor(layoutResId: Int, data: List<PackBean>?) : super(layoutResId, data) {}


    var processStatus: String = "10000"

    override fun convert(helper: BaseViewHolder, item: PackBean) {
        helper.addOnClickListener(R.id.llPesticidesOnclick)
            .setText(R.id.tvLevel, "${item.level}")
            .setText(R.id.tvProductName, "${item.productName}")
            .setText(R.id.tvArchBatch, if (processStatus == "10000") "${item.archiNumber}" else "${item.slaughterArchiNumber}")
            .setText(R.id.tvProductDate, "${item.packDate}")
            .setText(R.id.tvPackNum, "${item.num}")
    }
}
