package com.jsnj33.suyuan.view.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.jsnj33.suyuan.BuildConfig
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.view.login.LoginActivity
import com.leaf.library.StatusBarUtil
import kotlinx.android.synthetic.main.activity_splash.*

/**
 * 开屏页
 */
class SplashActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "AAAABBCCDDDD"
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart: ")

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate: 1")
        setContentView(R.layout.activity_splash)
        Log.d(TAG, "onCreate: 2")
        StatusBarUtil.setTransparentForWindow(this)
        rlBg.setBackgroundResource(BuildConfig.splash)
        if (BuildConfig.FLAVOR == "sheyang") {
            ivInfo.visibility = View.VISIBLE
        }
        Thread(Runnable {
            Thread.sleep(2000)
            LoginActivity.start(this)
            finish()
        }).start()
    }

}
