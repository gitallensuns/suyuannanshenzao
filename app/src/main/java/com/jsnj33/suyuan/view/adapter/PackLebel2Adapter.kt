package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.PackLebelBean
import com.jsnj33.suyuan.repository.model.PesticidesBean

/**
 * @Author 萤火虫
 * @Date 2021/10/12
 * @Desc
 */
class PackLebel2Adapter(data: MutableList<PackLebelBean>?) : BaseQuickAdapter<PackLebelBean, BaseViewHolder>(R.layout.adapter_pack_lebel,data) {
    override fun convert(helper: BaseViewHolder, item: PackLebelBean?) {
        helper.setText(R.id.tvInfo,item?.name)
            .addOnClickListener(R.id.tvInfo)
    }
}