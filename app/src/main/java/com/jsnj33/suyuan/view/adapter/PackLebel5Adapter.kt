package com.jsnj33.suyuan.view.adapter

import android.widget.CheckedTextView
import androidx.appcompat.widget.AppCompatCheckBox
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.MassifBean
import com.jsnj33.suyuan.repository.model.PackLebelBean
import com.jsnj33.suyuan.repository.model.PesticidesBean
import com.jsnj33.suyuan.repository.model.ReportBean

/**
 * @Author 萤火虫
 * @Date 2021/10/12
 * @Desc
 */
class PackLebel5Adapter(data: MutableList<MassifBean>?) : BaseQuickAdapter<MassifBean, BaseViewHolder>(R.layout.adapter_pack_lebel5, data) {
    override fun convert(helper: BaseViewHolder, item: MassifBean?) {
        helper.setText(R.id.accbTxt, item?.massifName)

      var accbTxt=  helper.getView<AppCompatCheckBox>(R.id.accbTxt)
        accbTxt.isChecked=item?.isCheck?:false
        accbTxt .setOnCheckedChangeListener { buttonView, isChecked ->
            item?.isCheck =isChecked
        }
    }
}