package com.jsnj33.suyuan.view.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.PesticidesBean

/**
 * @Author 萤火虫
 * @Date 2021/10/12
 * @Desc
 */
class PackLebelAdapter(data: MutableList<PesticidesBean>?, val processStatus: String) : BaseQuickAdapter<PesticidesBean, BaseViewHolder>(R.layout.adapter_pack_lebel, data) {
    override fun convert(helper: BaseViewHolder, item: PesticidesBean?) {
        helper.setText(R.id.tvInfo, if (processStatus == "10000") item?.archiNumber else item?.slaughterArchiNumber)
            .addOnClickListener(R.id.tvInfo)
    }
}