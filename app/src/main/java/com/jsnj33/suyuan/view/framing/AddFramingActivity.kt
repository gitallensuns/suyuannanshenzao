package com.jsnj33.suyuan.view.framing

import android.app.Activity
import android.content.Intent
import com.chad.library.adapter.base.BaseQuickAdapter
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.model.FramingBean
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.FramingAdapter
import com.jsnj33.suyuan.view.pesticides.ChooseActivity
import com.jsnj33.suyuan.viewmodel.FramingViewModel
import com.leaf.library.StatusBarUtil
import kotlinx.android.synthetic.main.activity_add_framing.*
import kotlinx.android.synthetic.main.activity_main.mRecyclerView
import kotlinx.android.synthetic.main.toolbar_header.*

/**
 * 添加农事
 */
class AddFramingActivity : BaseActivity<FramingViewModel>() {
    companion object {
        const val TAG = "AddFramingActivity"
        fun start(activity: Activity, archId: String, type: Int, plantSeasonId: String): Unit {
            var intent = Intent(activity, AddFramingActivity::class.java)
            intent.putExtra("archId", archId)
            intent.putExtra("type", type)
            intent.putExtra("plantSeasonId", plantSeasonId)
            activity.startActivity(intent)
        }
    }

    private var adapter = FramingAdapter(null);
    private var archId: String? = null //生产档案id
    private var type: Int = 0
    var framingBean: FramingBean? = null
    var plantSeasonId: String? = null
    override fun getLayoutID(): Int = R.layout.activity_add_framing

    override fun initLoadView() {
        StatusBarUtil.setGradientColor(this, mMaterialCardView)
        tvTitle.text = "添加养殖管理"
        mRecyclerView.adapter = adapter;
        adapter.openLoadAnimation()
    }

    override fun loadData() {
        archId = intent.getStringExtra("archId")
        plantSeasonId = intent.getStringExtra("plantSeasonId")
        type = intent.getIntExtra("type", 0)
        viewModel.getDataList()
    }

    override fun watchListner() {
        viewModel.dataList.observe(this@AddFramingActivity, androidx.lifecycle.Observer {
            adapter.setNewData(it)
        })
        adapter.setOnItemChildClickListener(BaseQuickAdapter.OnItemChildClickListener { adapter, _, position ->
            adapter.data.map {
                it as FramingBean
                it.isChecked = false
            }
            (adapter.data.get(position) as FramingBean).isChecked = true
            adapter.data.map {
                it as FramingBean
                if (it.isChecked) {
                    framingBean = it
                }
            }
            adapter.notifyDataSetChanged()
        })

        ivBack.setOnClickListener {
            finish()
        }

        btnChoose.setOnClickListener {
            if (framingBean == null) {
                ToastUtil.show("请选择管理类型")
                return@setOnClickListener
            }
            if (type == 3) {
                UploadIMGActivity.start(this, framingBean?.farmingCode!!, archId!!, null, type,"$plantSeasonId")
            } else {
                ChooseActivity.start(
                    this@AddFramingActivity,
                    framingBean?.farmingCode!!,
                    archId!!,
                    type,
                    "$plantSeasonId"
                )
            }
        }
    }
}
