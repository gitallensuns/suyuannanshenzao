package com.jsnj33.suyuan.view.record

import android.content.Context
import android.content.Intent
import com.google.gson.Gson
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.model.PackBean
import com.jsnj33.suyuan.repository.model.TransportBean
import com.jsnj33.suyuan.viewmodel.PackViewModel
import kotlinx.android.synthetic.main.activity_transport_detail.*
import kotlinx.android.synthetic.main.toolbar_header.*

class TransportDetailActivity : BaseActivity<PackViewModel>() {
    companion object {
        @JvmStatic
        fun start(context: Context, gson: String, processStatus: String) {
            val starter = Intent(context, TransportDetailActivity::class.java)
            starter.putExtra("gson", gson)
            starter.putExtra("processStatus", processStatus)
            context.startActivity(starter)
        }
    }

    override fun getLayoutID(): Int = R.layout.activity_transport_detail
    lateinit var processStatus2: String
    lateinit var data: TransportBean

    override fun initLoadView() {
        tvTitle.text = "运输详情"
        var gson = intent.getStringExtra("gson")
        processStatus2 = intent.getStringExtra("processStatus")
        data = Gson().fromJson(gson, TransportBean::class.java)
        with(data) {
            tvName.text = "${data.productName}"
            tvPC.text = if (processStatus2 == "10000") "${this.archiNumber}" else "${this.slaughterArchiNumber}"
            tvChe.text = "${data.vehicle}"
            tvtransDate.text = "${data.transDate}"
            tvFZR.text = "${data.charge}"
            tvdestination.text = "${data.destination}"
            tvarriveDate.text = "${data.arriveDate}"
        }
    }

    override fun loadData() {
    }

    override fun watchListner() {
        ivBack.setOnClickListener {
            finish()
        }
    }
}