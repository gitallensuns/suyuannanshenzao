package com.jsnj33.suyuan.view.record

import android.content.Context
import android.content.Intent
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.model.PatrolBean
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.PatrolAdapter
import com.jsnj33.suyuan.viewmodel.PatrolViewModel
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import kotlinx.android.synthetic.main.activity_patrol_record.*
import kotlinx.android.synthetic.main.layout_refresh.*
import kotlinx.android.synthetic.main.toolbar_header_right.*

/**
 * 巡田记录
 */
class PatrolRecordActivity : BaseActivity<PatrolViewModel>() {
    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, PatrolRecordActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun getLayoutID(): Int = R.layout.activity_patrol_record
    lateinit var adapter: PatrolAdapter
    var pageNum = 1
    override fun initLoadView() {
        tvTitle.text = "巡田记录"
        adapter = PatrolAdapter(null)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.adapter = adapter
//        adapter.setNewData(arrayListOf("", "", ""))
        viewModel.getPatrolList(pageNum)
        mStatusView.handleLoading()
    }

    override fun loadData() {
    }

    override fun watchListner() {

        viewModel.apply {
            this.patrolList.observe(this@PatrolRecordActivity, Observer {
                mStatusView.handleSuccess()
                if (it.code == ResultStatus.SUCCESS) {
                    if (pageNum == 1) {
                        mSmartRefreshLayout.finishRefresh()
                        adapter.setNewData(it.data.records)
                        if (it.data.total == 0) {
                            mStatusView.handleEmpty()
                        }
                        if (it.data.total < 15) {
                            mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                        }
                        return@Observer
                    }
                    if (it.data.isNextPage) {
                        mSmartRefreshLayout.finishLoadMore()
                    } else {
                        mSmartRefreshLayout.finishLoadMoreWithNoMoreData()
                    }
                    adapter.addData(it.data.records)
                } else {
                    ToastUtil.show("${it.message}")
                }
            })
        }
        adapter.setOnItemChildClickListener { adapter, view, position ->
            var data = adapter.data[position] as PatrolBean
            PatrolDetailActivity.start(this, data.id)
        }
        mSmartRefreshLayout.setOnRefreshLoadMoreListener(object : OnRefreshLoadMoreListener {
            override fun onRefresh(refreshLayout: RefreshLayout) {
                pageNum = 1
                viewModel.getPatrolList(pageNum)
            }

            override fun onLoadMore(refreshLayout: RefreshLayout) {
                pageNum += 1
                viewModel.getPatrolList(pageNum)
            }
        })
        ivBack.setOnClickListener {
            finish()
        }

        ivAdd.setOnClickListener {
            AddPatrolActivity.start(this)
        }
    }

    override fun onResume() {
        super.onResume()
        pageNum = 1
        viewModel.getPatrolList(pageNum)
    }
}