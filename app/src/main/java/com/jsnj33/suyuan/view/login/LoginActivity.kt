package com.jsnj33.suyuan.view.login

import android.app.Activity
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.jsnj33.suyuan.BuildConfig
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.LogUtil
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.main.MainActivity
import com.leaf.library.StatusBarUtil
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.toolbar_header.*
import net.iharder.Base64

class LoginActivity : BaseActivity<LoginViewModel>() {
    companion object {
        const val TAG = "LoginActivity"
        fun start(actvity: Activity): Unit {
            var intent = Intent(actvity, LoginActivity::class.java)
            actvity.startActivity(intent)
        }
    }

    var phone: String by PreferencesUtil<String>("phone", "")
    var password: String by PreferencesUtil("password", "")
    var personName: String? by PreferencesUtil("personName", "")
    var id: String? by PreferencesUtil("id", "")

    /**
     * traceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)
     */
    var traceIndustry: Int? by PreferencesUtil("traceIndustry", 10007)
    var tracePeasantId: Long? by PreferencesUtil("tracePeasantId", 0)
    var mobileToken: String by PreferencesUtil("mobileToken", "token")
    var token: String by PreferencesUtil("token", "token")

    override fun getLayoutID(): Int {
        return R.layout.activity_login
    }

    override fun initLoadView() {
        StatusBarUtil.setGradientColor(this, mMaterialCardView)
        ivIcon.setImageResource(BuildConfig.logo)
        tvInfo.text = "${BuildConfig.appTitle}"
        tvTitle.text = "登录"
        ivBack.visibility = View.GONE
        mStatusView.rootView = mLoginContent
        etPhone.setText(phone)
        etPassword.setText(password)
    }

    override fun loadData() {

    }

    fun validata(): Boolean {
        if (TextUtils.isEmpty(etPhone.text)) {
            ToastUtil.show("请输入账号")
            return false
        }
        if (TextUtils.isEmpty(etPassword.text)) {
            ToastUtil.show("请输入密码")
            return false
        }
        return true
    }

    override fun watchListner() {
        viewModel.loginBean.observe(this@LoginActivity, Observer {
            mStatusView.handleSuccess()
            LogUtil.d(TAG, it.message)
            if (it.code == ResultStatus.SUCCESS) {
                ToastUtil.show("登录成功")
                traceIndustry = it.data?.userInfo?.traceIndustry
                tracePeasantId = it.data?.userInfo?.tracePeasantId
                id=it.data?.userInfo?.id
//                if (BuildConfig.FLAVOR.contains("xuyi")) {
//                    mobileToken="${it.data?.token}"
//                }
                token="${it.data?.token}"
                MainActivity.start(this)
                finish()
            } else {
                ToastUtil.show(it.message!!)
            }
        })
        btnLogin.setOnClickListener {
            if (validata()) {
                mStatusView.handleLoading(true, "登录中...")
                phone = etPhone.text.toString()
                password = etPassword.text.toString()

                var token: String = String(Base64.encodeBytesToBytes("${phone}@${password}".toByteArray()))
                mobileToken = token
                viewModel.login(etPhone.text.toString(), etPassword.text.toString(), mobileToken)
            }
        }
    }
}
