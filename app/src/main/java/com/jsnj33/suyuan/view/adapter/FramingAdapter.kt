package com.jsnj33.suyuan.view.adapter

import android.annotation.SuppressLint
import android.os.Build
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.FramingBean

/**
 * 添加农事适配器
 */
class FramingAdapter(data: MutableList<FramingBean>?) :
    BaseQuickAdapter<FramingBean, BaseViewHolder>(R.layout.adapter_addframing, data) {

    @SuppressLint("ResourceAsColor")
    override fun convert(helper: BaseViewHolder, item: FramingBean?) {
        helper.setImageResource(R.id.ivIcon, item!!.url)
            .setText(R.id.tvName, item.name)
            .addOnClickListener(R.id.mMCVOnclick)
        var mMCVOnclick = helper.getView<MaterialCardView>(R.id.mMCVOnclick)
        if (item.isChecked) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mMCVOnclick.strokeColor = mContext.getColor(R.color.colorPrimary)
                mMCVOnclick.strokeWidth = 3
            } else {
                mMCVOnclick.strokeColor = R.color.colorPrimary
                mMCVOnclick.strokeWidth = 3
            }
        } else {
            mMCVOnclick.strokeWidth = 0
        }

    }
}