package com.jsnj33.suyuan.view.adapter

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.client.RetrofitClient
import com.jsnj33.suyuan.repository.model.PatrolBean

/**
 *@Author 萤火虫
 *@Date 2022/6/21
 *@Desc
 */
class PatrolAdapter(data: MutableList<PatrolBean>?) : BaseQuickAdapter<PatrolBean, BaseViewHolder>(R.layout.adapter_patrol, data) {
    override fun convert(helper: BaseViewHolder, item: PatrolBean?) {
        helper?.setText(R.id.tvPatrolPerson, "巡田人员：${item?.fieldPerson}")
            ?.setText(R.id.tvFarmer, item?.fieldFarmName)
            ?.setText(R.id.tvArea, "${item?.fieldLocation}")
            ?.setText(R.id.tvpatrolTime, "${item?.fieldTime}")
            ?.addOnClickListener(R.id.mMCVOnclick)
        Glide.with(helper.itemView.context).load( item?.fieldPic)
            .into(helper.getView(R.id.ivItemIcon) as ImageView)
    }
}