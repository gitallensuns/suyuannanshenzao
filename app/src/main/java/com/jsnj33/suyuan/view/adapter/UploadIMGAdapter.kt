package com.jsnj33.suyuan.view.adapter

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.UploadIMGBean

class UploadIMGAdapter(data: MutableList<UploadIMGBean>?) :
    BaseMultiItemQuickAdapter<UploadIMGBean, BaseViewHolder>(data) {
    var maxSize = 3

    companion object {
        const val ADDIMG = 0;
        const val PICTAG = 1;
    }

    init {
        addItemType(ADDIMG, R.layout.adapter_upload_add)
        addItemType(PICTAG, R.layout.adapter_upload_img)
    }

    override fun convert(helper: BaseViewHolder, item: UploadIMGBean?) {

        when (item!!.type) {
            ADDIMG ->
                helper.addOnClickListener(R.id.llAddIMG)
            PICTAG -> {
                var ivPic = helper.getView<ImageView>(R.id.ivPicture)
                Glide.with(helper.itemView.context).apply {
                    this.load(item.file)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(ivPic)
                }
                helper.addOnClickListener(R.id.ivDeleteIMG)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (data.size >= (maxSize + 1)) maxSize else data.size
    }
}