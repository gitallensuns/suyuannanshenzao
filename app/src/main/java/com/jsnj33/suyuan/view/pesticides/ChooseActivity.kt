package com.jsnj33.suyuan.view.pesticides

import android.app.Activity
import android.content.Intent
import android.os.Parcelable
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.huantansheng.easyphotos.EasyPhotos
import com.huantansheng.easyphotos.models.album.entity.Photo
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.model.ChooseBean
import com.jsnj33.suyuan.repository.model.ProductList
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.model.WHBean
import com.jsnj33.suyuan.util.GlideEngine
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.ChooseAdapter
import com.jsnj33.suyuan.view.framing.UploadIMGActivity
import com.jsnj33.suyuan.viewmodel.PesticidesViewModel
import com.leaf.library.StatusBarUtil
import kotlinx.android.synthetic.main.activity_choose.*
import kotlinx.android.synthetic.main.activity_choose.mRecyclerView
import kotlinx.android.synthetic.main.activity_choose.tvScan
import java.io.File

class ChooseActivity : BaseActivity<PesticidesViewModel>() {

    companion object {
        val REQUEST_TAKE_PHOTO = 101

        const val TAG = "ChooseActivity"
        fun start(activity: Activity, farmingCode: Int = 0, archId: String, type: Int, plantSeasonId: String): Unit {
            val intent = Intent(activity, ChooseActivity::class.java)
            intent.putExtra("farmingCode", farmingCode)
            intent.putExtra("archId", archId)
            intent.putExtra("type", type)
            intent.putExtra("plantSeasonId", plantSeasonId)
            activity.startActivity(intent)
        }
    }

    private lateinit var chooseAdapter: ChooseAdapter
    var farmingCode: Int = 0//选择养殖管理的Code
    var archId: String? = null //生产档案id(对应生产档案列表)；
    var type: Int = 0
    var path = ""
    var plantSeasonId=""
    override fun getLayoutID(): Int = R.layout.activity_choose

    override fun initLoadView() {
        StatusBarUtil.setGradientColor(this, mMaterialCardView)
        tvTitle.text = "请选择名称"
        chooseAdapter = ChooseAdapter(null)
        mRecyclerView.adapter = chooseAdapter
        plantSeasonId = intent.getStringExtra("plantSeasonId")
    }

    override fun loadData() {
        farmingCode = intent.getIntExtra("farmingCode", 0)
        archId = intent.getStringExtra("archId")
        type = intent.getIntExtra("type", 0)
        mStatusView.rootView = mRlContent
        mStatusView.handleLoading()
        viewModel.getChooseList(type)
    }


    override fun watchListner() {

        viewModel.chooseList.observe(this@ChooseActivity, Observer {
            mStatusView.handleSuccess()
            if (!it.isNullOrEmpty()) {
                chooseAdapter.setNewData(it)
            } else {
                ToastUtil.show("暂无投入品")
            }
        })
        //图像识别后
        viewModel.uploadIMG.observe(this, Observer {
            mStatusView.handleSuccess()
            if (it?.code == ResultStatus.SUCCESS) {
                ToastUtil.show("识别成功")
                viewModel.getChooseList2(type)
            } else {
                ToastUtil.show("${it?.message}")
            }
        })
        ivBack.setOnClickListener {
            finish()
        }
        chooseAdapter.setOnItemChildClickListener { _, view, pos ->
            when (view.id) {
                R.id.mMCOnclick ->
                    viewModel.addItem()
                R.id.ivDelete ->
                    viewModel.removeItem(pos)
            }
        }
        mAppCompatButton.setOnClickListener {
            if (!viewModel.isAdd) {
                ToastUtil.show("暂无投入品 无法提交")
                return@setOnClickListener
            }
            var dataListBean = chooseAdapter.data as List<ChooseBean>
            dataListBean.forEach {
                if (it.inputConsum.equals("")) {
                    ToastUtil.show("请输入使用数量")
                    return@setOnClickListener
                }
            }
            var productList = ArrayList<ProductList>()
            dataListBean.map {
                productList.add(ProductList(it.productId, it.inputConsum, it.inputUnit,it.inputSubtype))
            }
            UploadIMGActivity.start(this, farmingCode, archId!!, productList, type,plantSeasonId)
        }
        tvScan.setOnClickListener {
            EasyPhotos.createAlbum(this, true, GlideEngine.getInstance())
                .setFileProviderAuthority("$packageName.fileprovider")
                .setCleanMenu(false)
                .setPuzzleMenu(false)
                .start(REQUEST_TAKE_PHOTO)
        }

    }

    @Suppress("UNCHECKED_CAST")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            //拍照
            REQUEST_TAKE_PHOTO -> {
                mStatusView.handleLoading(false)
                val resultPhotos =
                    data!!.getParcelableArrayListExtra<Parcelable>(EasyPhotos.RESULT_PHOTOS) as ArrayList<Photo>
                resultPhotos.forEach {
                    path = it.path
                }
                viewModel.scanIMGUplaod(File(path))
            }
        }
    }
}
