package com.jsnj33.suyuan.view.adapter

import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.FramingDetailBean
import com.jsnj33.suyuan.repository.model.TraceProductionBean
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.util.StringUtils
import com.jsnj33.suyuan.repository.client.RetrofitClient
import com.jsnj33.suyuan.repository.model.AddProductBean

class FramingRecordAdapter(layoutResId: Int, data: List<Any>?, val types: Int) :
    BaseQuickAdapter<Any, BaseViewHolder>(
        layoutResId,
        data
    ) {
    var traceIndustry: Int = 0

    init {
        /**
         * traceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)
         */
        var traceIndustry: Int by PreferencesUtil("traceIndustry", 10007)
        this.traceIndustry = traceIndustry
    }

    override fun convert(helper: BaseViewHolder, item: Any) {
        when (types) {
            0 -> {
                item as FramingDetailBean
                Glide.with(helper.itemView.context).load(RetrofitClient.BASE_URL + item.path)
                    .into(helper.getView(R.id.ivItemIcon) as ImageView)
                helper.addOnClickListener(R.id.mMCVOnclick)
                    .setText(R.id.tvItemTitle, "上传者:${item.personName}")
                    .setText(R.id.tvItemTime, item.createDate)
                    .setGone(R.id.tvItemPerson, false)
                    .setGone(R.id.tvItemPersonL, false)
                when (traceIndustry) {
                    10008 -> {
                        helper.setText(
                            R.id.tvItemType,
                            StringUtils.farmingTypeValue(traceIndustry, item.breedCode)
                        )
                    }
                    10009 -> {
                        helper.setText(
                            R.id.tvItemType,
                            StringUtils.farmingTypeValue(traceIndustry, item.fishbreedCode)
                        )
                    }
                    else -> {
                        helper.setText(
                            R.id.tvItemType,
                            StringUtils.farmingTypeValue(traceIndustry, item.farmingCode)
                        )
                    }
                }
            }
            1 -> {
                item as TraceProductionBean
                Glide.with(helper.itemView.context).load(RetrofitClient.BASE_URL + item.path)
                    .into(helper.getView(R.id.ivItemIcon) as ImageView)
                helper.addOnClickListener(R.id.mMCVOnclick)
                    .setText(R.id.tvItemTitle, "上传者:${item.personName}")
                    .setText(R.id.tvWorkTypeName, StringUtils.getRecordName(traceIndustry, types))
                    .setText(
                        R.id.tvWorkTimeName,
                        StringUtils.getRecordDoValue(traceIndustry, types)
                    )
                    .setText(R.id.tvItemPersonL, StringUtils.getRecordTime(traceIndustry, types))
                    .setText(R.id.tvItemType, item.inputName)
                    .setText(
                        R.id.tvItemTime,
                        "${item.inputConsum}${StringUtils.getInputUnitValue(item.inputUnit)}"
                    )
                    .setText(R.id.tvItemPerson, item.createDate)
            }
            2 -> {
                item as TraceProductionBean
                Glide.with(helper.itemView.context).load(RetrofitClient.BASE_URL + item.path)
                    .into(helper.getView(R.id.ivItemIcon) as ImageView)
                helper.addOnClickListener(R.id.mMCVOnclick)
                    .setText(R.id.tvItemTitle, "上传者:${item.personName}")
                    .setText(R.id.tvWorkTypeName, StringUtils.getRecordName(traceIndustry, types))
                    .setText(
                        R.id.tvWorkTimeName,
                        StringUtils.getRecordDoValue(traceIndustry, types)
                    )
                    .setText(R.id.tvItemPersonL, StringUtils.getRecordTime(traceIndustry, types))
                    .setText(R.id.tvItemType, item.inputName)
                    .setText(
                        R.id.tvItemTime,
                        "${item.inputConsum}${StringUtils.getInputUnitValue(item.inputUnit)}"
                    )
                    .setText(R.id.tvItemPerson, item.createDate)
            }

            3 -> {
                item as TraceProductionBean
                Glide.with(helper.itemView.context).load(RetrofitClient.BASE_URL + item.path)
                    .into(helper.getView(R.id.ivItemIcon) as ImageView)
                helper.addOnClickListener(R.id.mMCVOnclick)
                    .setText(R.id.tvItemTitle, "上传者:${item.personName}")
                    .setText(R.id.tvWorkTypeName, StringUtils.getRecordName(traceIndustry, types))
                    .setText(
                        R.id.tvWorkTimeName,
                        StringUtils.getRecordDoValue(traceIndustry, types)
                    )
                    .setText(R.id.tvItemPersonL, StringUtils.getRecordTime(traceIndustry, types))
                    .setText(R.id.tvItemType, item.inputName)
                    .setText(
                        R.id.tvItemTime,
                        "${item.inputConsum}${StringUtils.getInputUnitValue(item.inputUnit)}"
                    )
                    .setText(R.id.tvItemPerson, item.createDate)
            }
            9 -> {
                try {
                    item as AddProductBean
                    var pic = item.pic?.split(",")?.get(0)
                    Glide.with(helper.itemView.context).load(RetrofitClient.BASE_URL + pic)
                        .into(helper.getView(R.id.ivItemIcon) as ImageView)
                    helper.addOnClickListener(R.id.mMCVOnclick)
                        .setText(R.id.tvItemTitle, "上传者:${item.principal}")
                        .setText(R.id.tvWorkTypeName, "产品名称")
                        .setText(R.id.tvWorkTimeName, "生产日期")
                        .setText(R.id.tvItemPersonL, "生产备注")
                        .setText(R.id.tvItemType, item.productName)
                        .setText(
                            R.id.tvItemTime,
                            "${item.processDate}"
                        )
                        .setText(R.id.tvItemPerson, item.processRemark)
                } catch (e: Exception) {
                    Log.d(TAG, "convert: ${e.message}")
                }

            }
        }

    }

}
