package com.jsnj33.suyuan.view.main

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Parcelable
import android.view.KeyEvent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.huantansheng.easyphotos.EasyPhotos
import com.huantansheng.easyphotos.models.album.entity.Photo
import com.jsnj33.suyuan.App
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.base.BaseActivity
import com.jsnj33.suyuan.repository.model.MainBean
import com.jsnj33.suyuan.repository.model.MenuBean
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.*
import com.jsnj33.suyuan.view.adapter.MenuAdapter
import com.jsnj33.suyuan.view.camera.ScanResultActivity
import com.jsnj33.suyuan.view.login.LoginActivity
import com.jsnj33.suyuan.view.pesticides.AddpesticidesActivity
import com.jsnj33.suyuan.view.record.FramingRecordActivity
import com.jsnj33.suyuan.view.record.PackActivity
import com.jsnj33.suyuan.view.record.PatrolRecordActivity
import com.jsnj33.suyuan.view.record.TransportActivity
import com.jsnj33.suyuan.viewmodel.MainViewModel
import com.leaf.library.StatusBarUtil
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import kotlin.collections.ArrayList

/**
 * 主页
 */
class MainActivity : BaseActivity<MainViewModel>() {
    /**
     * 该页面的公共方法
     */
    companion object {
        const val TAG = "MainActivity"
        const val REQUEST_PERMISSION = 101;
        fun start(activity: Activity) {
            var intent = Intent(activity, MainActivity::class.java)
            activity.startActivity(intent)
        }
    }

    val REQUEST_TAKE_PHOTO = 101
    var adapter: MenuAdapter? = null;
    var list = ArrayList<MenuBean>()
    var path = ""
    var personName: String? by PreferencesUtil("personName", null)

    override fun getLayoutID(): Int = R.layout.activity_main

    override fun initLoadView() {
        StatusBarUtil.setGradientColor(this, appBarLayout)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.isNestedScrollingEnabled = false
        adapter = MenuAdapter(list);
        mRecyclerView.adapter = adapter
        //申请权限
        viewModel.applypermission(this, packageManager, packageName)
        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(App.intance, R.color.colorPrimary))
    }

    override fun loadData() {
        viewModel.getMainData()
        viewModel.getUserInfo()
        tvTitleSub.text = StringUtils.setTitle(viewModel.traceIndustry)
    }

    /**
     * 监听数据变化
     */
    override fun watchListner() {
        mSwipeRefreshLayout.setOnRefreshListener {
            viewModel.getUserInfo()
        }
        adapter!!.setOnItemChildClickListener { adapter, view, position ->
            view.display
            var menuBean = adapter.data.get(position) as MenuBean;
            when (menuBean.type) {
                //添加农事生产
                0 -> AddpesticidesActivity.start(this@MainActivity, 3, menuBean.code)
                //添加农药
                1 -> AddpesticidesActivity.start(this@MainActivity, 0, menuBean.code)
                //添加施肥
                2 -> AddpesticidesActivity.start(this@MainActivity, 1, menuBean.code)
                //添加种子
                3 -> AddpesticidesActivity.start(this@MainActivity, 2, menuBean.code)
                //农事记录//
                4 -> FramingRecordActivity.start(this@MainActivity, 0, menuBean.code)
                //肥料记录
                5 -> {
                    var type = when (viewModel.traceIndustry) {
                        10007 -> 2
                        else -> 1
                    }
                    FramingRecordActivity.start(this@MainActivity, type, menuBean.code)
                }
                //农药记录
                6 -> {
                    var type = when (viewModel.traceIndustry) {
                        10007 -> 1
                        else -> 2
                    }
                    FramingRecordActivity.start(this@MainActivity, type, menuBean.code)
                }

                //种子记录
                7 ->
                    FramingRecordActivity.start(this@MainActivity, 3, menuBean.code)
                //添加生产
                8 -> AddpesticidesActivity.start(this@MainActivity, 4, menuBean.code)
                9 -> FramingRecordActivity.start(this@MainActivity, 9, menuBean.code)
                10 -> PackActivity.start(this@MainActivity, menuBean.code)
                11 -> TransportActivity.start(this@MainActivity, menuBean.code)
                12 -> PatrolRecordActivity.start(this@MainActivity)
            }
        }
        viewModel.apply {
            mainData.observe(this@MainActivity, Observer {
                adapter!!.setNewData(it)
            })
        }
        viewModel.mainBean.observe(this@MainActivity, Observer {
            mSwipeRefreshLayout.isRefreshing = false
            if (it.code == ResultStatus.SUCCESS) {
                var userInfo = it.data as MainBean
                tvAddress.text = "${userInfo.enterName ?: ""}"
                tvUserName.text = "${userInfo.personName ?: ""}"
                personName = "${userInfo.personName ?: ""}"
                when (viewModel.traceIndustry) {
                    10008 -> {
                        tvProductionNum.text = "${userInfo.breedCount.toString()}次"
                    }
                    10009 -> {
                        tvProductionNum.text = "${userInfo.fishCount.toString()}次"
                    }
                    else -> {
                        tvProductionNum.text = "${userInfo.framCount.toString()}次"
                    }
                }
                tvTouruNum.text = "${userInfo.productCount.toString()}次"
            } else {
                ToastUtil.show(it.message!!)
            }
        })
        //图片上传后的回调
        viewModel.uploadIMG.observe(this, Observer {
            mProgressBar.visibility = View.GONE
            if (it?.code == ResultStatus.SUCCESS) {
                var gson = Gson().toJson(it.data)
                ToastUtil.show("识别成功")
                ScanResultActivity.start(this, gson)
            } else {
                ToastUtil.show(it?.message!!)
            }
        })
        tvScan.setOnClickListener {
            ToastUtil.show("扫一扫")
            EasyPhotos.createAlbum(this, true, GlideEngine.getInstance())
                .setFileProviderAuthority("$packageName.fileprovider")
                .setCleanMenu(false)
                .setPuzzleMenu(false)
                .start(REQUEST_TAKE_PHOTO)
        }
        tvExit.setOnClickListener {
            LoginActivity.start(this)
            finish()
        }
    }

    /**
     * 权限申请结果回调
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.size == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
            ) {
            } else {
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    /**
     * 双击返回键退出程序
     */
    var time = 0.toLong();

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        var currentTime = System.currentTimeMillis();
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (currentTime - time > 2000) {
                ToastUtil.show("再点击一次退出程序")
                time = currentTime
                return true
            } else {
                return super.onKeyDown(keyCode, event)
            }
        }
        return false
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        viewModel.getUserInfo()
    }

    @Suppress("UNCHECKED_CAST")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            //拍照
            REQUEST_TAKE_PHOTO -> {
                mProgressBar.visibility = View.VISIBLE
                val resultPhotos =
                    data!!.getParcelableArrayListExtra<Parcelable>(EasyPhotos.RESULT_PHOTOS) as ArrayList<Photo>
                resultPhotos.forEach {
                    path = it.path
                }
                viewModel.scanIMGUplaod(File(path))
            }
        }
    }
}
