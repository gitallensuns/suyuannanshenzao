package com.jsnj33.suyuan

import android.app.Application
import com.jsnj33.suyuan.delegate.DelegateExt
import com.jsnj33.suyuan.util.CardUtil
import com.jsnj33.suyuan.util.LogUtil
import com.scwang.smartrefresh.layout.header.ClassicsHeader

import android.content.Context
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.api.RefreshHeader
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreator
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.scwang.smartrefresh.layout.footer.ClassicsFooter
import com.scwang.smartrefresh.layout.api.RefreshFooter
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreator
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.jsnj33.suyuan.util.PreferencesUtil


class App : Application() {

    companion object {
        var intance: App by DelegateExt.notNullSingleValue()
        const val TAG = "APP"
    }

    override fun onCreate() {
        super.onCreate()
        intance = this;
        LogUtil.d(TAG, "InitApplication")
        initSmartRefreshLayout()
        PreferencesUtil.get(this)
    }

    private fun initSmartRefreshLayout() {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, _ ->
            ClassicsHeader(context)
        }
        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator { context, _ ->
            //指定为经典Footer，默认是 BallPulseFooter
            ClassicsFooter(context).setDrawableSize(20f)
        }
    }
}