package com.jsnj33.suyuan.delegate

import android.app.Activity
import java.lang.ref.WeakReference
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * 委托弱引用Activity
 */
class WeakRefrenceDelegate<Activity> : ReadWriteProperty<Any?, Activity> {
    var weakReference: WeakReference<Activity>? = null;


    override fun getValue(thisRef: Any?, property: KProperty<*>): Activity {
        return weakReference!!.get()!!;
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: Activity) {

        weakReference = WeakReference(value);
    }

}