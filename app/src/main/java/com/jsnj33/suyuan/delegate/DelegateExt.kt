package com.jsnj33.suyuan.delegate

import kotlin.properties.ReadWriteProperty

/**
 * 委托工具类
 */
object DelegateExt {
    /**
     * 判断对象是否为null
     */
    fun <T> notNullSingleValue(): ReadWriteProperty<Any?, T> = NotNullSingleValueVar()

}