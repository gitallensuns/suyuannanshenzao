package com.jsnj33.suyuan.delegate

import java.lang.IllegalArgumentException
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class NotNullSingleValueVar<T> : ReadWriteProperty<Any?, T> {
    var value: T? = null;
    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value!!;
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = value
    }
}