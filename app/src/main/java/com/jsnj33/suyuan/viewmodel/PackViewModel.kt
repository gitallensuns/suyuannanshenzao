package com.jsnj33.suyuan.viewmodel

import androidx.lifecycle.MutableLiveData
import com.jsnj33.suyuan.repository.model.*
import com.jsnj33.suyuan.base.BaseViewModel
import com.jsnj33.suyuan.http.RxHelper
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.client.RetrofitClient
import com.jsnj33.suyuan.util.ToastUtil
import io.reactivex.functions.Consumer


/**
 * 农事的VM层
 */
class PackViewModel : BaseViewModel() {
    val packList = MutableLiveData<HttpResponseList<PackBean>>()
    val transportList = MutableLiveData<HttpResponseList<TransportBean>>()
    val productionList = MutableLiveData<HttpResponse<List<PesticidesBean>>>()
    val addStatus = MutableLiveData<HttpResponse<Any>>()
    val addTransportStatus = MutableLiveData<HttpResponse<Any>>()

    /**
     * 获取添加农事数据
     */
    fun getDataList(page: Int, processStatus: Int, productName: String? = ""): ResultListing<HttpResponseList<PackBean>> {
        RetrofitClient.getInstance().getPackRecordList(RequestPackParams(processStatus, productName), page, 10)
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                packList.value = it
            }, Consumer {
                packList.value = HttpResponseList(code = ResultStatus.FAILURE, message = "${it.message}", data = Data(0, 0, ArrayList(), false, 0, 0, false), status = "2000")
            })
        return ResultListing(packList)
    }

    /**
     * 获取运输管理记录
     */
    fun getDataTansPortList(page: Int, processStatus: Int, productName: String? = ""): ResultListing<HttpResponseList<TransportBean>> {
        RetrofitClient.getInstance().getTransportRecordList(RequestPackParams(processStatus, productName), page, 10)
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                transportList.value = it
            }, Consumer {
                transportList.value = HttpResponseList(code = ResultStatus.FAILURE, message = "${it.message}", data = Data(0, 0, ArrayList(), false, 0, 0, false), status = "2000")
            })
        return ResultListing(transportList)
    }

    //获取生产档案号
    fun getPestList(processStatus: String, personCharge: String): ResultListing<HttpResponse<List<PesticidesBean>>> {
        var api = when (processStatus) {
            //自产包装档案号
            "10000" -> {
                RetrofitClient.getInstance().traceProductionArchi(RequestParams(personCharge = personCharge))
                    .compose(RxHelper.rxSchedulerHelper())
            }
            //宰杀包装档案号  10001
            else -> RetrofitClient.getInstance().traceProduction2Archi(RequestParams(personCharge = personCharge))
                .compose(RxHelper.rxSchedulerHelper())
        }
        api.subscribe(Consumer {
            productionList.value = it
        }, Consumer {
            productionList.value = HttpResponse(it.message)
        })
        return ResultListing(productionList)
    }

    /**
     * 新增包装管理
     */
    fun addPackManage(requstAddPackParams: RequstAddPackParams): ResultListing<HttpResponse<Any>> {
        RetrofitClient.getInstance().addPackAsync(requstAddPackParams)
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                addStatus.value = it
            }, Consumer {
                addStatus.value = HttpResponse(it.message, ResultStatus.FAILURE)
            })
        return ResultListing(addStatus)
    }

    /**
     * 新增运输管理
     */
    fun addTransportAsync(requstAddTransportParams: RequstAddTransportParams): ResultListing<HttpResponse<Any>> {
        RetrofitClient.getInstance().addTransportAsync(requstAddTransportParams)
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                addTransportStatus.value = it
            }, Consumer {
                addTransportStatus.value = HttpResponse(it.message, ResultStatus.FAILURE)
            })
        return ResultListing(addTransportStatus)
    }
}