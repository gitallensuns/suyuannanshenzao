package com.jsnj33.suyuan.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.jsnj33.suyuan.http.RxHelper
import com.jsnj33.suyuan.repository.model.*
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.util.ToastUtil
import com.jsnj33.suyuan.view.adapter.ChooseAdapter
import com.jsnj33.suyuan.base.BaseViewModel
import com.jsnj33.suyuan.repository.client.RetrofitClient
import io.reactivex.functions.Consumer
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File

/**
 * 农事的VM层
 */
class PesticidesViewModel : BaseViewModel() {
    val dataList = MutableLiveData<HttpResponse<List<PesticidesBean>>>();
    val chooseList = MutableLiveData<List<ChooseBean>>();
    val list = ArrayList<ChooseBean>();
    var dataListBean = ArrayList<DataListBean>();

    /**
     * traceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)
     */
    var traceIndustry: Int by PreferencesUtil("traceIndustry", 10007)
    val uploadIMG = MutableLiveData<HttpResponse<List<DataListBean>>>()
    var beanId = ""
    private var dataListBeans = ArrayList<DataListBean>()

    var isAdd: Boolean = false

    /**
     * 获取选中数据
     */
    fun getDataList(personCharge: String): ResultListing<List<PesticidesBean>> {
        RetrofitClient.getInstance().traceProductionArchi(RequestParams(personCharge = personCharge))
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(
                Consumer {
                    it.run {
                        if (this.code == ResultStatus.SUCCESS) dataList.value = this
                        else dataList.value = HttpResponse(it.message, ResultStatus.FAILURE)
                    }
                },
                Consumer {
                    dataList.value = HttpResponse(it.message, ResultStatus.FAILURE)
                }
            )
        return ResultListing(null, dataList)
    }

    /**
     *下拉菜单数据
     */
    fun getChooseList(type: Int): ResultListing<List<ChooseBean>> {
        var inputCategory: Int
        var api =
            when (traceIndustry) {
                //畜禽
                10008 -> {
                    inputCategory = when (type) {
                        0 -> 102004
                        1 -> 102007
                        else -> 102009
                    }
                    RetrofitClient.getInstance()
                        .traceProductionInputX(RequestPestParams(inputCategory))
                        .compose(RxHelper.rxSchedulerHelper())
                }
                //渔业
                10009 -> {
                    inputCategory = when (type) {
                        0 -> 10101//添加喂投
                        1 -> 10105//添加渔苗
                        else -> 10104//添加鱼药
                    }
                    RetrofitClient.getInstance()
                        .traceProductionInputYY(RequestPestParams(inputCategory))
                        .compose(RxHelper.rxSchedulerHelper())
                }
                //农业
                else -> {
                    inputCategory = when (type) {
                        0 -> 11
                        1 -> 10
                        else -> 9
                    }
                    RetrofitClient.getInstance()
                        .traceProductionInput(RequestPestParams(inputCategory))
                        .compose(RxHelper.rxSchedulerHelper())
                }
            }

        api.subscribe(
            Consumer {
                it.run {
                    if (this.code == ResultStatus.SUCCESS) {
                        if (it.data?.size!! > 0) {
                            isAdd = true
                            dataListBean = (it.data as ArrayList<DataListBean>?)!!
                            list.clear()
                            list.add(ChooseBean(0, ChooseAdapter.TAGITEM, "", it.data))
                            list.add(ChooseBean(-1, ChooseAdapter.TAGADD, "0"))
                            chooseList.value = list
                        } else {
                            chooseList.value = null
                        }

                    } else {
                        dataList.value = null
                        ToastUtil.show(it.message!!)
                    }
                }
            },
            Consumer {
                dataList.value = null
            }
        )
        return ResultListing(chooseList)
    }

    fun getChooseList2(type: Int): ResultListing<List<ChooseBean>> {
        var inputCategory: Int
        var api =
            when (traceIndustry) {
                //畜禽
                10008 -> {
                    inputCategory = when (type) {
                        0 -> 102004
                        1 -> 102007
                        else -> 102009
                    }
                    RetrofitClient.getInstance()
                        .traceProductionInputX(RequestPestParams(inputCategory))
                        .compose(RxHelper.rxSchedulerHelper())
                }
                //渔业
                10009 -> {
                    inputCategory = when (type) {
                        0 -> 10101//添加喂投
                        1 -> 10105//添加渔苗
                        else -> 10104//添加鱼药
                    }
                    RetrofitClient.getInstance()
                        .traceProductionInputYY(RequestPestParams(inputCategory))
                        .compose(RxHelper.rxSchedulerHelper())
                }
                //农业
                else -> {
                    inputCategory = when (type) {
                        0 -> 10026
                        1 -> 10027
                        else -> 10028
                    }
                    RetrofitClient.getInstance()
                        .traceProductionInput(RequestPestParams(inputCategory))
                        .compose(RxHelper.rxSchedulerHelper())
                }
            }

        api.subscribe(
            Consumer {
                it.run {
                    if (this.code == ResultStatus.SUCCESS) {
                        if (it.data?.size!! > 0) {
                            dataListBeans = it.data as ArrayList<DataListBean>
                            var index = 0
                            if (beanId.isNotEmpty()) {
                                dataListBeans.forEachIndexed { inde, dataListBean ->
                                    if ("${dataListBean.id}" == beanId) {
                                        index = inde
                                        return@forEachIndexed
                                    }
                                }
                            }
                            list.forEach { choose ->
                                choose.dataList = it.data
                                choose.position = index
                            }
                            chooseList.value = list
                        } else {
                            chooseList.value = null
                        }

                    } else {
                        dataList.value = null
                        ToastUtil.show(it.message!!)
                    }
                }
            },
            Consumer {
                dataList.value = null
            }
        )
        return ResultListing(chooseList)
    }

    /**
     * 添加项
     */
    fun addItem(): ResultListing<List<ChooseBean>> {
        list.add(list.size - 1, ChooseBean(list.size - 1, ChooseAdapter.TAGITEM, "", dataListBean))
        chooseList.postValue(list)
        return ResultListing(chooseList)
    }

    /**
     * 移除项
     */
    fun removeItem(position: Int): ResultListing<List<ChooseBean>> {
        list.removeAt(position)
        chooseList.postValue(list)
        return ResultListing(chooseList)
    }

    /**
     * 上传图片识别
     */
    @SuppressLint("CheckResult")
    fun scanIMGUplaod(file: File?): ResultListing<HttpResponse<List<DataListBean>>> {
        var map = mutableMapOf<String, RequestBody>()
        val requestFile = RequestBody.create(MediaType.parse(""), File(file!!.path))
        map["images\"; filename=\"\" ${File(file.path)}"] = requestFile

        RetrofitClient.getInstance().scanIMG2(map).compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                if (it.code == ResultStatus.SUCCESS) {
                    beanId = "${it.data?.id}"
                    uploadIMG.value =
                        HttpResponse(it.message, it.code, it.date, it.msg, listOf(it.data!!))
                } else {
                    uploadIMG.value = HttpResponse("匹配度不够 重新上传", ResultStatus.FAILURE, 0, it.msg, null)
                }
            }, Consumer {
                uploadIMG.value =
                    HttpResponse("匹配度不够 重新上传", ResultStatus.FAILURE, 0, "匹配度不够 重新上传", null)
            })
        return ResultListing(uploadIMG)
    }


}