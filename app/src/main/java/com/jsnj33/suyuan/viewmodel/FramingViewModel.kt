package com.jsnj33.suyuan.viewmodel

import androidx.core.net.toFile
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.http.RxHelper
import com.jsnj33.suyuan.repository.model.*
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.LogUtil
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.view.adapter.UploadIMGAdapter
import com.jsnj33.suyuan.view.framing.UploadIMGActivity
import com.jsnj33.suyuan.base.BaseViewModel
import com.jsnj33.suyuan.repository.client.RetrofitClient
import io.reactivex.functions.Consumer
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * 农事的VM层
 */
class FramingViewModel : BaseViewModel() {
    val dataList = MutableLiveData<List<FramingBean>>();

    val dataUploadList = MutableLiveData<List<UploadIMGBean>>()

    var dataIMG: MutableList<UploadIMGBean>? = null//图片集合

    var partList = ArrayList<MultipartBody.Part>()//上传图片的入参

    var uploadIMGResult = MutableLiveData<HttpResponse<Any>>()

    var framingDetail = MutableLiveData<HttpResponse<FramingDetailBean>>()

    /**
     * 获取地块列表
     */
    var productionMassif = MutableLiveData<HttpResponseList<ListBean>>()

    /**
     * 获取栋舍列表
     */
    var livestockField = MutableLiveData<HttpResponseList<ListBean>>()

    /**
     * 获取鱼塘列表
     */
    var tangkou = MutableLiveData<HttpResponseList<ListBean>>()


    val tracePhenologicalPhase = MutableLiveData<HttpResponseList<WHBean>>();
    val tracePlantSeason = MutableLiveData<HttpResponse<XMBean>>();


    /**
     * traceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)
     */
    var traceIndustry: Int by PreferencesUtil("traceIndustry", 10007)

    /**
     * 获取添加农事数据
     */
    fun getDataList(): ResultListing<List<FramingBean>> {
        when (traceIndustry) {

            10008 -> {
                var data = listOf(
                    FramingBean(
                        "清洁", 1020102, R.mipmap.ic_qingjie, 0
                    ), FramingBean(
                        "消毒", 1020104, R.mipmap.ic_xiaodu, 0
                    ), FramingBean(
                        "其他", 1020107, R.mipmap.ic_other, 0
                    )
                )

//                FramingBean(
//                    "防疫", 1020103, R.mipmap.ic_chayang, 0
//                )，
//                FramingBean(
//                    "投药", 1020105, R.mipmap.ic_chayang, 0
//                )
//                FramingBean(
//                    "投幼崽", 1020106, R.mipmap.ic_chayang, 0
//                ),
                dataList.postValue(data)
            }
            10009 -> {
                var data = listOf(
                    FramingBean(
                        "增氧", 300102, R.mipmap.ic_zengyang, 0
                    ), FramingBean(
                        "加水", 300103, R.mipmap.ic_jiashui, 0
                    ), FramingBean(
                        "清淤", 300104, R.mipmap.ic_qingyu, 0
                    ), FramingBean(
                        "其他", 300108, R.mipmap.ic_other, 0
                    )
                )

//                FramingBean(
//                    "投喂", 300101, R.mipmap.ic_weitou, 0
//                ),
//                , FramingBean(
//                    "疫苗", 300105, R.mipmap.ic_yimiao, 0
//                ), FramingBean(
//                "渔药", 300106, R.mipmap.ic_chayang, 0
//                ), FramingBean(
//                "鱼苗", 300107, R.mipmap.ic_chayang, 0
//                ),
                dataList.postValue(data)
            }
            else -> {
                var data = listOf(
                    FramingBean(
                        "灌溉", 100102, R.mipmap.ic_guangai, 0
                    ), FramingBean(
                        "除草", 100105, R.mipmap.ic_chucao, 0
                    ), FramingBean(
                        "插秧", 100106, R.mipmap.ic_chayang, 0
                    ), FramingBean(
                        "翻地", 100107, R.mipmap.ic_fandi, 0
                    ), FramingBean(
                        "其他", 100108, R.mipmap.ic_other, 0
                    )
                )
                dataList.postValue(data)
            }
        }

        return ResultListing(dataList)
    }

    /**
     * 添加图片
     */
    fun initPicControl(): ResultListing<List<UploadIMGBean>> {
        dataIMG = mutableListOf(UploadIMGBean(null, UploadIMGAdapter.ADDIMG))
        dataUploadList.postValue(dataIMG)
        return ResultListing(dataUploadList)
    }

    /**
     * 添加图片
     */
    fun addPicture(uploadIMGBean: UploadIMGBean): ResultListing<List<UploadIMGBean>> {
        dataIMG!!.add(0, uploadIMGBean)
        LogUtil.d("IMG", uploadIMGBean.file?.path)
        dataUploadList.postValue(dataIMG)
        return ResultListing(dataUploadList)
    }

    /**
     * 获取地块列表
     * 获取栋舍列表
     * 获取鱼塘列表
     */
    fun getList(archiId: String): ResultListing<HttpResponseList<ListBean>> {
        when (traceIndustry) {
            10008 -> {
                RetrofitClient.getInstance().livestockFieldlist(RequestProductionAddressBean(archiId))
                    .compose(RxHelper.rxSchedulerHelper())
                    .subscribe(Consumer {
                        if (it.code == ResultStatus.SUCCESS) {
                            it.data.records.forEach { data ->
                                data.showName = data.fieldName
                            }
                            livestockField.postValue(it)
                        } else {
                            livestockField.postValue(
                                HttpResponseList(
                                    ResultStatus.FAILURE,
                                    it.message,
                                    Data(0, 0, ArrayList(), false, 0, 0, false)
                                )
                            )
                        }
                    }, Consumer {
                        livestockField.postValue(
                            HttpResponseList(
                                ResultStatus.FAILURE,
                                it.message!!,
                                Data(0, 0, ArrayList(), false, 0, 0, false)
                            )
                        )
                    })
                return ResultListing(livestockField)
            }
            10009 -> {
                RetrofitClient.getInstance().tangkouList(RequestProductionAddressBean(archiId))
                    .compose(RxHelper.rxSchedulerHelper())
                    .subscribe(Consumer {
                        if (it.code == ResultStatus.SUCCESS) {
                            it.data.records.forEach { data ->
                                data.showName = data.fieldName
                            }
                            tangkou.postValue(it)
                        } else {
                            tangkou.postValue(
                                HttpResponseList(
                                    ResultStatus.FAILURE,
                                    it.message,
                                    Data(0, 0, ArrayList(), false, 0, 0, false)
                                )
                            )
                        }
                    }, Consumer {
                        tangkou.postValue(
                            HttpResponseList(
                                ResultStatus.FAILURE,
                                it.message!!,
                                Data(0, 0, ArrayList(), false, 0, 0, false)
                            )
                        )
                    })
                return ResultListing(tangkou)
            }
            else -> {
                RetrofitClient.getInstance().productionMassifList(RequestProductionAddressBean(archiId))
                    .compose(RxHelper.rxSchedulerHelper())
                    .subscribe(Consumer {
                        if (it.code == ResultStatus.SUCCESS) {
                            it.data.records.forEach { data ->
                                data.showName = "${data.massifName}"
                            }
                            productionMassif.postValue(it)
                        } else {
                            productionMassif.postValue(
                                HttpResponseList(

                                    ResultStatus.FAILURE,
                                    it.message,
                                    Data(0, 0, ArrayList(), false, 0, 0, false)
                                )
                            )
                        }
                    }, Consumer {
                        productionMassif.postValue(
                            HttpResponseList(
                                ResultStatus.FAILURE,
                                it.message!!,
                                Data(0, 0, ArrayList(), false, 0, 0, false)
                            )
                        )
                    })
                return ResultListing(productionMassif)
            }
        }
    }

    /**
     * 删除图片
     */
    fun deletePicture(pos: Int): ResultListing<List<UploadIMGBean>> {
        dataIMG!!.removeAt(pos)
        dataUploadList.postValue(dataIMG)
        return ResultListing(dataUploadList)
    }

    /**
     * 上传图片
     */
    fun uploadIMG(): ResultListing<HttpResponse<Any>> {
        var map = mutableMapOf<String, RequestBody>()
        dataIMG!!.forEach {
            if (it.type == UploadIMGAdapter.PICTAG) {
                val requestFile = RequestBody.create(MediaType.parse(""), it.file!!.toFile())
                map["files\"; filename=\"\" ${it.file.toFile()}"] = requestFile
            }
        }

        RetrofitClient.getInstance().uploadIMG(map).compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                if (it.code == ResultStatus.SUCCESS) {
                    uploadIMGResult.postValue(it)
                } else {
                    uploadIMGResult.postValue(HttpResponse(it.msg, ResultStatus.FAILURE))
                }
                LogUtil.d(UploadIMGActivity.TAG, it.message)
            }, Consumer {
                uploadIMGResult.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
            })
        return ResultListing(uploadIMGResult)
    }

    /**
     * 新增单个农事管理表详情
     */
    fun addAppProductFarming(
        taskPic: String,
        farmingCode: String,
        archild: String,
        massifName: String? = null,
        massifId: String? = null,
        product: ArrayList<ProductList>? = null,
        phenologicalPhaseId: String? = "",
        phenologicalPhaseName: String? = ""
    ): ResultListing<HttpResponse<FramingDetailBean>> {
        var prod = if (product == null) "" else Gson().toJson(product)
        var api = when (traceIndustry) {
            10008 -> {
                RetrofitClient.getInstance()
                    .addAppProductFarmingX(
                        RequestXMParams(
                            taskPic,
                            farmingCode,
                            archild,
                            prod,
                            massifName
                        )
                    ).compose(RxHelper.rxSchedulerHelper())
            }
            10009 -> {
                RetrofitClient.getInstance()
                    .addAppProductFarmingY(
                        RequestYYParams(
                            taskPic,
                            farmingCode,
                            archild,
                            prod,
                            massifName
                        )
                    ).compose(RxHelper.rxSchedulerHelper())
            }
            else -> {
                RetrofitClient.getInstance()
                    .addAppProductFarmingN(
                        RequestFramingParams(
                            taskPic,
                            farmingCode,
                            archild,
                            prod,
                            massifName,
                            massifId,
                            phenologicalPhaseId,
                            phenologicalPhaseName
                        )
                    ).compose(RxHelper.rxSchedulerHelper())
            }
        }
        api.subscribe(Consumer {
            if (it.code == ResultStatus.SUCCESS) {
                framingDetail.postValue(it!!)
            } else {
                framingDetail.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
            }
        },
            Consumer {
                framingDetail.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
            })
        return ResultListing(framingDetail)
    }


    /**
     * 获取物候期列表
     */
    fun getTracePhenologicalPhase(plantType: String) {
        RetrofitClient.getInstance().tracePhenologicalPhase(RequestWHParams("$plantType"))
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(
                Consumer {
                    tracePhenologicalPhase.postValue(it)
                },
                Consumer {
                    tracePhenologicalPhase.postValue(
                        HttpResponseList(
                            ResultStatus.FAILURE,
                            it.message!!,
                            Data<WHBean>(0, 0, ArrayList(), false, 0, 0, false)
                        )
                    )
                }
            )
    }

    /**
     * 获取小麦与水稻的值
     */
    fun tracePlantSeason(id: String) {
        RetrofitClient.getInstance().tracePlantSeason(id)
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(
                Consumer {
                    tracePlantSeason.postValue(it)
                }, Consumer {
                    tracePlantSeason.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
                }
            )
    }
}