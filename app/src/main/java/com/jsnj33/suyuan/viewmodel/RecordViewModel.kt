package com.jsnj33.suyuan.viewmodel

import androidx.lifecycle.MutableLiveData
import com.jsnj33.suyuan.http.RxHelper
import com.jsnj33.suyuan.repository.model.*
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.base.BaseViewModel
import com.jsnj33.suyuan.repository.client.RetrofitClient
import io.reactivex.functions.Consumer

/**
 * 记录相关的VM层
 */
class RecordViewModel : BaseViewModel() {
    val framingList = MutableLiveData<HttpResponseList<FramingDetailBean>>()
    val otherList = MutableLiveData<HttpResponseList<TraceProductionBean>>()
    val otherList2 = MutableLiveData<HttpResponseList<AddProductBean>>()

    /**
     * 农事生产详情
     */
    val framingRecordDetail = MutableLiveData<HttpResponse<FramingDetailBean>>();
    val oterRecordDetail = MutableLiveData<HttpResponse<TraceProductionBean>>();

    /**
     * traceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)
     */
    var traceIndustry: Int by PreferencesUtil("traceIndustry", 10007)

    /***
     * 获取农事记录
     * @param sort  //传1 代表农事生产(其余则不传)
     * @param farmingCode //100102 肥料，100103农药 100100种子
     * @param page
     */
    fun getFramingRecordList(
        sort: Int? = 0,
        page: Int? = 1
    ): ResultListing<HttpResponseList<FramingDetailBean>> {
        when (traceIndustry) {
            10008 -> {
                var tracePeasantId: Long by PreferencesUtil("tracePeasantId", 0)
                RetrofitClient.getInstance()
                    .getFramingRecordListX(
                        RequestFramingRecordParams(sort, tracePeasantId),
                        page,
                        mPageSize
                    )
                    .compose(RxHelper.rxSchedulerHelper())
            }
            10009 -> {
                var tracePeasantId: Long by PreferencesUtil("tracePeasantId", 0)
                RetrofitClient.getInstance()
                    .getFramingRecordListYY(
                        RequestFramingRecordParams(sort, tracePeasantId),
                        page,
                        mPageSize
                    )
                    .compose(RxHelper.rxSchedulerHelper())
            }
            else -> {
                RetrofitClient.getInstance()
                    .getFramingRecordList(RequestFramingRecordParams(sort), page, mPageSize)
                    .compose(RxHelper.rxSchedulerHelper())
            }
        }

            .subscribe(
                Consumer {
                    framingList.postValue(it)
                },
                Consumer {
                    framingList.postValue(
                        HttpResponseList(
                            ResultStatus.FAILURE,
                            it.message!!,
                            Data<FramingDetailBean>(0, 0, ArrayList(), false, 0, 0, false)
                        )
                    )
                }
            )
        return ResultListing(framingList)
    }

    /**
     * 生产添加记录
     */
    fun getAddProductionRecordList(
        page: Int? = 1
    ): ResultListing<HttpResponseList<AddProductBean>> {
        RetrofitClient.getInstance()
            .productionList(
                RequestParams(),
                page,
                mPageSize
            )
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(
                Consumer {
                    otherList2.postValue(it)
                },
                Consumer {
                    otherList2.postValue(
                        HttpResponseList(
                            ResultStatus.FAILURE,
                            it.message!!,
                            Data<AddProductBean>(0, 0, ArrayList(), false, 0, 0, false)
                        )
                    )
                }
            )
        return ResultListing(otherList2)
    }



    /***
     * 施肥、农药、种子的记录
     * @param farmingCode //100102 肥料，100103农药 100100种子
     * @param page
     */
    fun getOtherRecordList(
        farmingCode: Int? = 0,
        page: Int? = 1
    ): ResultListing<HttpResponseList<TraceProductionBean>> {
        var api = when (traceIndustry) {
            10008 -> {
                var tracePeasantId: Long by PreferencesUtil("tracePeasantId", 0)
                RetrofitClient.getInstance()
                    .getOtherRecordListX(
                        RequestOtherRecordParamsX(tracePeasantId, farmingCode),
                        page,
                        mPageSize
                    )
                    .compose(RxHelper.rxSchedulerHelper())
            }
            10009 -> {
                var tracePeasantId: Long by PreferencesUtil("tracePeasantId", 0)
                RetrofitClient.getInstance()
                    .getOtherRecordListYY(
                        RequestOtherRecordParamsYY(tracePeasantId, farmingCode),
                        page,
                        mPageSize
                    )
                    .compose(RxHelper.rxSchedulerHelper())
            }
            else -> {
                RetrofitClient.getInstance()
                    .getOtherRecordListN(RequestOtherRecordParams(farmingCode), page, mPageSize)
                    .compose(RxHelper.rxSchedulerHelper())
            }
        }

        api.subscribe(
            Consumer {
                otherList.postValue(it)
            },
            Consumer {
                otherList.postValue(
                    HttpResponseList(
                        ResultStatus.FAILURE,
                        it.message!!,
                        Data<TraceProductionBean>(0, 0, ArrayList(), false, 0, 0, false)
                    )
                )
            }
        )
        return ResultListing(otherList)
    }

    /***
     * 获取数据
     * @param sort
     * @param farmingCode
     * @param page
     * @param type
     */
    fun getData(sort: Int? = 0, farmingCode: Int? = 0, page: Int? = 1, type: Int) {
        when (type) {
            0 ->
                //农事生产记录
                getFramingRecordList(sort, page)
            9 -> {
                getAddProductionRecordList(page)
            }
            else ->
                //施肥、农药、种子的记录
                getOtherRecordList(farmingCode, page)
        }
    }


    /**
     * 获取农事详情
     */
    fun getFramingRecordDetailData(framingId: Long): ResultListing<HttpResponse<FramingDetailBean>> {
        var api = when (traceIndustry) {
            10008 -> {
                RetrofitClient.getInstance()
                    .getFramingRecordDetailByIdX(framingId)
                    .compose(RxHelper.rxSchedulerHelper())
            }
            10009 -> {
                RetrofitClient.getInstance()
                    .getFramingRecordDetailByIdYY(framingId)
                    .compose(RxHelper.rxSchedulerHelper())
            }
            else -> {
                RetrofitClient.getInstance()
                    .getFramingRecordDetailById(framingId)
                    .compose(RxHelper.rxSchedulerHelper())
            }
        }

        api.subscribe(
            Consumer {
                framingRecordDetail.postValue(it)
            },
            Consumer {
                framingRecordDetail.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
            }
        )
        return ResultListing(framingRecordDetail)
    }

    /**
     * 获取农药、种子、化肥详情
     */
    fun getOtherRecordDetailData(framingId: Long): ResultListing<HttpResponse<TraceProductionBean>> {
        var api = when (traceIndustry) {
            10008 -> {
                RetrofitClient.getInstance().getOtherRecordDetailByIdX(framingId)
                    .compose(RxHelper.rxSchedulerHelper())
            }
            10009 -> {
                RetrofitClient.getInstance().getOtherRecordDetailByIdYY(framingId)
                    .compose(RxHelper.rxSchedulerHelper())
            }
            else -> {
                RetrofitClient.getInstance()
                    .getOtherRecordDetailByIdN(framingId)
                    .compose(RxHelper.rxSchedulerHelper())
            }
        }
        api.subscribe(
            Consumer {
                oterRecordDetail.postValue(it)
            },
            Consumer {
                oterRecordDetail.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
            }
        )
        return ResultListing(oterRecordDetail)
    }

}
