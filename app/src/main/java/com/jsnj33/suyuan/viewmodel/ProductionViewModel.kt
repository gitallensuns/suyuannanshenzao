package com.jsnj33.suyuan.viewmodel

import androidx.core.net.toFile
import androidx.lifecycle.MutableLiveData
import com.jsnj33.suyuan.base.BaseViewModel
import com.jsnj33.suyuan.http.RxHelper
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.client.RetrofitClient
import com.jsnj33.suyuan.repository.model.*
import com.jsnj33.suyuan.util.LogUtil
import com.jsnj33.suyuan.view.adapter.UploadIMGAdapter
import com.jsnj33.suyuan.view.framing.UploadIMGActivity
import io.reactivex.functions.Consumer
import okhttp3.MediaType
import okhttp3.RequestBody

/**
 *@Author 萤火虫
 *@Date 2021/6/3
 *@Desc
 */
class ProductionViewModel : BaseViewModel() {
    val addResult = MutableLiveData<HttpResponse<Any>>();

    var uploadIMGResult = MutableLiveData<HttpResponse<ArrayList<UploadIMGBean2>>>()

    /**
     * 添加农产品
     */
    fun addProduction(requestProductionParams: RequestProductionParams): ResultListing<HttpResponse<Any>> {
        RetrofitClient.getInstance().addProduction(requestProductionParams)
            .compose(RxHelper.rxSchedulerHelper()).subscribe(
                Consumer {
                    addResult.postValue(it)
                },
                Consumer {
                    addResult.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
                })
        return ResultListing(addResult)
    }

    /**
     * 上传图片
     */
    fun uploadIMG(dataIMG: ArrayList<UploadIMGBean>): ResultListing<HttpResponse<ArrayList<UploadIMGBean2>>> {
        var map = mutableMapOf<String, RequestBody>()
        dataIMG?.forEach {
            if (it.type == UploadIMGAdapter.PICTAG) {
                val requestFile = RequestBody.create(MediaType.parse(""), it.file!!.toFile())
                map["files\"; filename=\"\" ${it.file.toFile()}"] = requestFile
            }
        }
        RetrofitClient.getInstance().uploadIMG2(map).compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                if (it.code == ResultStatus.SUCCESS) {
                    uploadIMGResult.postValue(it)
                } else {
                    uploadIMGResult.postValue(HttpResponse(it.msg, ResultStatus.FAILURE))
                }
            }, Consumer {
                uploadIMGResult.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
            })
        return ResultListing(uploadIMGResult)
    }

}