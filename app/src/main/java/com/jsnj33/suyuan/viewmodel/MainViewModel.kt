package com.jsnj33.suyuan.viewmodel

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.lifecycle.MutableLiveData
import com.jsnj33.suyuan.BuildConfig
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.http.RxHelper
import com.jsnj33.suyuan.repository.model.*
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.view.adapter.MenuAdapter
import com.jsnj33.suyuan.view.main.MainActivity
import com.jsnj33.suyuan.base.BaseViewModel
import com.jsnj33.suyuan.repository.client.RetrofitClient
import io.reactivex.functions.Consumer
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File

class MainViewModel : BaseViewModel() {

    val mainData = MutableLiveData<List<MenuBean>>()
    val mainBean = MutableLiveData<HttpResponse<MainBean>>()
    val uploadIMG = MutableLiveData<HttpResponse<ScanEntity>>();

    /**
     * traceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)
     */
    var traceIndustry: Int by PreferencesUtil("traceIndustry", 10007)

    /**
     * 首页数据集
     */
    fun getMainData(): ResultListing<List<MenuBean>> {

        var data = ArrayList<MenuBean>()
        when (traceIndustry) {
            //种植业
            10007 -> {
                data.add(MenuBean("农事生产", R.mipmap.ic_add, R.drawable.shape_frarming_product, 0, MenuAdapter.ONE, 0))
                data.add(MenuBean("添加农药", R.mipmap.ic_add, R.drawable.shape_addnongyao, 1, MenuAdapter.ONE, 100104))
                data.add(MenuBean("添加肥料", R.mipmap.ic_add, R.drawable.shape_addfeiliao, 2, MenuAdapter.ONE, 100103))
                data.add(MenuBean("添加种子", R.mipmap.ic_add, R.drawable.shape_addzhongzi, 3, MenuAdapter.ONE, 100101))
                if (BuildConfig.FLAVOR == "guanyun") {
                    data.add(MenuBean("添加生产", R.mipmap.ic_add, R.drawable.shape_addzhongzi, 8, MenuAdapter.ONE, 0))
                    data.add(MenuBean("生产记录", R.mipmap.ic_nongshi, 0, 9, MenuAdapter.TOW, 0))
                }
                data.add(MenuBean("农事记录", R.mipmap.ic_nongshi, 0, 4, MenuAdapter.TOW, 0))
                data.add(MenuBean("农药记录", R.mipmap.ic_nongyao, 0, 5, MenuAdapter.TOW, 100104))
                data.add(MenuBean("肥料记录", R.mipmap.ic_feiliao, 0, 6, MenuAdapter.TOW, 100103))
                data.add(MenuBean("种子记录", R.mipmap.ic_zhongzi, 0, 7, MenuAdapter.TOW, 100101))
                data.add(MenuBean("巡田记录", R.mipmap.ic_nongshi, 0, 12, MenuAdapter.TOW, 0))
            }
            /**
             *畜牧业
             */
            10008 -> {
                data.add(MenuBean("养殖管理", R.mipmap.ic_add, R.drawable.shape_frarming_product, 0, MenuAdapter.ONE, 0))
                data.add(MenuBean("添加投喂", R.mipmap.ic_add, R.drawable.shape_addnongyao, 1, MenuAdapter.ONE, 1020101))
                data.add(MenuBean("投放药品", R.mipmap.ic_add, R.drawable.shape_addfeiliao, 2, MenuAdapter.ONE, 1020105))
                data.add(MenuBean("投入幼崽", R.mipmap.ic_add, R.drawable.shape_addzhongzi, 3, MenuAdapter.ONE, 1020106))
                data.add(MenuBean("养殖记录", R.mipmap.ic_nongshi, 0, 4, MenuAdapter.TOW, 0))
                data.add(MenuBean("投喂记录", R.mipmap.ic_feiliao, 0, 5, MenuAdapter.TOW, 1020101))
                data.add(MenuBean("投药记录", R.mipmap.ic_nongyao, 0, 6, MenuAdapter.TOW, 1020105))
                data.add(MenuBean("幼崽记录", R.mipmap.ic_zhongzi, 0, 7, MenuAdapter.TOW, 1020106))
                if (BuildConfig.FLAVOR == "xuyi") {
                    data.add(MenuBean("包装记录", R.mipmap.ic_pack, 0, 10, MenuAdapter.TOW, 10008))
                    data.add(MenuBean("运输记录", R.mipmap.ic_tansport, 0, 11, MenuAdapter.TOW, 10008))
                }
            }
            /**
             * 渔业
             */
            10009 -> {
                data.add(MenuBean("渔业养殖", R.mipmap.ic_add, R.drawable.shape_frarming_product, 0, MenuAdapter.ONE, 0))
                data.add(MenuBean("添加投喂", R.mipmap.ic_add, R.drawable.shape_addnongyao, 1, MenuAdapter.ONE, 300101))
                data.add(MenuBean("添加鱼苗", R.mipmap.ic_add, R.drawable.shape_addfeiliao, 2, MenuAdapter.ONE, 300107))
                data.add(MenuBean("添加鱼药", R.mipmap.ic_add, R.drawable.shape_addzhongzi, 3, MenuAdapter.ONE, 300106))
                data.add(MenuBean("养殖记录", R.mipmap.ic_nongshi, 0, 4, MenuAdapter.TOW, 0))
                data.add(MenuBean("投喂记录", R.mipmap.ic_feiliao, 0, 5, MenuAdapter.TOW, 300101))
                data.add(MenuBean("鱼苗记录", R.mipmap.ic_nongyao, 0, 6, MenuAdapter.TOW, 300107))
                data.add(MenuBean("鱼药记录", R.mipmap.ic_zhongzi, 0, 7, MenuAdapter.TOW, 300106))
                if (BuildConfig.FLAVOR == "xuyi") {
                    data.add(MenuBean("包装记录", R.mipmap.ic_pack, 0, 10, MenuAdapter.TOW, 10009))
                    data.add(MenuBean("运输记录", R.mipmap.ic_tansport, 0, 11, MenuAdapter.TOW, 10009))
                }
            }
        }

        mainData.value = data
        return ResultListing(mainData)
    }


    /**
     * 获取用户信息
     */
    fun getUserInfo(): ResultListing<HttpResponse<MainBean>> {
        var api =
            when (traceIndustry) {
                10008 -> {
                    RetrofitClient.getInstance().quearMainDataX()
                        .compose(RxHelper.rxSchedulerHelper())
                }
                10009 -> {
                    RetrofitClient.getInstance().quearMainDataY()
                        .compose(RxHelper.rxSchedulerHelper())
                }
                else -> {
                    RetrofitClient.getInstance().quearMainDataN()
                        .compose(RxHelper.rxSchedulerHelper())
                }
            }
        api.subscribe(
            Consumer {
                mainBean.postValue(it)
            },
            Consumer {
                mainBean.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
            })
        return ResultListing(mainBean)
    }

    /**
     * 申请相关权限
     */
    fun applypermission(activity: Activity, packageManager: PackageManager, packageName: String) {
        //定位
        val location = packageManager.checkPermission(
            Manifest.permission.ACCESS_FINE_LOCATION,
            packageName
        ) == PackageManager.PERMISSION_GRANTED

        if (Build.VERSION.SDK_INT >= 23 && (!location)) {
            requestPermissions(
                activity,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.WRITE_SETTINGS,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_SETTINGS,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_CONTACTS
                ),
                MainActivity.REQUEST_PERMISSION
            )
        }
    }

    /**
     * 上传图片识别
     */
    fun scanIMGUplaod(file: File?): ResultListing<HttpResponse<ScanEntity>> {
        var map = mutableMapOf<String, RequestBody>()
        val requestFile = RequestBody.create(MediaType.parse(""), File(file!!.path))
        map["images\"; filename=\"\" ${File(file.path)}"] = requestFile
        RetrofitClient.getInstance().scanIMG(map).compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                uploadIMG.value = it
            }, Consumer {
                uploadIMG.value = HttpResponse(it.message, ResultStatus.FAILURE)
            })

        return ResultListing(uploadIMG)
    }
}