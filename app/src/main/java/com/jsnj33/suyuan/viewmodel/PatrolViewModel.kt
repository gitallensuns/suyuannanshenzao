package com.jsnj33.suyuan.viewmodel

import androidx.core.net.toFile
import androidx.lifecycle.MutableLiveData
import com.jsnj33.suyuan.base.BaseViewModel
import com.jsnj33.suyuan.http.RxHelper
import com.jsnj33.suyuan.repository.client.ResultStatus
import com.jsnj33.suyuan.repository.client.RetrofitClient
import com.jsnj33.suyuan.repository.model.*
import com.jsnj33.suyuan.view.adapter.UploadIMGAdapter
import io.reactivex.functions.Consumer
import okhttp3.MediaType
import okhttp3.RequestBody

/**
 * @Author 萤火虫
 * @Date 2022/6/21
 * @Desc
 */
class PatrolViewModel : BaseViewModel() {
    val patrolList = MutableLiveData<HttpResponseList<PatrolBean>>()
    val addPatrol = MutableLiveData<HttpResponse<Any>>()
    val reportList = MutableLiveData<HttpResponse<ArrayList<ReportBean>>>()
    val massifList = MutableLiveData<HttpResponse<ArrayList<MassifBean>>>()
    val patrolDetail = MutableLiveData<HttpResponse<PatrolBean>>()
    var uploadIMGResult = MutableLiveData<HttpResponse<ArrayList<UploadIMGBean2>>>()

    /**
     * 获取巡田列表
     */
    fun getPatrolList(pageNum: Int, pageSize: Int? = 15): ResultListing<HttpResponseList<PatrolBean>> {
        RetrofitClient.getInstance().getPatrolListAsync(pageNum, pageSize)
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                patrolList.value = it
            }, Consumer {
                patrolList.value = HttpResponseList(code = ResultStatus.FAILURE, message = "${it.message}", data = Data(0, 0, ArrayList(), false, 0, 0, false), status = "2000")
            })
        return ResultListing(patrolList)
    }

    /**
     * 新增巡田任务
     */
    fun addPatrol(patrolParams: AddPatrolParams): ResultListing<HttpResponse<Any>> {
        RetrofitClient.getInstance().addPatrolAsync(patrolParams)
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                addPatrol.value = it
            }, Consumer {
                addPatrol.value = HttpResponse(code = ResultStatus.FAILURE, message = "${it.message}")
            })
        return ResultListing(addPatrol)
    }

    /**
     * 上报信息列表
     */
    fun getPatrolUpload(): ResultListing<HttpResponse<ArrayList<ReportBean>>> {
        RetrofitClient.getInstance().getPatrolReportAsync()
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                reportList.value = it
            }, Consumer {
                reportList.value = HttpResponse(code = ResultStatus.FAILURE, message = "${it.message}")
            })
        return ResultListing(reportList)
    }

    /**
     * 地块列表
     */
    fun getPatrolMassif(): ResultListing<HttpResponse<ArrayList<MassifBean>>> {
        RetrofitClient.getInstance().getPatrolmassifAsync()
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                massifList.value = it
            }, Consumer {
                massifList.value = HttpResponse(code = ResultStatus.FAILURE, message = "${it.message}")
            })
        return ResultListing(massifList)
    }

    /**
     * 巡田详情
     */
    fun getPatrolDetail(id: String): ResultListing<HttpResponse<PatrolBean>> {
        RetrofitClient.getInstance().getPatrolDetailAsync(id)
            .compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                patrolDetail.value = it
            }, Consumer {
                patrolDetail.value = HttpResponse(code = ResultStatus.FAILURE, message = "${it.message}")
            })
        return ResultListing(patrolDetail)
    }

    /**
     * 上传图片
     */
    fun uploadIMG(dataIMG: ArrayList<UploadIMGBean>): ResultListing<HttpResponse<ArrayList<UploadIMGBean2>>> {
        var map = mutableMapOf<String, RequestBody>()
        dataIMG?.forEach {
            if (it.type == UploadIMGAdapter.PICTAG) {
                val requestFile = RequestBody.create(MediaType.parse(""), it.file!!.toFile())
                map["files\"; filename=\"\" ${it.file.toFile()}"] = requestFile
            }
        }
        RetrofitClient.getInstance().uploadIMG2(map).compose(RxHelper.rxSchedulerHelper())
            .subscribe(Consumer {
                if (it.code == ResultStatus.SUCCESS) {
                    uploadIMGResult.postValue(it)
                } else {
                    uploadIMGResult.postValue(HttpResponse(it.msg, ResultStatus.FAILURE))
                }
            }, Consumer {
                uploadIMGResult.postValue(HttpResponse(it.message, ResultStatus.FAILURE))
            })
        return ResultListing(uploadIMGResult)
    }
}