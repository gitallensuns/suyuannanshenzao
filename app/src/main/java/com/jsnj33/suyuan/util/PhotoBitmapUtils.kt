package com.jsnj33.suyuan.util

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import kotlinx.android.synthetic.main.activity_uploadimg.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object PhotoBitmapUtils {

    /**
     * 把原图按1/10的比例压缩
     *
     * @param path 原图的路径
     * @return 压缩后的图片
     */
    fun getCompressPhoto(path: String): Bitmap? {
        var options: BitmapFactory.Options? = BitmapFactory.Options();
        options?.inJustDecodeBounds = false;
        options?.inSampleSize = 10;  // 图片的大小设置为原来的十分之一
        var bmp = BitmapFactory.decodeFile(path, options);
//        options = null;
        return bmp;
    }

}