package com.jsnj33.suyuan.util

import android.widget.Toast
import com.jsnj33.suyuan.App
import java.util.*

object ToastUtil {
    var toast: Toast? = null;
    var time: Long = 0;
    fun show(message: String, duration: Int = Toast.LENGTH_LONG): Unit {
        if (null == toast) {
            toast = Toast.makeText(App.intance, message, duration);
        } else {
            toast!!.duration = duration
            toast!!.setText(message)
        }
        if (Calendar.getInstance().timeInMillis - time > 2000)
            toast!!.show();
        time = Calendar.getInstance().timeInMillis;

    }
}