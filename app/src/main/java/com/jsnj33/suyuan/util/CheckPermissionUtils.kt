package com.jsnj33.suyuan.util

import android.Manifest
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import android.content.Context
import java.util.ArrayList


object CheckPermissionUtils {

    //需要申请的权限
     val permissions = arrayOf<String>(
        Manifest.permission.CAMERA
    )

    //检测权限
    fun checkPermission(context: Context): Array<String> {
        val data = ArrayList<String>()//存储未申请的权限
        for (permission in permissions) {
            val checkSelfPermission = ContextCompat.checkSelfPermission(context, permission)
            if (checkSelfPermission == PackageManager.PERMISSION_DENIED) {//未申请
                data.add(permission)
            }
        }
        return data.toTypedArray()
    }
}