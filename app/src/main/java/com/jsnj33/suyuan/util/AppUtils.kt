package com.jsnj33.suyuan.util

import android.Manifest
import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.telephony.TelephonyManager
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.app.ActivityCompat
import com.jsnj33.suyuan.App
import java.io.File

/**
 *
 * App工具类
 */
object AppUtils {

    /**
     * 获取上下文对象
     *
     * @return 上下文对象
     */
  val context: Context
        get() = App.intance;



    /**
     * 获取版本名称
     */
    private fun getAppVersionName(context: Context): String? {
        var versionName: String? = null
        try {
            // ---get the package info---
            val pm = context.packageManager
            val pi = pm.getPackageInfo(context.packageName, 0)
            versionName = pi.versionName
            if (versionName == null || versionName.length <= 0) {
                return ""
            }
        } catch (e: Exception) {
            Log.e("VersionInfo", "Exception", e)
        }

        return versionName
    }

    /**
     * 显示软键盘
     */
    private fun openSoftInput(et: EditText) {
        val inputMethodManager = et.context
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(et, InputMethodManager.HIDE_NOT_ALWAYS)
    }

}
