package com.jsnj33.suyuan.util

import android.text.TextUtils
import com.jsnj33.suyuan.R
import com.jsnj33.suyuan.repository.model.Data
import com.jsnj33.suyuan.repository.model.DataListBean
import com.jsnj33.suyuan.repository.model.FramingBean
import kotlinx.android.synthetic.main.toolbar_header.*
import java.util.regex.Pattern

/**
 * 字符串工具类
 */
object StringUtils {
    /**
     * 判断字符串是否有值，如果为null或者是空字符串或者只有空格或者为"null"字符串，则返回true，否则则返回false
     */
    fun isEmpty(value: String?): Boolean {
        return !(value != null && !"".equals(value.trim { it <= ' ' }, ignoreCase = true)
                && !"null".equals(value.trim { it <= ' ' }, ignoreCase = true))
    }

    fun setTitle(traceIndustry: Int): String {
        return when (traceIndustry) {
            10008 -> "溯源系统-畜禽业"
            10009 -> "溯源系统-渔业"
            else -> "溯源系统-农业"
        }
    }
    fun setTitle2(traceIndustry: Int): String {
        return when (traceIndustry) {
            10008 -> "添加养殖管理"
            10009 -> "添加养殖管理"
            else -> "添加农事管理"
        }
    }

    /**
     * 作业类型
     * @param type
     */
    fun farmingTypeValue(traceIndustry: Int, type: Int): String {
        with(traceIndustry)
        {
            return if (10008 == this) {
                when (type) {
                    1020101 -> "投喂"
                    1020102 -> "清洁"
                    1020103 -> "防疫"
                    1020104 -> "消毒"
                    1020105 -> "投药"
                    1020106 -> "投幼崽"
                    else -> "其他"
                }
            } else if (10009 == this) {
//                投喂:300101 增氧:300102 加水:300103 清淤:300104 疫苗:300105 渔药:300106 鱼苗:300107  其他:300108
                when (type) {
                    300101 -> "投喂"
                    300102 -> "增氧"
                    300103 -> "加水"
                    300104 -> "清淤"
                    300105 -> "疫苗"
                    300106 -> "鱼药"
                    300107 -> "鱼苗"
                    300108 -> "其他"
                    else -> "其他"
                }
            } else {
                when (type) {
                    100101 -> "播种"
                    100102 -> "灌溉"
                    100103 -> "施肥"
                    100104 -> "用药"
                    100105 -> "除草"
                    100106 -> "插秧"
                    100107 -> "翻地"
                    else -> "其他"
                }
            }
        }
    }

    /**
     * 作业类型
     * @param type
     */
//    fun farmingTypeSub(traceIndustry: Int, farmingCod: Int): String {
//        return when (farmingCod) {
//            100104 -> "农药投入详情"
//            100103 -> "施肥投入详情"
//            100101 -> "种子投入详情"
//            1020103 -> "疫苗投入详情"
//            1020105 -> "兽药投入详情"
//            1020106 -> "幼崽投入记录"
//            10100 -> "疫苗投入记录"
//            10105 -> "鱼苗投入记录"
//            10104 -> "鱼药投入记录"
//            else -> {
//                "作业类型"
//            }
//        }
//    }

    /**
     * 单位映射
     */
    fun getInputUnitValue(unit: Int): String {
        when (unit) {
            10030 ->
                return "克(g)"
            10031 ->
                return "千克(kg)"
            10032 ->
                return "毫升(mL)"
            10033 ->
                return "升(L)"
            10034 ->
                return "吨"
            10035 ->
                return "其他"
            10036 ->
                return "只"
            10037 ->
                return "头"
            10038 ->
                return "支"
            10039 ->
                return "袋"
            else ->
                return "未知"
        }
    }

    /**
     * 投入品单位
     */
    fun listCompany(): ArrayList<DataListBean> {
        /**
         * traceIndustry 所属行业(10007种植业；10008畜牧业；10009渔业；10010其他)
         */
        var traceIndustry: Int by PreferencesUtil("traceIndustry", 10007)

        var list = arrayListOf<DataListBean>()
        when (traceIndustry) {
            10007 -> {
                list.add(DataListBean(10030, "克(g)"))
                list.add(DataListBean(10031, "千克(kg)"))
                list.add(DataListBean(10032, "毫升(mL)"))
                list.add(DataListBean(10033, "升(L)"))
                list.add(DataListBean(10034, "吨"))
                list.add(DataListBean(10035, "其他"))
            }
            10008 -> {
                list.add(DataListBean(10030, "克(g)"))
                list.add(DataListBean(10031, "千克(kg)"))
                list.add(DataListBean(10032, "毫升(mL)"))
                list.add(DataListBean(10033, "升(L)"))
                list.add(DataListBean(10034, "吨"))
                list.add(DataListBean(10036, "只"))
                list.add(DataListBean(10037, "头"))
                list.add(DataListBean(10038, "支"))
                list.add(DataListBean(10039, "袋"))
                list.add(DataListBean(10035, "其他"))
            }
            else -> {
                list.add(DataListBean(10030, "克(g)"))
                list.add(DataListBean(10031, "千克(kg)"))
                list.add(DataListBean(10032, "毫升(mL)"))
                list.add(DataListBean(10033, "升(L)"))
                list.add(DataListBean(10034, "吨"))
                list.add(DataListBean(10038, "支"))
                list.add(DataListBean(10039, "袋"))
                list.add(DataListBean(10040, "尾"))
                list.add(DataListBean(10035, "其他"))
            }
        }

        return list
    }

    /**
     * 通过code查询Index
     */
    fun quearPosition(code: Int): Int {
        listCompany().forEachIndexed { index, data ->
            if (code == data.inputUnit) {
                return index
            }
        }
        return 0
    }

    /**
     * 仿治对象
     * @param type
     */
    fun pestControlTargets(type: Int): String {
        when (type) {
            10040 ->
                return "杀虫剂"
            10041 ->
                return "杀菌剂"
            10042 ->
                return "杀螨剂"
            10043 ->
                return "杀线虫剂"
            10044 ->
                return "杀鼠剂"
            10045 ->
                return "除草剂"
            10046 ->
                return "脱叶剂"
            10047 ->
                return "植物生成调节剂"
            else ->
                return "其他"
        }
    }

    fun getRecordName(traceIndustry: Int, type: Int): String {
        return when (traceIndustry) {
            10008 -> {
                when (type) {
                    1 -> "饲料名称"
                    2 -> "兽药名称"
                    3 -> "养殖品种"
                    else -> "作业类型"
                }
            }
            10009 -> {
                when (type) {
                    1 -> "饲料名称"
                    2 -> "鱼苗名称"
                    3 -> "鱼药名称"
                    else -> "作业类型"
                }
            }
            else -> {
                when (type) {
                    1 -> "肥料名称"
                    2 -> "农药名称"
                    3 -> "种子品种"
                    else -> "农事类型"
                }
            }
        }
    }

    fun getRecordDoValue(traceIndustry: Int, type: Int): String {
        return when (traceIndustry) {
            10008 -> {
                when (type) {
                    1 -> "使用量"
                    2 -> "使用量"
                    3 -> "养殖数量"
                    else -> "使用量"
                }
            }
            10009 -> {
                when (type) {
                    1 -> "使用量"
                    2 -> "使用量"
                    3 -> "使用量"
                    else -> "使用量"
                }
            }
            else -> {
                when (type) {
                    1 -> "使用量"
                    2 -> "使用量"
                    3 -> "使用量"
                    else -> "使用量"
                }
            }
        }
    }

    fun getRecordTime(traceIndustry: Int, type: Int): String {
        return when (traceIndustry) {
            10008 -> {
                when (type) {
                    1 -> "使用时间"
                    2 -> "使用时间"
                    3 -> "投入时间"
                    else -> "使用时间"
                }
            }
            10009 -> {
                when (type) {
                    1 -> "使用时间"
                    2 -> "使用时间"
                    3 -> "使用时间"
                    else -> "使用时间"
                }
            }
            else -> {
                when (type) {
                    1 -> "使用时间"
                    2 -> "使用时间"
                    3 -> "使用时间"
                    else -> "使用时间"
                }
            }
        }
    }

    fun framingCode(traceIndustry: Int, type: Int): Int {
        return when (traceIndustry) {
            10008 -> {
                when (type) {
                    0 -> 1020103
                    1 -> 1020105
                    2 -> 1020106
                    else -> 1020103
                }
            }
            10009 -> {
                when (type) {
                    0 -> 10100
                    1 -> 10105
                    2 -> 10104
                    else -> 10100
                }
            }
            else -> {
                when (type) {
                    0 -> 100104
                    1 -> 100103
                    2 -> 100101
                    else -> 100104
                }
            }
        }
    }

    /**
     * 添加疫苗的title
     */
    fun getAddTitle(farmingCode: Int): String {
        return when (farmingCode) {
            100104 -> "添加农药"
            100103 -> "添加施肥"
            100101 -> "添加种子"
            1020103 -> "添加疫苗防疫"
            1020105 -> "添加投放药品"
            1020106 -> "添加投入幼崽"
            10100 -> "添加疫苗"
            10105 -> "添加鱼苗"
            10104 -> "添加鱼药"
            else -> "选择生产档案"
        }
    }

    /**
     * 添加疫苗的title
     */
    fun getRecordTitle(traceIndustry: Int, farmingCode: Int): String {
//        10007种植业；10008畜牧业；10009渔业；10010其他)
        LogUtil.e("record", "traceIndustry=${traceIndustry}farmingCode=${farmingCode}")
        return when (farmingCode) {
            100104 -> "农药投入记录"
            100103 -> "施肥投入记录"
            100101 -> "种子投入记录"
            1020103 -> "疫苗投入记录"
            1020105 -> "兽药投入记录"
            1020106 -> "幼崽投入记录"
            1020101 -> "投喂记录"
            300101 -> "投喂记录"
            300107 -> "鱼苗投入记录"
            300106 -> "鱼药投入记录"
            else -> {
                when (traceIndustry) {
                    10007 -> "农事生产记录"
                    10008 -> "养殖记录"
                    10009 -> "养殖记录"
                    else -> "农事生产记录"
                }
            }
        }
    }

    /**
     * 添加疫苗的title
     */
    fun getRecordDetailTitle(traceIndustry: Int, farmingCode: Int): String {
//        10007种植业；10008畜牧业；10009渔业；10010其他)
        return when (farmingCode) {
            100104 -> "农药投入详情"
            100103 -> "施肥投入详情"
            100101 -> "种子投入详情"
            1020103 -> "疫苗投入详情"
            1020101 -> "投喂记录详情"
            1020105 -> "兽药投入详情"
            1020106 -> "幼崽投入记录"
            300101 -> "投喂记录详情"
            300107 -> "鱼苗投入详情"
            300106 -> "鱼药投入详情"
            else -> {
                when (traceIndustry) {
                    10007 -> "农事生产详情"
                    10008 -> "养殖管理详情"
                    10009 -> "渔业养殖详情"
                    else -> "农事生产详情"
                }
            }
        }
    }
}
