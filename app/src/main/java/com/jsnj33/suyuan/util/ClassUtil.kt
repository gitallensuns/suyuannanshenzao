package com.jsnj33.suyuan.util

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.jsnj33.suyuan.base.NoViewModel
import java.lang.reflect.ParameterizedType


object ClassUtil {

    /**
     * 获取泛型ViewModel的class对象
     */
    fun <T> getViewModel(obj: Any): Class<T>? {
        val currentClass = obj.javaClass
        val tClass = getGenericClass<T>(currentClass, ViewModel::class.java)
        return if (tClass == null || tClass == AndroidViewModel::class.java || tClass == NoViewModel::class.java) {
            null
        } else tClass
    }

    private fun <T> getGenericClass(klass: Class<*>?, filterClass: Class<*>): Class<T>? {
        val type = klass!!.genericSuperclass
        if (type !is ParameterizedType) return null
        val types = type.actualTypeArguments
            for (t in types) {
                @Suppress("UNCHECKED_CAST")
                val tClass = t as Class<T>
                if (filterClass.isAssignableFrom(tClass)) {
                    return tClass
                }
            }
        return null
    }

    public inline fun <reified T> Any.toType(): T? {
        return if (this is T) {
            this
        } else {
            null
        }
    }

}
