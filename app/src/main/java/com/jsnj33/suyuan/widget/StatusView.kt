package com.sunwei.statusview.widget

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.jsnj33.suyuan.R
import kotlinx.android.synthetic.main.status_view.view.*

/**
 * Android 视图容器切换容器
 */
class StatusView : LinearLayout, CallbackLister {


    constructor(content: Context) : super(content) {
        initView(null, 0)
    }

    constructor(context: Context, attributes: AttributeSet) : super(context, attributes) {
        initView(attributes, 0)
    }

    constructor(context: Context, attributes: AttributeSet? = null, defStyle: Int = 0) : super(
        context,
        attributes,
        defStyle
    ) {
        initView(attributes, defStyle)
    }

    /**
     * Index=
     * 0：无数据时 的ICON
     * 1：加载错误的ICON
     */
    private val mIcons = intArrayOf(0, 0)
    /**
     *文字设置Index
     * 0表示 加载中
     * 1：空数据的文案
     * 2：错误异常的文案
     */
    private val mTexts = intArrayOf(0, 0, 0)

    /**
     * 加载圈颜色
     */
    private val mColor = intArrayOf(1)
    //布局容器
    private var mRootView: View? = null

    /**
     * 初始化数据
     */
    private fun initView(attrs: AttributeSet?, defStyle: Int): Unit {
        View.inflate(context, R.layout.status_view, this)
        var typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.StatusView, defStyle, 0
        )
        //无数据的Icon
        mIcons[0] =
            typedArray.getInt(
                R.styleable.StatusView_svEmptyIcon,
                R.drawable.ic_empty
            )
        //错误的Icon
        mIcons[1] =
            typedArray.getInt(
                R.styleable.StatusView_svErrorIcon,
                R.drawable.ic_error
            )
        //加载中的文案
        mTexts[0] =
            typedArray.getInt(
                R.styleable.StatusView_svLoadingText,
                R.string.sv_loadingTxt
            )
        //无数据的文案
        mTexts[1] =
            typedArray.getInt(
                R.styleable.StatusView_svEmptyText,
                R.string.sv_EmptyTxt
            )
        //显示错误的文案
        mTexts[2] =
            typedArray.getInt(
                R.styleable.StatusView_svErrorText,
                R.string.sv_errorTxt
            )
//        //显示加载圈颜色
//        mColor[0] = typedArray.getInt(R.styleable.StatusView_svLoadingColor, R.color.colorAccent)
//        mLoadingIndicatorView.setIndicatorColor(mColor[0])
        typedArray.recycle()
    }

    /**
     * 设置容器《当时无数据/加载中/错误时隐藏》
     */
    override fun setRootView(view: View) {
        mRootView = view
        mStatusViewContent?.visibility = View.GONE
        mLoadingIndicatorView.visibility = View.GONE
        mLoadingIndicatorView.hide()
        ivEmpty.visibility = View.GONE
        tvNotData.visibility = View.GONE
    }

    /**
     * 处理成功
     */
    override fun handleSuccess() {
        mRootView?.visibility = View.VISIBLE
        mStatusViewContent?.visibility = View.GONE
        mLoadingIndicatorView.visibility = View.GONE
        mLoadingIndicatorView.hide()
        ivEmpty.visibility = View.GONE
        tvNotData.visibility = View.GONE
    }

    /**
     * 处理失败
     */
    override fun handleFailure(error: String?) {
        mStatusViewContent.visibility = View.VISIBLE
        mRootView?.visibility = View.GONE
        mLoadingIndicatorView.visibility = View.GONE
        mLoadingIndicatorView.smoothToHide()
        mLoadingIndicatorView.hide()
        ivEmpty.visibility = View.VISIBLE
        tvNotData.visibility = View.VISIBLE
        tvNotData.text = error
        ivEmpty.setImageResource(mIcons[0])
    }

    override fun onDestory() {
        handleSuccess()
    }

    /**
     * 处理中
     */
    @SuppressLint("ResourceAsColor")
    override fun handleLoading(isTransparent: Boolean?, txt: String?) {
        /**
         * isTransparent true 表示透明 false  白色
         */
        when (isTransparent) {
            true -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mStatusViewContent.setBackgroundColor(context.getColor(android.R.color.transparent))
                } else {
                    mStatusViewContent.setBackgroundColor(android.R.color.transparent)
                }
            }
            false -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mStatusViewContent.setBackgroundColor(context.getColor(android.R.color.white))
                } else {
                    mStatusViewContent.setBackgroundColor(android.R.color.white)
                }
            }

            else -> {}
        }
        mStatusViewContent.visibility = View.VISIBLE
        mRootView?.visibility = View.GONE
        mLoadingIndicatorView.visibility = View.VISIBLE
        mLoadingIndicatorView.smoothToShow()
        ivEmpty.visibility = View.GONE
        tvNotData.visibility = View.VISIBLE
        tvNotData.text = txt
    }

    /**
     * 处理Null数据
     */
    override fun handleEmpty(txt: String?) {
        mStatusViewContent.visibility = View.VISIBLE
        mRootView?.visibility = View.GONE
        mLoadingIndicatorView.visibility = View.GONE
        mLoadingIndicatorView.hide()
        ivEmpty.visibility = View.VISIBLE
        tvNotData.visibility = View.VISIBLE
        tvNotData.text = txt
        ivEmpty.setImageResource(mIcons[1])
    }
}


/**
 * 行为控制接口类
 */
interface CallbackLister {
    /**
     * 设置布局容器《当时无数据/加载中/错误时隐藏》
     */
    fun setRootView(view: View)

    fun onDestory()
    /**
     * 处理成功
     */
    fun handleSuccess()

    /**
     * 处理加载中
     * @param isTransparent ture 表示背景透明，否则背景不透明
     */
    fun handleLoading(isTransparent: Boolean? = false, txt: String? = "加载中~")

    /**
     * 处理空数据
     */
    fun handleEmpty(txt: String? = "暂无更多数据")

    /**
     * 处理失败
     */
    fun handleFailure(error: String? = "内容加载失败，请检查网络设置")
}

