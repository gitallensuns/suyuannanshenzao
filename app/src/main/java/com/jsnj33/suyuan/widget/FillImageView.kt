package com.jsnj33.suyuan.widget

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class FillImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    init {
        inits();
    }

    var mMatrix: Matrix? = null
    var paint: Paint? = null
    var srcR: Rect? = null
    var dstR: RectF? = null
    var deviceR: RectF? = null

    fun inits(): Unit {
        mMatrix = Matrix();
        paint = Paint();
        srcR = Rect();
        dstR = RectF();
        deviceR = RectF();
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        //TODO 还原matrix中的缩放比例
        matrix.reset();
        var drawable = drawable;
        if (drawable == null) {
            return;
        }
        var w = width
        var h = height;
        if (w == 0 || h == 0) {
            return;
        }
        if (!(drawable is BitmapDrawable)) {
            return;
        }
        var b = (drawable).getBitmap();
        if (null == b) {
            return;
        }
        // 获得图片的宽高
        var width = b.width;
        var height = b.height;
        // 计算缩放比例
        var scaleWidth = (w * 1f) / (width * 1f);
        var scaleHeight = (h * 1f) / (height * 1f);
        // 取得想要缩放的matrix参数
        matrix.postScale(scaleWidth, scaleHeight);

        var transformed = !matrix.rectStaysRect();
        if (transformed) {
            paint!!.setAntiAlias(true);
        }
        srcR!!.set(0, 0, width, height);
        dstR!!.set(0.toFloat(), 0.toFloat(), width.toFloat(), height.toFloat());
        canvas!!.concat(matrix);
        paint!!.setFilterBitmap(true);
        canvas.drawBitmap(b, srcR, dstR!!, paint);
    }
}