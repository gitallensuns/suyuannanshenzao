package com.jsnj33.suyuan.http;

import com.jsnj33.suyuan.http.intercepter.LoggerIntercepter;
import com.jsnj33.suyuan.http.intercepter.LoggingLevel;
import com.jsnj33.suyuan.http.intercepter.NetworkIntercepter;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitCreateHelper {
    private static final int TIMEOUT_READ = 5;
    private static final int TIMEOUT_CONNECTION = 5;
    private static final LoggerIntercepter interceptor = new LoggerIntercepter(LoggingLevel.ALL);
    private static final NetworkIntercepter networkIntercepter = new NetworkIntercepter();
    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            //打印日志
            .addInterceptor(networkIntercepter)
            .addInterceptor(interceptor)
            .connectTimeout(TIMEOUT_CONNECTION, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT_READ, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT_READ, TimeUnit.SECONDS)
            //失败重连
            .retryOnConnectionFailure(true)
            .build();


    public static <T> T createApi(Class<T> clazz, String url) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(clazz);
    }
}

