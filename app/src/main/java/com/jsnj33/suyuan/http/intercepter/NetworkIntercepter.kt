package com.jsnj33.suyuan.http.intercepter

import android.util.Log
import com.jsnj33.suyuan.util.LogUtil
import com.jsnj33.suyuan.util.PreferencesUtil
import com.jsnj33.suyuan.view.login.LoginActivity.Companion.TAG
import net.iharder.Base64

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * 网络拦截器
 */
class NetworkIntercepter : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var mobileToken: String by PreferencesUtil("mobileToken", "wuzhi")
        var token: String by PreferencesUtil("token", "token")
        LogUtil.e(TAG, mobileToken)
        val request = chain.request().newBuilder()
            .addHeader("Content-Type", "application/json;charset=utf-8")
            .addHeader("mobileToken", mobileToken)
            .addHeader("Authorization", token)
            .addHeader("token", token)
            .build()
        return chain.proceed(request)
    }
}
