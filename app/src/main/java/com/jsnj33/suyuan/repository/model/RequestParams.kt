package com.jsnj33.suyuan.repository.model

/**
 * 请求体
 * @param inputCategory:10026农药；10027肥料；10028种苗
 */
data class RequestPestParams(var inputCategory: Int, var status: String? = "10018")
data class RequestWHParams(var plantType: String)

data class RequestParams(var status: String? = "10018", var personCharge: String? = null)

data class RequestProductionParams(
    var archiId: String? = null,
    var archiNumber: String? = null,
    var enterNumber: String? = null,
    var pic: String? = null,
    var principal: String? = null,
    var processDate: String? = null,
    var processRemark: String? = null,
    var productName: String? = null
)

data class RequestProductionAddressBean(var archiId: String)

/**
 * 登录参数
 */
data class RequestLoginParams(var loginName: String, var password: String, var mobileToken: String)

/**
 *农业
 */
data class RequestFramingParams(
    val taskPic: String,
    val farmingCode: String,
    val archiId: String,
    val product: String? = "",
    val massifName: String? = "",
    val massifId: String? = "",
    val phenologicalPhaseId: String? = "",
    val phenologicalPhaseName: String? = ""
)

/**
 *畜牧业
 */
data class RequestXMParams(
    val taskPic: String,
    val breedCode: String,
    val archiId: String,
    val product: String? = "",
    val fieldName: String? = ""
)

/**
 *渔业
 */
data class RequestYYParams(
    val taskPic: String,
    val fishbreedCode: String,
    val archiId: String,
    val product: String? = "",
    val tangkouName: String? = ""
)

/**
 * 农事记录
 */
data class RequestFramingRecordParams(
    var sort: Int? = 0,//传1 代表农事生产(其余则不传)
    var personId: Long? = null
)

data class RequestOtherRecordParams(
    var farmingCode: Int? = 0//传1 代表农事生产(其余则不传)
)

data class RequestOtherRecordParamsX(
    var personId: Long,
    var breedCode: Int? = 0//传1 代表农养殖管理(其余则不传)
)

data class RequestOtherRecordParamsYY(
    var personId: Long,
    var fishbreedCode: Int? = 0//传1 代表渔业养殖(其余则不传)
)

data class RequestPackParams(var processStatus: Int, var productName: String? = null)


//archiId: "27103519904104448"
//archiNumber: null
//level: "1"
//material: "100"
//mode: 100901
//num: "100"
//packDate: "2021-10-13 00:00:00"
//productName: "鸡肉"
//slaughterArchiNumber: 200001211
//specs: "100"
data class RequstAddPackParams(
    var archiId: String? = null,
    var archiNumber: String? = null,
    var productName: String? = null,
    var packDate: String? = null,
    var level: String? = null,
    var material: String? = null,
    var mode: String? = null,
    var num: String? = null,
    var slaughterArchiNumber: String? = null,
    var specs: String? = null,
)

data class RequstAddTransportParams(
    var archiId: String? = null,
    var archiNumber: String? = null,
    var productName: String? = null,
    var arriveDate: String? = null,
    var charge: String? = null,
    var destination: String? = null,
    var transDate: String? = null,
    var vehicle: String? = null,
    var slaughterArchiNumber: String? = null,
)

data class AddPatrolParams(
    var createBy: String? = null,
    var fieldFarm: String? = null,
    var fieldPic: String? = null,
    var remarks: String? = null,
    var fieldPerson: String? = null,
    var fieldPlotId: String? = null,
    var fieldTime: String? = null,
    var id: String? = null,
    var messageReportId: String? = null,
    var updateBy: String? = null
)