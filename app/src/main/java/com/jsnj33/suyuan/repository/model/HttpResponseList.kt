package com.jsnj33.suyuan.repository.model

//所有列表接口最外层封装类
data class HttpResponseList<T>(
    val code: Int,
    val message: String,
    val `data`: Data<T>,
    val status: String? = null,
    val date: Long? = null
)

data class Data<T>(
    val current: Int,
    val pages: Int,
    val records: ArrayList<T>,
    val searchCount: Boolean,
    val size: Int,
    val total: Int,
    var isNextPage: Boolean = total < (current * size)
)

