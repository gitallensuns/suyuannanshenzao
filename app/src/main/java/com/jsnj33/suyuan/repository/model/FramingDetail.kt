package com.jsnj33.suyuan.repository.model

import android.os.Parcel
import android.os.Parcelable

data class FramingDetail(
    val archiId: Long,
    val createBy: Int,
    val createDate: String?,
    val enterNumber: String?,
    val farmingCode: Int,
    val filePath: List<String>? = null,
    val flag: Int,
    val id: String?,
    val massifArea: String?,
    val massifId: String?,
    val massifName: String?,
    val massifUnit: String?,
    val personId: String?,
    val personName: String?,
    val product: String?,
    val remarks: String?,
    val sort: String?,
    val status: String?,
    val taskDate: String?,
    val taskPic: String?,
    val taskPicFile: String?,
    val tenantId: String?,
    val traceProductionInputList: List<traceProductionInput>,
    val updateBy: Int,
    val updateDate: String?,
    val uuid: String?,
    val version: Int
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readLong(),
        source.readInt(),
        source.readString(),
        source.readString(),
        source.readInt(),
        ArrayList<String>().apply {
            source.readList(
                this as List<*>,
                traceProductionInput::class.java.classLoader
            )
        },
        source.readInt(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        ArrayList<traceProductionInput>().apply {
            source.readList(
                this as List<*>,
                traceProductionInput::class.java.classLoader
            )
        },
        source.readInt(),
        source.readString(),
        source.readString(),
        source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeLong(archiId)
        writeInt(createBy)
        writeString(createDate)
        writeString(enterNumber)
        writeInt(farmingCode)
        writeList(filePath)
        writeInt(flag)
        writeString(id)
        writeString(massifArea)
        writeString(massifId)
        writeString(massifName)
        writeString(massifUnit)
        writeString(personId)
        writeString(personName)
        writeString(product)
        writeString(remarks)
        writeString(sort)
        writeString(status)
        writeString(taskDate)
        writeString(taskPic)
        writeString(taskPicFile)
        writeString(tenantId)
        writeList(traceProductionInputList)
        writeInt(updateBy)
        writeString(updateDate)
        writeString(uuid)
        writeInt(version)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<FramingDetail> =
            object : Parcelable.Creator<FramingDetail> {
                override fun createFromParcel(source: Parcel): FramingDetail = FramingDetail(source)
                override fun newArray(size: Int): Array<FramingDetail?> = arrayOfNulls(size)
            }
    }
}

data class traceProductionInput(
    val id: String? = "",
    val inputConsum: String? = "",
    val inputUnit: String? = ""
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(inputConsum)
        writeString(inputUnit)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<traceProductionInput> =
            object : Parcelable.Creator<traceProductionInput> {
                override fun createFromParcel(source: Parcel): traceProductionInput =
                    traceProductionInput(source)

                override fun newArray(size: Int): Array<traceProductionInput?> = arrayOfNulls(size)
            }
    }
}