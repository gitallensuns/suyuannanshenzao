package com.jsnj33.suyuan.repository.model

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import com.chad.library.adapter.base.entity.MultiItemEntity
import java.io.File
import java.io.Serializable

class UploadIMGBean(val file: Uri? = null, val type: Int) : MultiItemEntity, Parcelable, Serializable {

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Uri::class.java.classLoader),
        parcel.readInt()
    ) {
    }

    override fun getItemType(): Int {
        return type
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(file, flags)
        parcel.writeInt(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UploadIMGBean> {
        override fun createFromParcel(parcel: Parcel): UploadIMGBean {
            return UploadIMGBean(parcel)
        }

        override fun newArray(size: Int): Array<UploadIMGBean?> {
            return arrayOfNulls(size)
        }
    }

}

data class UploadIMGBean2(
    val createBy: String,
    val createDate: String,
    val createName: Any,
    val fileDesc: Any,
    val fileName: String,
    val filePath: String,
    val fileUrl: Any,
    val id: String,
    val remarks: Any,
    val size: String,
    val sort: Any,
    val status: Any,
    val tenantId: Any,
    val trueName: String,
    val updateBy: String,
    val updateDate: String,
    val uuid: String,
    val version: Int
)