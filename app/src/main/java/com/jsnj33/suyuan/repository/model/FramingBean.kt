package com.jsnj33.suyuan.repository.model

data class FramingBean(
    val name: String? = "",
    val farmingCode: Int = 0,
    val url: Int,
    val type: Int,
    var isChecked: Boolean = false
)

data class PackBean(
    var traceCode: String,
    var productName: String,
    var specs: String,
    var material: String,
    var mode: Int,
    var num: String,
    var packDate: String,
    var slaughterArchiNumber: String,
    var archiNumber: String,
    var level: String,
)

data class TransportBean(
    val archiId: String,
    val archiNumber: String,
    val arriveDate: String,
    val charge: String,
    val createBy: String,
    val createDate: String,
    val destination: String,
    val enterNumber: String,
    val id: String,
    val processStatus: Int,
    val productName: String,
    val remarks: Any,
    val slaughterArchiNumber: Int,
    val sort: Any,
    val status: Int,
    val tenantId: Any,
    val traceCode: Any,
    val transDate: String,
    val updateBy: String,
    val updateDate: String,
    val uuid: String,
    val vehicle: String,
    val version: Int
)