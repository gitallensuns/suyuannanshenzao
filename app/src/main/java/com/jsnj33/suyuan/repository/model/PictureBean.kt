package com.jsnj33.suyuan.repository.model

data class PictureBean(
    val id: String? = null,
    val remarks: Any? = null,
    val sort: Any? = null,
    val version: Int = 0,
    val status: Any? = null,
    val createBy: Int = 0,
    val createDate: String? = null,
    val updateBy: Int = 0,
    val updateDate: String? = null,
    val uuid: String? = null,
    val tenantId: Any? = null,
    val fileName: String? = null,
    val trueName: String? = null,
    val fileDesc: Any? = null,
    val filePath: String? = null,
    val fileUrl: Any? = null,
    val size: String? = null,
    val createName: Any? = null
)