package com.jsnj33.suyuan.repository.model

import com.chad.library.adapter.base.entity.MultiItemEntity

class MenuBean(
    val name: String,
    val url: Int = 0,
    val bg: Int,
    val type: Int = 0,
    val showType: Int,
    val code: Int
) :
    MultiItemEntity {
    override fun getItemType(): Int = showType
}