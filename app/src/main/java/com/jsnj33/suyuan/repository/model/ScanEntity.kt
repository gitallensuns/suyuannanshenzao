package com.jsnj33.suyuan.repository.model

data class ScanEntity(
    val name: String? = null,
    val quality: String? = null,
    val similarity: String? = null,
    val type: String? = null,
    val weight: String? = null,
    val filePath: String? = null
)