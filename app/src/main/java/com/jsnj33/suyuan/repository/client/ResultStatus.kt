package com.jsnj33.suyuan.repository.client

/**
 * 接口请求状态码
 */
class ResultStatus {
    companion object {
        const val SUCCESS = 10000//成功
        const val FAILURE = 20000 //失败
    }
}