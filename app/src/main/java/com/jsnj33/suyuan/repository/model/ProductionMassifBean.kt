package com.jsnj33.suyuan.repository.model


class ProductionAddressBean

/**
 * 获取地块列表
 * 获取栋舍列表
 * 获取鱼塘列表
 */
data class ListBean(
    val archiId: String,
    val createBy: String,
    val createDate: String,
    val enterNumber: String,
    val id: String,
    val massifArea: String,
    val massifId: String,
    val massifName: String?=null,
    val massifUnit: String,
    val remarks: String,
    val sort: String,
    val status: String,
    val tenantId: String,
    val updateBy: String,
    val updateDate: String,
    val uuid: String,
    val version: String,
    val fieldArea: Int,
    val fieldName: String,
    val fieldType: Int,
    val fieldUnit: Int,
    val lati: String,
    val longi: String,
    var showName:String
)