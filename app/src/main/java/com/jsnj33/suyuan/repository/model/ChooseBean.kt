package com.jsnj33.suyuan.repository.model

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.jsnj33.suyuan.view.adapter.ChooseAdapter
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

class ChooseBean(id: Int, type: Int, inputConsum: String?, dataList: List<DataListBean>? = null) :
    MultiItemEntity {
    var id: Int = 0
    var type = ChooseAdapter.TAGITEM
    var dataList: List<DataListBean>? = null
    var position: Int = 0
    var num: String = ""
    var inputConsum: String? = ""
    var productId: String? = ""
    var inputUnit: Int = 0
    var inputSubtype: String = ""

    init {
        this.id = id
        this.type = type
        this.inputConsum = inputConsum
        this.productId = productId
        this.inputUnit = inputUnit
        this.dataList = dataList
    }

    override fun getItemType(): Int {
        return type
    }
}

data class DataListBean(
    val inputUnit: Int,
    val inputName: String?,
    val createBy: String? = null,
    val createDate: String? = null,
    val enterNumber: String? = null,
    val fertiCategory: Int? = 0,
    val id: String? = null,
    val inputCategory: Int? = null,
    val inputSubtype: String? = null,
    val inputComponent: String? = null,
    val inputDescribe: String? = null,
    val inputRegister: String? = null,
    val pestCategory: String? = null,
    val pestControlTargets: String? = null,
    val pestIntervalDays: String? = null,
    val pestLicense: String? = null,
    val pestMethod: String? = null,
    val remarks: String? = null,
    val seedType: String? = null,
    val seedlingLevel: String? = null,
    val sort: String? = null,
    val status: Int? = null,
    val tenantId: String? = null,
    val updateBy: String? = null,
    val updateDate: String? = null,
    val uuid: String? = null,
    val version: Int? = 0

) {

    override fun toString(): String {
        return inputName.toString()
    }
}

/**
 * 选择后的产品列表
 */
@SuppressLint("ParcelCreator")
data class ProductList(
    var id: String? = "",
    var inputConsum: String? = "",
    var inputUnit: Int = 0,
    var inputSubtype: String? = ""
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readInt(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(inputConsum)
        writeInt(inputUnit)
        writeString(inputSubtype)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ProductList> = object : Parcelable.Creator<ProductList> {
            override fun createFromParcel(source: Parcel): ProductList = ProductList(source)
            override fun newArray(size: Int): Array<ProductList?> = arrayOfNulls(size)
        }
    }
}


data class WHBean(
    val createBy: Any,
    val createDate: Any,
    val cultivationMeasure: Any,
    val id: String,
    val leafNum: String,
    val phenologicalPhaseName: String,
    val plantType: String,
    val remarks: Any,
    val sort: Any,
    val status: Int,
    val tenantId: Any,
    val updateBy: Any,
    val updateDate: Any,
    val uuid: Any,
    val version: Int
) {
    override fun toString(): String {
        return phenologicalPhaseName
    }
}

data class XMBean(
    val createBy: String,
    val createDate: String,
    val enterNumber: Any,
    val farmName: String,
    val id: String,
    val plantCrop: String,
    val plantSeasonName: String,
    val plantSeasonTime: String,
    val remarks: Any,
    val sort: Any,
    val status: Int,
    val tenantId: Any,
    val updateBy: String,
    val updateDate: String,
    val uuid: String,
    val version: Int,
    val year: String
)