package com.jsnj33.suyuan.repository.model

import androidx.lifecycle.MutableLiveData


data class ResultListing<T>(
    val data: MutableLiveData<T>? = null,
    val dataList: MutableLiveData<HttpResponse<T>>? = null,
    val dataListObj: MutableLiveData<HttpResponseList<T>>? = null,
    val loadStatus: MutableLiveData<Resource<String>>? = null
)
