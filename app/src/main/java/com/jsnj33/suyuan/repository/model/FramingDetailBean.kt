package com.jsnj33.suyuan.repository.model

import java.io.Serializable

data class FramingDetailBean(
    val enterName: String?,
    val productName: String?,
    val archiId: String?,
    val createBy: String? = "",
    val createDate: String?,
    val enterNumber: String?,
    val farmingCode: Int,
    val breedCode: Int,
    val fishbreedCode: Int,
    val filePath: List<String>? = null,
    val path: String?,
    val id: Long,
    val massifArea: Int,
    val massifId: String,
    val massifName: String?,
    val massifUnit: Int,
    val personId: Long,
    val personName: String?,
    val remarks: String?,
    val sort: String?,
    val status: Int,
    val taskDate: String?,
    val taskPic: String?,
    val tenantId: String?,
    val traceProductionInputList: List<TraceProductionBean>? = null,
    val traceProductionFishInputList: List<TraceProductionBean>? = null,
    val traceProductionLivestockInputList: List<TraceProductionBean>? = null,
    val updateBy: Int,
    val updateDate: String?,
    val uuid: String?,
    val version: Int
)

data class TraceProductionBean(
    val enterName: String?,
    val productName: String? = null,
    val archiId: String? = null,
    val pestControlTargets: Int,
    val personName: String? = null,
    val createBy: Int,
    val createDate: String?,
    val enterNumber: String?,
    val id: Long,
    val inputConsum: Int,
    val inputId: Long,
    val inputName: String?,
    val inputUnit: Int,
    val remarks: String?,
    val sort: String?,
    val status: Int,
    val tenantId: String?,
    val updateBy: Int,
    val updateDate: String?,
    val uuid: String?,
    val version: Int,
    var path: String?,
    val farmingId: String?,
    val filePath: List<String>? = null,
    val farmingCode: Int,
    val breedCode: Int,
    val fishbreedCode: Int
)

data class AddProductBean(
    val archiId: String,
    val archiNumber: Int,
    val createBy: String,
    val createDate: String,
    val enterNumber: String,
    val id: String,
    val pic: String?,
    val principal: String,
    val processDate: String,
    val processRemark: String,
    val processStatus: Any,
    val productName: String,
    val remarks: Any,
    val slaughterArchiNumber: Any,
    val sort: Any,
    val status: Int,
    val tenantId: Any,
    val traceCode: Any,
    val updateBy: String,
    val updateDate: String,
    val uuid: String,
    val version: Int
):Serializable