package com.jsnj33.suyuan.repository.model

/**
 *@Author 萤火虫
 *@Date 2022/6/21
 *@Desc
 */
data class PatrolBean(
    val createBy: String,
    val createDate: String,
    val fieldFarm: String,
    val fieldFarmName: String,
    val fieldLocation: String,
    val fieldPerson: String,
    val fieldPic: String,
    val fieldPlotId: String,
    val fieldTime: Any,
    val id: String,
    val messageReportId: String,
    val messageReportName: Any,
    val remarks: Any,
    val sort: String,
    val status: String,
    val tenantId: String,
    val updateBy: String,
    val updateDate: String,
    val uuid: Any,
    val version: String
)


data class MassifBean(
    val createBy: String,
    val createDate: String,
    val enterNumber: String,
    val id: String,
    val lati: String,
    val lonLat: String,
    val longi: String,
    val massifArea: String,
    val massifName: String,
    var isCheck: Boolean? = false,
    val massifUnit: String,
    val personCharge: String,
    val productionAreaId: String,
    val remarks: String,
    val sort: String,
    val status: String,
    val tenantId: String,
    val updateBy: String,
    val updateDate: String,
    val uuid: String,
    val version: String
) {
    override fun toString(): String {
        return massifName
    }
}

data class ReportBean(
    val dictionaryKey: String,
    val id: String
)

