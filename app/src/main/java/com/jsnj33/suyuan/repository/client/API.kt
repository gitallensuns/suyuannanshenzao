package com.jsnj33.suyuan.repository.client

import com.jsnj33.suyuan.http.intercepter.NetworkIntercepter
import com.jsnj33.suyuan.repository.model.*
import com.jsnj33.suyuan.repository.client.RetrofitClient.VERSION
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.*

/**
 * 所有接口层
 */
interface API {
    /**
     * 登录接口
     */
    @POST(VERSION + "Login/check")
    fun login(@Body params: RequestLoginParams): Observable<HttpResponse<LoginBean>>;

    /**
     * 首页用户信息接口(农业)
     */
    @GET(VERSION + "TraceProductionFarming/getAppPrincipal")
//    @Headers
    fun quearMainDataN(): Observable<HttpResponse<MainBean>>

    /**
     * 首页用户信息接口(渔业)
     */
    @GET(VERSION + "TraceProductionFishbreed/getAppPrincipal")
    fun quearMainDataY(): Observable<HttpResponse<MainBean>>

    /**
     * 首页用户信息接口(畜牧)
     */
    @GET(VERSION + "TraceProductionLivestockBreed/getAppLivestockPrincipal")
    fun quearMainDataX(): Observable<HttpResponse<MainBean>>;

    /**
     * 生产档案列表
     */
    @POST(VERSION + "TraceProductionArchi/list")
    fun traceProductionArchi(@Body result: RequestParams): Observable<HttpResponse<List<PesticidesBean>>>

    /**
     * 宰杀包装-生产档案列表
     */
    @POST(VERSION + "TraceSlaughterRecord/list")
    fun traceProduction2Archi(@Body result: RequestParams): Observable<HttpResponse<List<PesticidesBean>>>

    /**
     * 新增生产加工
     */
    @POST(VERSION + "TraceProductProcess/add")
    fun addProduction(@Body result: RequestProductionParams): Observable<HttpResponse<Any>>

    /**
     *生产加工记录
     */
    @POST(VERSION + "TraceProductProcess/getProcess/{pageNum}/{pageSize}")
    fun productionList(
        @Body result: RequestParams, @Path("pageNum") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 1000
    ): Observable<HttpResponseList<AddProductBean>>

    /**
     *物候期
     */
    @POST(VERSION + "TracePhenologicalPhase/list/page/1/1000")
    fun tracePhenologicalPhase(@Body result: RequestWHParams): Observable<HttpResponseList<WHBean>>

    /**
     *获取小麦与水稻
     */
    @GET(VERSION + "TracePlantSeason/{id}")
    fun tracePlantSeason(@Path("id") id: String): Observable<HttpResponse<XMBean>>

    /**
     *投入品列表(农业)
     */
    @POST(VERSION + "TraceInput/list")
    fun traceProductionInput(@Body result: RequestPestParams): Observable<HttpResponse<List<DataListBean>>>

    /**
     * 图像识别
     */
    @POST(VERSION + "Detection/classify")
    @Multipart
    fun scanIMG2(@PartMap files: MutableMap<String, RequestBody>?): Observable<HttpResponse<DataListBean>>

    /**
     *投入品列表(畜牧业)
     */
    @POST(VERSION + "TraceLivestockInput/list")
    fun traceProductionInputX(@Body result: RequestPestParams): Observable<HttpResponse<List<DataListBean>>>

    /**
     *获取地块列表
     */
    @POST(VERSION + "TraceProductionMassif/getProductionMassifList/{page}/{pageSize}")
    fun productionMassifList(
        @Body request: RequestProductionAddressBean,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 1000
    ): Observable<HttpResponseList<ListBean>>

    /**
     *获取地块列表
     */
    @POST(VERSION + "TraceLivestockField/getLivestockFieldlist/page/{page}/{pageSize}")
    fun livestockFieldlist(
        @Body request: RequestProductionAddressBean,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 1000
    ): Observable<HttpResponseList<ListBean>>

    /**
     *获取地块列表
     */
    @POST(VERSION + "TraceTangkou/getTangkouList/{page}/{pageSize}")
    fun tangkouList(
        @Body request: RequestProductionAddressBean,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 1000
    ): Observable<HttpResponseList<ListBean>>

    /**
     *投入品列表(畜牧业)
     */
    @POST(VERSION + "TraceFishbreedInput/list")
    fun traceProductionInputYY(@Body result: RequestPestParams): Observable<HttpResponse<List<DataListBean>>>

    /**
     *新增单个农事管理表详情(农业)
     */
    @POST(VERSION + "TraceProductionFarming/addAppProductFarming")
    fun addAppProductFarmingN(@Body result: RequestFramingParams): Observable<HttpResponse<FramingDetailBean>>

    /**
     *新增单个畜牧管理表详情(畜牧业)
     */
    @POST(VERSION + "TraceProductionLivestockBreed/addAppProductionLivestockBreed")
    fun addAppProductFarmingX(@Body result: RequestXMParams): Observable<HttpResponse<FramingDetailBean>>

    /**
     *新增单个渔业管理表详情(渔业业)
     */
    @POST(VERSION + "TraceProductionFishbreed/addAppProductFishbreed")
    fun addAppProductFarmingY(@Body result: RequestYYParams): Observable<HttpResponse<FramingDetailBean>>

    /***
     * 农事记录列表
     */
    @POST(VERSION + "TraceProductionFarming/getAppProductionFarmingist/{page}/{pageSize}")
    fun getFramingRecordList(
        @Body result: RequestFramingRecordParams,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 15
    ): Observable<HttpResponseList<FramingDetailBean>>

    /***
     * 养殖管理记录（畜牧业）
     */
    @POST(VERSION + "TraceProductionLivestockBreed/getAppProductionLivestockBreedList/{page}/{pageSize}")
    fun getFramingRecordListX(
        @Body result: RequestFramingRecordParams,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 15
    ): Observable<HttpResponseList<FramingDetailBean>>

    /***
     * 养殖管理记录（畜牧业）
     */
    @POST(VERSION + "TraceProductionFishbreed/getAppProductionFishbreedList/{page}/{pageSize}")
    fun getFramingRecordListYY(
        @Body result: RequestFramingRecordParams,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 15
    ): Observable<HttpResponseList<FramingDetailBean>>

    /***
     * 农事记录列表（农业）
     */
    @POST(VERSION + "TraceProductionInput/applist/page/{page}/{pageSize}")
    fun getOtherRecordListN(
        @Body result: RequestOtherRecordParams,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 15
    ): Observable<HttpResponseList<TraceProductionBean>>

    /***
     * 养殖记录列表（畜牧业）
     */
    @POST(VERSION + "TraceProductionLivestockInput/appProductionLivestockInputlist/page/{page}/{pageSize}")
    fun getOtherRecordListX(
        @Body result: RequestOtherRecordParamsX,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 15
    ): Observable<HttpResponseList<TraceProductionBean>>

    /***
     * 疫苗、鱼苗 、鱼药记录（渔业）
     */
    @POST(VERSION + "TraceProductionFishInput/appList/page/{page}/{pageSize}")
    fun getOtherRecordListYY(
        @Body result: RequestOtherRecordParamsYY,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 15
    ): Observable<HttpResponseList<TraceProductionBean>>

    /***
     * 包装列表数据
     */
    @POST(VERSION + "TracePacking/getAppPackingist/{page}/{pageSize}")
    fun getPackRecordList(
        @Body result: RequestPackParams,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 15
    ): Observable<HttpResponseList<PackBean>>

    /**
     * 运输记录数据
     */
    @POST(VERSION + "TraceTransport/getTransportList/{page}/{pageSize}")
    fun getTransportRecordList(
        @Body result: RequestPackParams,
        @Path("page") page: Int? = 1,
        @Path("pageSize") pageSize: Int? = 15
    ): Observable<HttpResponseList<TransportBean>>

    /***
     * 农事记录详情接口调用
     */
    @GET(VERSION + "TraceProductionFarming/getAppProductionFarmingInfoById/{id}")
    fun getFramingRecordDetailById(@Path("id") id: Long): Observable<HttpResponse<FramingDetailBean>>

    /***
     * 养殖管理记录详情接口调用
     */
    @GET(VERSION + "TraceProductionLivestockBreed/getAppProductionLivestockBreedInfoById/{id}")
    fun getFramingRecordDetailByIdX(@Path("id") id: Long): Observable<HttpResponse<FramingDetailBean>>

    /***
     * 渔业管理记录详情接口调用（渔业）
     */
    @GET(VERSION + "TraceProductionFishbreed/getAppProductionFishbreedInfoById/{id}")
    fun getFramingRecordDetailByIdYY(@Path("id") id: Long): Observable<HttpResponse<FramingDetailBean>>

    /***
     * 农事农药、种子、施肥详情接口调用
     */
    @GET(VERSION + "TraceProductionInput/getinput/{id}")
    fun getOtherRecordDetailByIdN(@Path("id") id: Long): Observable<HttpResponse<TraceProductionBean>>

    /***
     * ：app获取生产投入品表详情(畜牧业)
     */
    @GET(VERSION + "TraceProductionLivestockInput/appGetLivestockinput/{id}")
    fun getOtherRecordDetailByIdX(@Path("id") id: Long): Observable<HttpResponse<TraceProductionBean>>

    /***
     * ：app获取生产投入品表详情(渔业)
     */
    @GET(VERSION + "TraceProductionFishInput/getInput/{id}")
    fun getOtherRecordDetailByIdYY(@Path("id") id: Long): Observable<HttpResponse<TraceProductionBean>>

    /***
     * 上传图片
     */
    @POST("sysFile/uploadImgs")
    @Multipart
    fun uploadIMG(@PartMap files: MutableMap<String, RequestBody>?): Observable<HttpResponse<Any>>

    /**
     * 上传图片
     */
    @POST("sysFile/uploadImgs")
    @Multipart
    fun uploadIMG2(@PartMap files: MutableMap<String, RequestBody>?): Observable<HttpResponse<ArrayList<UploadIMGBean2>>>

    /**
     * 图像识别
     */
    @POST("Detection/classify")
    @Multipart
    fun scanIMG(@PartMap files: MutableMap<String, RequestBody>?): Observable<HttpResponse<ScanEntity>>


    /**
     * 新增包装管理
     */
    @POST(VERSION + "TracePacking/add")
    fun addPackAsync(@Body mRequstAddPackParams: RequstAddPackParams): Observable<HttpResponse<Any>>

    /**
     * 新增运输管理
     */
    @POST(VERSION + "TraceTransport/add")
    fun addTransportAsync(@Body mRequstAddTransportParams: RequstAddTransportParams): Observable<HttpResponse<Any>>


    /**
     * 获取地块列表接口
     */
    @GET(VERSION + "IntelligentizeFieldRecord/massif/list")
    fun getPatrolmassifAsync(): Observable<HttpResponse<ArrayList<MassifBean>>>

    /**
     * 获取上报列表
     */
    @POST(VERSION + "IntelligentizeFieldRecord/messageReport/list")
    fun getPatrolReportAsync(@Body result: RequestParams?= RequestParams()): Observable<HttpResponse<ArrayList<ReportBean>>>

    /**
     * 获取巡田列表
     */
    @POST(VERSION + "IntelligentizeFieldRecord/pc/page/{pageNum}/{pageSize}")
    fun getPatrolListAsync(@Path("pageNum") pageNum: Int, @Path("pageSize") pageSize: Int?=15,@Body requestParams: RequestParams?= RequestParams()): Observable<HttpResponseList<PatrolBean>>

    /**
     * 新增巡田列表
     */
    @POST(VERSION + "IntelligentizeFieldRecord/save")
    fun addPatrolAsync(@Body addPatrolParams: AddPatrolParams): Observable<HttpResponse<Any>>

    /**
     * 获取巡田列表
     */
    @POST(VERSION + "IntelligentizeFieldRecord/{id}")
    fun getPatrolDetailAsync(@Path("id") id: String,@Body requestParams: RequestParams?=RequestParams()): Observable<HttpResponse<PatrolBean>>

}