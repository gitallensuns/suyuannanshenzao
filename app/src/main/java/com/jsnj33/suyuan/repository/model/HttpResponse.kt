package com.jsnj33.suyuan.repository.model

//所有接口最外层封装类
data class HttpResponse<T>(
    val message: String? = "接口异常",
    val code: Int = 0,
    val date: Long? = 0,
    val msg: String? = "",
    val data: T? = null
)