package com.jsnj33.suyuan.repository.client

import com.jsnj33.suyuan.BuildConfig
import com.jsnj33.suyuan.http.RetrofitCreateHelper

object RetrofitClient {
    const val BASE_URL = BuildConfig.APPURL//配置地址
    const val VERSION = "/api/1.1/"
    private var api: API? = null
    fun getInstance(type: String? = BASE_URL): API {
        api = RetrofitCreateHelper.createApi(API::class.java, type)
        return api!!
    }
}