#  叁拾叁纯溯源系统
*下列表示不同项目的渠道号*
 
```
     productFlavors {
        //射阳数据平台溯源
        sheyangsuyuan {}
        //广陵溯源
        guanlinsuyuan {}
        //盱眙纯溯源
        xuyisuyuan { }
         //马鞍纯溯源
        maansuyuan {}
     }
```
-------
注意：后续新增项目渠道，只需要在build.gradle新增渠道然后配置相关信息即可

-------

**例如：**
 
```
//马鞍
        maansuyuan {
            applicationId appId + ".maan"//包名
            versionCode 1 //版本号
            versionName "1.0.0_beta"//版本名
            flavorDimensions "maansuyuan"
            buildConfigField("String", "APPURL", '"http://119.3.169.92:8867"')//接口地址
            resValue "string", "app_name", "马鞍溯源" //app名字
            buildConfigField("String", "appTitle", '"马鞍溯源系统"')//登录页面title配置
            buildConfigField("int", "splash", 'R.mipmap.ic_splash_maan')//开屏页背景图配置；
            buildConfigField("int", "logo", 'R.drawable.ic_maanlogo')//登录页面logo配置；
            signingConfig signingConfigs.debug
            manifestPlaceholders = [
                    app_icon: "@drawable/ic_maanlogo",//logo
                    gaodeId : "9f2be99a2a3b939ecaa3720176065f1d"//高德key
            ]
        }
```


sha1:BD:B6:AD:15:C2:77:80:F8:6E:9F:71:F5:87:31:C1:08:94:3B:04:EE
包名 --:com.jsnj33.suyuan
 
     